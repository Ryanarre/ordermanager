﻿using System;
using System.Windows.Forms;

namespace OrdersManager
{
    public partial class StockLossDialog : Form
    {
        public StockLossDialog(
                MainWindow parent
            ,   DatabaseManager dbManager
            ,   StockType stockType
            ,   int stockId
            ,   bool isLoss
        )
        {
            InitializeComponent();

            mParent = parent;

            mDbManager = dbManager;

            mStockType = stockType;

            mStockId = stockId;

            if (!isLoss)
            {
                for (int i = 0; i < mDbManager.getOrderDao().size(); ++i)
                    orderCmb.Items.Add(mDbManager.getOrderDao().getOrder(i).Number);
            }
            else
            {
                orderLbl.Visible = false;
                orderCmb.Visible = false;

                guaranteeChk.Visible = false;
            }
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            Stock stock = mDbManager.getStockDao(mStockType).getStockByID(mStockId);

            int count = int.Parse(countTxt.Text);

            if (stock.CountEnd < count)
            {
                MessageBox.Show("Недостаточно материала на складе!");
                return;
            }
            else if (stock.CountEnd == count)
                mDbManager.getStockDao(mStockType).removeStock(mStockId);
            else
            {
                stock.CountEnd -= count;
                mDbManager.getStockDao(mStockType).modifyStock(stock);
            }

            StockHistory stockHistory = new StockHistory();

            stockHistory.OrderName = orderCmb.Text;
            stockHistory.Count = count;
            stockHistory.Date = lossDate.Value;
            stockHistory.StockID = mStockId;
            stockHistory.IsGuarantee = guaranteeChk.Checked;
            stockHistory.Comment = commentTxt.Text;
            stockHistory.Type = mStockType;
            stockHistory.Side = false;

            mDbManager.getStockHistoryDao().addStockHistory(stockHistory, true);

            mParent.reset();

            Close();
        }

        private void skipBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private MainWindow mParent;

        private DatabaseManager mDbManager;

        private StockType mStockType;

        private int mStockId;
    }
}
