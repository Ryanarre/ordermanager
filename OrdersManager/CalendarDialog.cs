﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace OrdersManager
{
    public partial class CalendarDialog : Form
    {
        DateTimePicker dtp = new DateTimePicker();
        Rectangle rect;
        public CalendarDialog(
                DatabaseManager dbManager
            ,   int ID
        )
        {
            InitializeComponent();

            mDbManager = dbManager;
            mID = ID;

            dataGridView1.Controls.Add(dtp);
            dtp.Visible = false;
            dtp.Format = DateTimePickerFormat.Custom;
            dtp.TextChanged += new EventHandler(dtp_TextChange);

            mDbManager = dbManager;
            mID = ID;

            if (mID != 0)
            {
                Calendar calendar = dbManager.getCalendarDao().getCalendarByID(ID);

                yearTxt.Text = calendar.Year.ToString();
                commentTxt.Text = calendar.Comment;

                for (int i = 0; i < mDbManager.getHolidayDao().size(); ++i)
                {
                    Holiday holiday = mDbManager.getHolidayDao().getHoliday(i);

                    if (holiday.CalendarID != ID)
                        continue;

                    dataGridView1.Rows.Add();
                    int index = dataGridView1.Rows.Count - 1;

                    dataGridView1.Rows[index].Cells[0].Value = holiday.Date;
                    dataGridView1.Rows[index].Cells[1].Value = holiday.Comment;
                }
            }
        }

        void dtp_TextChange(Object sender, EventArgs e)
        {
            dataGridView1.CurrentCell.Value = dtp.Text.ToString();
        }

        private void dataGridView1_Scroll(object sender, ScrollEventArgs e)
        {
            dtp.Visible = false;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == -1 || e.RowIndex == -1)
                return;

            switch (dataGridView1.Columns[e.ColumnIndex].Name)
            {
                case "date":
                    rect = dataGridView1.GetCellDisplayRectangle(e.ColumnIndex, e.RowIndex, true);
                    dtp.Size = new Size(rect.Width, rect.Height);
                    dtp.Location = new Point(rect.X, rect.Y);
                    dtp.Visible = true;

                    dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = dtp.Value.ToString("MM/dd/yyyy");

                    break;
            }
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            Calendar calendar = new Calendar();

            if (
                yearTxt.Text.Length == 0
            )
            {
                MessageBox.Show("Не все обязательные поля заполнены!");

                return;
            }

            if (!int.TryParse(yearTxt.Text, out calendar.Year))
            {
                MessageBox.Show(
                    "Невозможно добавить данные: некорректный формат числовых значений"
                  , "Ошибка"
                  , MessageBoxButtons.OK
                  , MessageBoxIcon.Error
                );
                return;
            }

            calendar.ID = mID;
            calendar.Comment = commentTxt.Text;

            if (mID != 0)
                mDbManager.getCalendarDao().modifyCalendar(calendar);
            else
            {
                mDbManager.getCalendarDao().addCalendar(calendar, true);

                mID = mDbManager.getCalendarDao().getCalendar(
                    mDbManager.getCalendarDao().size() - 1
                ).ID;
            }

            mDbManager.getHolidayDao().removeHolidays(mID);
            for (int i = 0; i < dataGridView1.Rows.Count; ++i)
            {
                if (dataGridView1.Rows[i].Cells[0].Value.ToString().Length == 0)
                {
                    MessageBox.Show("Не все обязательные поля заполнены!");

                    return;
                }

                Holiday holiday = new Holiday();

                holiday.CalendarID = mID;
                holiday.Date = DateTime.Parse(
                    dataGridView1.Rows[i].Cells[0].Value.ToString()
                );
                holiday.Comment = dataGridView1.Rows[i].Cells[1].Value.ToString();

                mDbManager.getHolidayDao().addHoliday(holiday, true);
            }

            Close();
        }

        private void removeCurrentRow()
        {
            int index = 0;
            if (detectCurrentRow(ref index))
                dataGridView1.Rows.RemoveAt(index);
        }

        private bool detectCurrentRow(ref int index)
        {
            DataGridViewCell cell = dataGridView1.CurrentCell;

            if (cell == null)
                return false;

            index = cell.RowIndex;
            return true;
        }

        private void addRowBtn_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Add();
        }

        private void removeRowBtn_Click(object sender, EventArgs e)
        {
            removeCurrentRow();
        }

        private void skipBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private DatabaseManager mDbManager;

        private int mID;
    }
}
