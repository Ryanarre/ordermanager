﻿using System.Collections.Generic;
using System.IO;

/*
 * TODO: Create base class for DAO - at least writeToFile/serialize/deserialize logic can be generalized (same for DB I suppose)
 */
namespace OrdersManager
{
    public class OrganisationDao
    {
        public OrganisationDao()
        {
            if (!File.Exists(mFileName))
                return;

            using (StreamReader reader = new StreamReader(mFileName))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    addOrganisation(
                        deserialize(line), false
                    );
                }
            }
        }
        public bool addOrganisation(Organisation organisation, bool write)
        {
            if (write)
            {
                int count = mOrganisations.Count;
                if (count != 0)
                    organisation.ID = mOrganisations[mOrganisations.Count - 1].ID + 1;
                else
                    organisation.ID = 1;
            }

            mOrganisations.Add(organisation);

            if (write)
                writeToFile();

            return true;
        }
        public Organisation getOrganisation(int index)
        {
            return mOrganisations[index];
        }

        public Organisation getOrganisationByID(int id)
        {
            return mOrganisations[getIndexByID(id)];
        }

        public void modifyOrganisation(Organisation organisation)
        {
            int index = getIndexByID(organisation.ID);

            if (index != -1)
            {
                mOrganisations[index] = organisation;

                writeToFile();
            }
        }

        public void removeOrganisation(int ID)
        {
            foreach (Organisation organisation in mOrganisations)
                if (organisation.ID == ID)
                {
                    mOrganisations.Remove(organisation);

                    writeToFile();

                    return;
                }
        }

        private int getIndexByID(int ID)
        {
            for (int i = 0; i < size(); ++i)
                if (mOrganisations[i].ID == ID)
                    return i;

            return -1;
        }

        private void writeToFile()
        {
            using (StreamWriter writetext = new StreamWriter(mFileName))
                foreach (Organisation cOrganisation in mOrganisations)
                    writetext.WriteLine(serialize(cOrganisation));
        }
        private string serialize(Organisation organisation)
        {
            return organisation.ID + "," +
                   organisation.Type + "," +
                   organisation.Name + "," +
                   organisation.StartDate.ToString() + "," +
                   organisation.EndDate.ToString() + "," +
                   organisation.Closed + "," +
                   organisation.Comment;
        }

        private Organisation deserialize(string str)
        {
            string[] values = str.Split(',');

            Organisation organisation = new Organisation();
            organisation.ID = int.Parse(values[0]);
            organisation.Type = values[1];
            organisation.Name = values[2];
            organisation.StartDate = System.DateTime.Parse(values[3]);
            organisation.EndDate = System.DateTime.Parse(values[4]);
            organisation.Closed = bool.Parse(values[5]);
            organisation.Comment = values[6];

            return organisation;
        }
        public int size()
        {
            return mOrganisations.Count;
        }

        private const string mFileName = "organisations.txt";

        private List<Organisation> mOrganisations = new List<Organisation>();
    }
}
