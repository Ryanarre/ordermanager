﻿using System.Collections.Generic;
using System.IO;

/*
 * TODO: Create base class for DAO - at least writeToFile/serialize/deserialize logic can be generalized (same for DB I suppose)
 */
namespace OrdersManager
{
    public class SalaryDao
    {
        public SalaryDao()
        {
            if (!File.Exists(mFileName))
                return;

            using (StreamReader reader = new StreamReader(mFileName))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    addSalary(
                        deserialize(line), false
                    );
                }
            }
        }
        public bool addSalary(Salary salary, bool write)
        {
            mSalaries.Add(salary);

            if (write)
                writeToFile();

            return true;
        }
        public Salary getSalary(int index)
        {
            return mSalaries[index];
        }

        private void writeToFile()
        {
            using (StreamWriter writetext = new StreamWriter(mFileName))
                foreach (Salary cSalary in mSalaries)
                    writetext.WriteLine(serialize(cSalary));
        }
        private string serialize(Salary salary)
        {
            return salary.Date + "," +
                   salary.Amount + "," +
                   salary.WorkerID + "," +
                   salary.Title;
        }

        private Salary deserialize(string str)
        {
            string[] values = str.Split(',');

            Salary salary = new Salary();
            salary.Date = System.DateTime.Parse(values[0]);
            salary.Amount = double.Parse(values[1]);
            salary.WorkerID = int.Parse(values[2]);
            salary.Title = values[3];

            return salary;
        }
        public int size()
        {
            return mSalaries.Count;
        }

        private const string mFileName = "salaries.txt";

        private List<Salary> mSalaries = new List<Salary>();
    }
}
