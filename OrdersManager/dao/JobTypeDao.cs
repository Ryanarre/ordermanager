﻿using System.Collections.Generic;
using System.IO;

/*
 * TODO: Create base class for DAO - at least writeToFile/serialize/deserialize logic can be generalized (same for DB I suppose)
 */
namespace OrdersManager
{
    public class JobTypeDao
    {
        public JobTypeDao()
        {
            if (!File.Exists(mFileName))
                return;

            using (StreamReader reader = new StreamReader(mFileName))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    addJobType(
                        deserialize(line), false
                    );
                }
            }
        }
        public bool addJobType(JobType jobType, bool write)
        {
            if (write)
            {
                int count = mJobTypes.Count;
                if (count != 0)
                    jobType.ID = mJobTypes[mJobTypes.Count - 1].ID + 1;
                else
                    jobType.ID = 1;
            }

            mJobTypes.Add(jobType);

            if (write)
                writeToFile();

            return true;
        }
        public JobType getJobType(int index)
        {
            return mJobTypes[index];
        }

        public JobType getJobTypeByID(int id)
        {
            return mJobTypes[getIndexByID(id)];
        }

        public void modifyJobType(JobType jobType)
        {
            int index = getIndexByID(jobType.ID);

            if (index != -1)
            {
                mJobTypes[index] = jobType;

                writeToFile();
            }
        }

        public void removeJobType(int ID)
        {
            foreach (JobType jobType in mJobTypes)
                if (jobType.ID == ID)
                {
                    mJobTypes.Remove(jobType);

                    writeToFile();

                    return;
                }
        }

        private int getIndexByID(int ID)
        {
            for (int i = 0; i < size(); ++i)
                if (mJobTypes[i].ID == ID)
                    return i;

            return -1;
        }

        private void writeToFile()
        {
            using (StreamWriter writetext = new StreamWriter(mFileName))
                foreach (JobType cJobType in mJobTypes)
                    writetext.WriteLine(serialize(cJobType));
        }
        private string serialize(JobType jobType)
        {
            return jobType.ID + "," +
                   jobType.Name + "," +
                   jobType.Unit + "," +
                   jobType.PricePerUnit.ToString() + "," +
                   jobType.Comment;
        }

        private JobType deserialize(string str)
        {
            string[] values = str.Split(',');

            JobType jobType = new JobType();
            jobType.ID = int.Parse(values[0]);
            jobType.Name = values[1];
            jobType.Unit = values[2];
            jobType.PricePerUnit = double.Parse(values[3]);
            jobType.Comment = values[4];

            return jobType;
        }
        public int size()
        {
            return mJobTypes.Count;
        }

        private const string mFileName = "jobTypes.txt";

        private List<JobType> mJobTypes = new List<JobType>();
    }
}
