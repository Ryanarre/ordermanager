﻿using System.Collections.Generic;
using System.IO;

/*
 * TODO: Create base class for DAO - at least writeToFile/serialize/deserialize logic can be generalized (same for DB I suppose)
 */
namespace OrdersManager
{
    public class AgentDao
    {
        public AgentDao(AgentType agentType)
        {
            switch (agentType)
            {
                case AgentType.Supplier:
                    mFileName = "suppliers.txt";
                    break;
                case AgentType.Customer:
                    mFileName = "customers.txt";
                    break;
                case AgentType.Contractor:
                    mFileName = "contractors.txt";
                    break;
            }

            if (!File.Exists(mFileName))
                return;

            using (StreamReader reader = new StreamReader(mFileName))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    addAgent(
                        deserialize(line), false
                    );
                }
            }
        }
        public bool addAgent(Agent agent, bool write)
        {
            if (write)
            {
                int count = mAgents.Count;
                if (count != 0)
                    agent.ID = mAgents[mAgents.Count - 1].ID + 1;
                else
                    agent.ID = 1;
            }

            mAgents.Add(agent);

            if (write)
                writeToFile();

            return true;
        }
        public Agent getAgent(int index)
        {
            return mAgents[index];
        }

        public Agent getAgentByID(int id)
        {
            foreach (Agent agent in mAgents)
                if (agent.ID == id)
                    return agent;

            return new Agent();
        }

        public void modifyAgent(Agent agent)
        {
            int index = getIndexByID(agent.ID);

            if (index != -1)
            {
                mAgents[index] = agent;

                writeToFile();
            }
        }

        public void removeAgent(int ID)
        {
            foreach (Agent agent in mAgents)
                if (agent.ID == ID)
                {
                    mAgents.Remove(agent);

                    writeToFile();

                    return;
                }
        }

        private int getIndexByID(int id)
        {
            for (int i = 0; i < size(); ++i)
                if (mAgents[i].ID == id)
                    return i;

            return -1;
        }

        private void writeToFile()
        {
            using (StreamWriter writetext = new StreamWriter(mFileName))
                foreach (Agent cAgent in mAgents)
                    writetext.WriteLine(serialize(cAgent));
        }
        private string serialize(Agent agent)
        {
            return agent.ID + "," +
                   agent.Name + "," +
                   agent.Phone + "," +
                   agent.Email;
        }

        private Agent deserialize(string str)
        {
            string[] values = str.Split(',');

            Agent agent = new Agent();
            agent.ID = int.Parse(values[0]);
            agent.Name = values[1];
            agent.Phone = values[2];
            agent.Email = values[3];

            return agent;
        }
        public int size()
        {
            return mAgents.Count;
        }

        private string mFileName;

        private List<Agent> mAgents = new List<Agent>();
    }
}
