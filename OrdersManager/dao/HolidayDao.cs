﻿using System.Collections.Generic;
using System.IO;

/*
 * TODO: Create base class for DAO - at least writeToFile/serialize/deserialize logic can be generalized (same for DB I suppose)
 */
namespace OrdersManager
{
    public class HolidayDao
    {
        public HolidayDao()
        {
            if (!File.Exists(mFileName))
                return;

            using (StreamReader reader = new StreamReader(mFileName))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    addHoliday(
                        deserialize(line), false
                    );
                }
            }
        }
        public bool addHoliday(Holiday holiday, bool write)
        {
            mHolidays.Add(holiday);

            if (write)
                writeToFile();

            return true;
        }
        public Holiday getHoliday(int index)
        {
            return mHolidays[index];
        }

        public void removeHolidays(int calendarID)
        {
            foreach (Holiday holiday in mHolidays)
                if (holiday.CalendarID == calendarID)
                    mHolidays.Remove(holiday);

            writeToFile();
        }

        private void writeToFile()
        {
            using (StreamWriter writetext = new StreamWriter(mFileName))
                foreach (Holiday cHoliday in mHolidays)
                    writetext.WriteLine(serialize(cHoliday));
        }
        private string serialize(Holiday holiday)
        {
            return holiday.CalendarID + "," +
                   holiday.Date.ToString() + "," +
                   holiday.Comment;
        }

        private Holiday deserialize(string str)
        {
            string[] values = str.Split(',');

            Holiday holiday = new Holiday();
            holiday.CalendarID = int.Parse(values[0]);
            holiday.Date = System.DateTime.Parse(values[1]);
            holiday.Comment = values[2];

            return holiday;
        }
        public int size()
        {
            return mHolidays.Count;
        }

        private const string mFileName = "holidays.txt";

        private List<Holiday> mHolidays = new List<Holiday>();
    }
}
