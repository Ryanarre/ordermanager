﻿using System.Collections.Generic;
using System.IO;

namespace OrdersManager
{
    public class FlowDao
    {
        public FlowDao(FlowType flowType)
        {
            switch (flowType)
            {
                default:
                case FlowType.Profit:
                    mFileName = "profits.txt";
                    break;
                case FlowType.Loss:
                    mFileName = "losses.txt";
                    break;
                case FlowType.Charge:
                    mFileName = "charges.txt";
                    break;
                case FlowType.Payment:
                    mFileName = "payments.txt";
                    break;
            }

            if (!File.Exists(mFileName))
                return;

            using (StreamReader reader = new StreamReader(mFileName))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                    addFlow(deserialize(line), false);
            }
        }
        public void addFlow(Flow flow, bool write)
        {
            if (write)
            {
                int count = mFlows.Count;
                if (count != 0)
                    flow.ID = mFlows[mFlows.Count - 1].ID + 1;
                else
                    flow.ID = 1;
            }

            mFlows.Add(flow);

            if (write)
                writeToFile();
        }

        public void modifyFlow(Flow flow)
        {
            int index = getIndexByID(flow.ID);

            if (index != -1)
            {
                mFlows[index] = flow;

                writeToFile();
            }
        }

        public void removeFlow(int ID)
        {
            foreach (Flow flow in mFlows)
                if (flow.ID == ID)
                {
                    mFlows.Remove(flow);

                    writeToFile();

                    return;
                }
        }

        public Flow getFlowByID(int id)
        {
            foreach (Flow flow in mFlows)
                if (flow.ID == id)
                    return flow;

            return new Flow();
        }

        public Flow getFlow(int index)
        {
            return mFlows[index];
        }

        public int size()
        {
            return mFlows.Count;
        }

        private int getIndexByID(int id)
        {
            for (int i = 0; i < size(); ++i)
                if (mFlows[i].ID == id)
                    return i;

            return -1;
        }

        private string serialize(Flow flow)
        {
            return flow.ID + "," +
                   flow.Number + "," +
                   flow.Purpose + "," +
                   flow.Date.ToString() + "," +
                   flow.Summ + "," +
                   flow.Agent + "," +
                   flow.Currency + "," +
                   flow.Comment + "," +
                   flow.IsGuarantee.ToString() + "," +
                   flow.Unit + "," +
                   flow.PricePerUnit;
        }

        private Flow deserialize(string orderStr)
        {
            string[] values = orderStr.Split(',');

            Flow flow = new Flow();

            flow.ID = int.Parse(values[0]);
            flow.Number = values[1];
            flow.Purpose = values[2];
            flow.Date = System.DateTime.Parse(values[3]);
            flow.Summ = double.Parse(values[4]);
            flow.Agent = values[5];
            flow.Currency = values[6];
            flow.Comment = values[7];
            flow.IsGuarantee = bool.Parse(values[8]);
            flow.Unit = values[9];
            flow.PricePerUnit = double.Parse(values[10]);

            return flow;
        }

        private void writeToFile()
        {
            using (StreamWriter writetext = new StreamWriter(mFileName))
                foreach (Flow cFlow in mFlows)
                    writetext.WriteLine(serialize(cFlow));
        }

        private List<Flow> mFlows = new List<Flow>();

        private string mFileName;
    }
}
