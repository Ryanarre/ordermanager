﻿using System.Collections.Generic;
using System.IO;

/*
 * TODO: Create base class for DAO - at least writeToFile/serialize/deserialize logic can be generalized (same for DB I suppose)
 */
namespace OrdersManager
{
    public class ReferenceDao
    {
        public ReferenceDao(MenuElem referenceType)
        {
            switch (referenceType)
            {
                case MenuElem.Department:
                    mFileName = "departments.txt";
                    break;
                case MenuElem.PurposeFinancial:
                    mFileName = "puprosesFinancial.txt";
                    break;
                case MenuElem.PurposeMaterial:
                    mFileName = "puprosesMaterial.txt";
                    break;
            }

            if (!File.Exists(mFileName))
                return;

            using (StreamReader reader = new StreamReader(mFileName))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    addReference(
                        deserialize(line), false
                    );
                }
            }
        }
        public bool addReference(Reference reference, bool write)
        {
            if (write)
            {
                int count = mReferences.Count;
                if (count != 0)
                    reference.ID = mReferences[mReferences.Count - 1].ID + 1;
                else
                    reference.ID = 1;
            }

            mReferences.Add(reference);

            if (write)
                writeToFile();

            return true;
        }
        public Reference getReference(int index)
        {
            return mReferences[index];
        }

        public Reference getReferenceByID(int id)
        {
            return mReferences[getIndexByID(id)];
        }

        public void modifyReference(Reference reference)
        {
            int index = getIndexByID(reference.ID);

            if (index != -1)
            {
                mReferences[index] = reference;

                writeToFile();
            }
        }

        public void removeReference(int ID)
        {
            foreach (Reference reference in mReferences)
                if (reference.ID == ID)
                {
                    mReferences.Remove(reference);

                    writeToFile();

                    return;
                }
        }

        private int getIndexByID(int ID)
        {
            for (int i = 0; i < size(); ++i)
                if (mReferences[i].ID == ID)
                    return i;

            return -1;
        }

        private void writeToFile()
        {
            using (StreamWriter writetext = new StreamWriter(mFileName))
                foreach (Reference cReference in mReferences)
                    writetext.WriteLine(serialize(cReference));
        }
        private string serialize(Reference reference)
        {
            return reference.ID + "," +
                   reference.Name + "," +
                   reference.Comment + ",";
        }

        private Reference deserialize(string str)
        {
            string[] values = str.Split(',');

            Reference reference = new Reference();
            reference.ID = int.Parse(values[0]);
            reference.Name = values[1];
            reference.Comment = values[2];

            return reference;
        }
        public int size()
        {
            return mReferences.Count;
        }

        private string mFileName;

        private List<Reference> mReferences = new List<Reference>();
    }
}
