﻿using System.Collections.Generic;
using System.IO;

/*
 * TODO: Create base class for DAO - at least writeToFile/serialize/deserialize logic can be generalized (same for DB I suppose)
 */
namespace OrdersManager
{
    public class WorkDayDao
    {
        public WorkDayDao()
        {
            if (!File.Exists(mFileName))
                return;

            using (StreamReader reader = new StreamReader(mFileName))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    addWorkDay(
                        deserialize(line), false
                    );
                }
            }
        }
        public bool addWorkDay(WorkDay workDay, bool write)
        {
            if (write)
            {
                int count = mWorkDays.Count;
                if (count != 0)
                    workDay.ID = mWorkDays[mWorkDays.Count - 1].ID + 1;
                else
                    workDay.ID = 1;
            }

            mWorkDays.Add(workDay);

            if (write)
                writeToFile();

            return true;
        }
        public WorkDay getWorkDay(int index)
        {
            return mWorkDays[index];
        }

        public WorkDay getWorkDayByID(int id)
        {
            return mWorkDays[getIndexByID(id)];
        }

        public void modifyWorkDay(WorkDay workDay)
        {
            int index = getIndexByID(workDay.ID);

            if (index != -1)
            {
                mWorkDays[index] = workDay;

                writeToFile();
            }
        }

        public void removeWorkDay(int ID)
        {
            foreach (WorkDay workDay in mWorkDays)
                if (workDay.ID == ID)
                {
                    mWorkDays.Remove(workDay);

                    writeToFile();

                    return;
                }
        }

        private int getIndexByID(int ID)
        {
            for (int i = 0; i < size(); ++i)
                if (mWorkDays[i].ID == ID)
                    return i;

            return -1;
        }

        private void writeToFile()
        {
            using (StreamWriter writetext = new StreamWriter(mFileName))
                foreach (WorkDay cWorkDay in mWorkDays)
                    writetext.WriteLine(serialize(cWorkDay));
        }
        private string serialize(WorkDay workDay)
        {
            return workDay.ID + "," +
                   workDay.Date + "," +
                   workDay.Comment;
        }

        private WorkDay deserialize(string str)
        {
            string[] values = str.Split(',');

            WorkDay workDay = new WorkDay();
            workDay.ID = int.Parse(values[0]);
            workDay.Date = System.DateTime.Parse(values[1]);
            workDay.Comment = values[2];

            return workDay;
        }
        public int size()
        {
            return mWorkDays.Count;
        }

        private const string mFileName = "workDays.txt";

        private List<WorkDay> mWorkDays = new List<WorkDay>();
    }
}
