﻿using System.Collections.Generic;
using System.IO;

/*
 * TODO: Create base class for DAO - at least writeToFile/serialize/deserialize logic can be generalized (same for DB I suppose)
 */
namespace OrdersManager
{
    public class StockInfoDao
    {
        public StockInfoDao(StockType stockType)
        {
            switch (stockType)
            {
                case StockType.Material:
                    mFileName = "materialsInfo.txt";
                    break;
                case StockType.Tool:
                    mFileName = "toolsInfo.txt";
                    break;
                case StockType.CustomMaterial:
                    mFileName = "customMaterialsInfo.txt";
                    break;
                case StockType.Trim:
                    mFileName = "trimsInfo.txt";
                    break;
            }

            if (!File.Exists(mFileName))
                return;

            using (StreamReader reader = new StreamReader(mFileName))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    addStockInfo(
                        deserialize(line), false
                    );
                }
            }
        }
        public bool addStockInfo(StockInfo stockInfo, bool write)
        {
            if (write)
            {
                int count = mStocks.Count;
                if (count != 0)
                    stockInfo.ID = mStocks[mStocks.Count - 1].ID + 1;
                else
                    stockInfo.ID = 1;
            }

            mStocks.Add(stockInfo);

            if (write)
                writeToFile();

            return true;
        }
        public StockInfo getStockInfo(int index)
        {
            return mStocks[index];
        }

        public StockInfo getStockInfoByID(int id)
        {
            return mStocks[getIndexByID(id)];
        }

        public void modifyStockInfo(StockInfo stockInfo)
        {
            int index = getIndexByID(stockInfo.ID);

            if (index != -1)
            {
                mStocks[index] = stockInfo;

                writeToFile();
            }
        }

        public void removeStockInfo(int ID)
        {
            foreach (StockInfo stockInfo in mStocks)
                if (stockInfo.ID == ID)
                {
                    mStocks.Remove(stockInfo);

                    writeToFile();

                    return;
                }
        }

        private int getIndexByID(int ID)
        {
            for (int i = 0; i < size(); ++i)
                if (mStocks[i].ID == ID)
                    return i;

            return -1;
        }

        private void writeToFile()
        {
            using (StreamWriter writetext = new StreamWriter(mFileName))
                foreach (StockInfo cStock in mStocks)
                    writetext.WriteLine(serialize(cStock));
        }
        private string serialize(StockInfo stockInfo)
        {
            return stockInfo.ID + "," +
                   stockInfo.Name + "," +
                   stockInfo.Unit + "," +
                   stockInfo.Comment;
        }

        private StockInfo deserialize(string str)
        {
            string[] values = str.Split(',');

            StockInfo stockInfo = new StockInfo();
            stockInfo.ID = int.Parse(values[0]);
            stockInfo.Name = values[1];
            stockInfo.Unit = values[2];
            stockInfo.Comment = values[3];

            return stockInfo;
        }
        public int size()
        {
            return mStocks.Count;
        }

        private string mFileName;

        private List<StockInfo> mStocks = new List<StockInfo>();
    }
}
