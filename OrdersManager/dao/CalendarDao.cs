﻿using System.Collections.Generic;
using System.IO;

/*
 * TODO: Create base class for DAO - at least writeToFile/serialize/deserialize logic can be generalized (same for DB I suppose)
 */
namespace OrdersManager
{
    public class CalendarDao
    {
        public CalendarDao()
        {
            if (!File.Exists(mFileName))
                return;

            using (StreamReader reader = new StreamReader(mFileName))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    addCalendar(
                        deserialize(line), false
                    );
                }
            }
        }
        public bool addCalendar(Calendar calendar, bool write)
        {
            if (write)
            {
                int count = mCalendars.Count;
                if (count != 0)
                    calendar.ID = mCalendars[mCalendars.Count - 1].ID + 1;
                else
                    calendar.ID = 1;
            }

            mCalendars.Add(calendar);

            if (write)
                writeToFile();

            return true;
        }
        public Calendar getCalendar(int index)
        {
            return mCalendars[index];
        }

        public Calendar getCalendarByID(int id)
        {
            return mCalendars[getIndexByID(id)];
        }

        public void modifyCalendar(Calendar calendar)
        {
            int index = getIndexByID(calendar.ID);

            if (index != -1)
            {
                mCalendars[index] = calendar;

                writeToFile();
            }
        }

        public void removeCalendar(int ID)
        {
            foreach (Calendar calendar in mCalendars)
                if (calendar.ID == ID)
                {
                    mCalendars.Remove(calendar);

                    writeToFile();

                    return;
                }
        }

        private int getIndexByID(int ID)
        {
            for (int i = 0; i < size(); ++i)
                if (mCalendars[i].ID == ID)
                    return i;

            return -1;
        }

        private void writeToFile()
        {
            using (StreamWriter writetext = new StreamWriter(mFileName))
                foreach (Calendar cCalendar in mCalendars)
                    writetext.WriteLine(serialize(cCalendar));
        }
        private string serialize(Calendar calendar)
        {
            return calendar.ID + "," +
                   calendar.Year.ToString() + "," +
                   calendar.Comment;
        }

        private Calendar deserialize(string str)
        {
            string[] values = str.Split(',');

            Calendar calendar = new Calendar();
            calendar.ID = int.Parse(values[0]);
            calendar.Year = int.Parse(values[1]);
            calendar.Comment = values[2];

            return calendar;
        }
        public int size()
        {
            return mCalendars.Count;
        }

        private const string mFileName = "calendars.txt";

        private List<Calendar> mCalendars = new List<Calendar>();
    }
}
