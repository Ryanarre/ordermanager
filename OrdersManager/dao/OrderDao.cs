﻿using System.Collections.Generic;
using System.IO;

namespace OrdersManager
{
    public class OrderDao
    {
        public OrderDao(bool isTemp)
        {
            mFileName = isTemp ? "temp_orders.txt" : "orders.txt";

            if (!File.Exists(mFileName))
                return;

            using (StreamReader reader = new StreamReader(mFileName))
            {
                string line;
                if ((line = reader.ReadLine()) != null)
                    mLastID = int.Parse(line);

                while ((line = reader.ReadLine()) != null)
                {
                    addOrder(
                        deserialize(line), false
                    );
                }
            }
        }

        public void addOrder(Order order, bool write)
        {
            if (order.ID == 0)
                order.ID = ++mLastID;

            mOrders.Add(order);

            if (write)
                writeToFile();
        }

        public void modifyOrder(Order order)
        {
            int index = getIndexByID(order.ID);

            if (index != -1)
            {
                mOrders[index] = order;

                writeToFile();
            }
        }

        public void removeOrder(int ID)
        {
            foreach (Order order in mOrders)
                if (order.ID == ID)
                {
                    mOrders.Remove(order);

                    writeToFile();

                    return;
                }
        }

        public Order getOrderByID(int id)
        {
            foreach (Order order in mOrders)
                if (order.ID == id)
                    return order;

            return new Order();
        }

        public Order getOrder(int index)
        {
            return mOrders[index];
        }

        public int size()
        {
            return mOrders.Count;
        }

        private int getIndexByID(int id)
        {
            for (int i = 0; i < size(); ++i)
                if (mOrders[i].ID == id)
                    return i;

            return -1;
        }

        private string serialize(Order order)
        {
            return order.ID + "," +
                   order.Number + "," +
                   order.Name + "," +
                   order.Type + "," +
                   order.DebtClients + "," +
                   order.DebtWorkers + "," +
                   order.StartDate.ToString() + "," +
                   order.EndDate.ToString() + "," +
                   order.Summ;
        }

        private Order deserialize(string orderStr)
        {
            string[] values = orderStr.Split(',');

            Order order = new Order();

            order.ID = int.Parse(values[0]);
            order.Number = values[1];
            order.Name = values[2];
            order.Type = values[3];
            order.DebtClients = double.Parse(values[4]);
            order.DebtWorkers = double.Parse(values[5]);
            order.StartDate = System.DateTime.Parse(values[6]);
            order.EndDate = System.DateTime.Parse(values[7]);
            order.Summ = double.Parse(values[8]);

            return order;
        }

        private void writeToFile()
        {
            using (StreamWriter writetext = new StreamWriter(mFileName))
            {
                writetext.WriteLine(mLastID);

                foreach (Order cOrder in mOrders)
                    writetext.WriteLine(serialize(cOrder));
            }
        }

        private List<Order> mOrders = new List<Order>();

        private int mLastID;

        private string mFileName;
    }
}
