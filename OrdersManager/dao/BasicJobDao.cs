﻿using System.Collections.Generic;
using System.IO;

/*
 * TODO: Create base class for DAO - at least writeToFile/serialize/deserialize logic can be generalized (same for DB I suppose)
 */
namespace OrdersManager
{
    public class BasicJobDao
    {
        public BasicJobDao()
        {
            if (!File.Exists(mFileName))
                return;

            using (StreamReader reader = new StreamReader(mFileName))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    addBasicJob(
                        deserialize(line), false
                    );
                }
            }
        }
        public bool addBasicJob(BasicJob basicJob, bool write)
        {
            if (write)
            {
                int count = mBasicJobs.Count;
                if (count != 0)
                    basicJob.ID = mBasicJobs[mBasicJobs.Count - 1].ID + 1;
                else
                    basicJob.ID = 1;
            }

            mBasicJobs.Add(basicJob);

            if (write)
                writeToFile();

            return true;
        }
        public BasicJob getBasicJob(int index)
        {
            return mBasicJobs[index];
        }

        public BasicJob getBasicJobByID(int id)
        {
            return mBasicJobs[getIndexByID(id)];
        }

        public void modifyBasicJob(BasicJob basicJob)
        {
            int index = getIndexByID(basicJob.ID);

            if (index != -1)
            {
                mBasicJobs[index] = basicJob;

                writeToFile();
            }
        }

        public void removeBasicJob(int ID)
        {
            foreach (BasicJob basicJob in mBasicJobs)
                if (basicJob.ID == ID)
                {
                    mBasicJobs.Remove(basicJob);

                    writeToFile();

                    return;
                }
        }

        private int getIndexByID(int ID)
        {
            for (int i = 0; i < size(); ++i)
                if (mBasicJobs[i].ID == ID)
                    return i;

            return -1;
        }

        private void writeToFile()
        {
            using (StreamWriter writetext = new StreamWriter(mFileName))
                foreach (BasicJob cBasicJob in mBasicJobs)
                    writetext.WriteLine(serialize(cBasicJob));
        }
        private string serialize(BasicJob basicJob)
        {
            return basicJob.ID + "," +
                   basicJob.Name + "," +
                   basicJob.Unit + "," +
                   basicJob.PricePerUnit.ToString() + "," +
                   basicJob.Comment;
        }

        private BasicJob deserialize(string str)
        {
            string[] values = str.Split(',');

            BasicJob basicJob = new BasicJob();
            basicJob.ID = int.Parse(values[0]);
            basicJob.Name = values[1];
            basicJob.Unit = values[2];
            basicJob.PricePerUnit = double.Parse(values[3]);
            basicJob.Comment = values[4];

            return basicJob;
        }
        public int size()
        {
            return mBasicJobs.Count;
        }

        private const string mFileName = "basicJobs.txt";

        private List<BasicJob> mBasicJobs = new List<BasicJob>();
    }
}
