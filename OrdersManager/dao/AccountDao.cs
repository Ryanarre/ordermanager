﻿using System.Collections.Generic;
using System.IO;

/*
 * TODO: Create base class for DAO - at least writeToFile/serialize/deserialize logic can be generalized (same for DB I suppose)
 */
namespace OrdersManager
{
    public class AccountDao
    {
        public AccountDao()
        {
            if (!File.Exists(mFileName))
                return;

            using (StreamReader reader = new StreamReader(mFileName))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    addAccount(
                        deserialize(line), false
                    );
                }
            }
        }
        public bool addAccount(Account account, bool write)
        {
            if (write)
            {
                int count = mAccounts.Count;
                if (count != 0)
                    account.ID = mAccounts[mAccounts.Count - 1].ID + 1;
                else
                    account.ID = 1;
            }

            mAccounts.Add(account);

            if (write)
                writeToFile();

            return true;
        }
        public Account getAccount(int index)
        {
            return mAccounts[index];
        }

        public Account getAccountByID(int id)
        {
            return mAccounts[getIndexByID(id)];
        }

        public void modifyAccount(Account account)
        {
            int index = getIndexByID(account.ID);

            if (index != -1)
            {
                mAccounts[index] = account;

                writeToFile();
            }
        }

        public void removeAccount(int ID)
        {
            foreach (Account account in mAccounts)
                if (account.ID == ID)
                {
                    mAccounts.Remove(account);

                    writeToFile();

                    return;
                }
        }

        private int getIndexByID(int ID)
        {
            for (int i = 0; i < size(); ++i)
                if (mAccounts[i].ID == ID)
                    return i;

            return -1;
        }

        private void writeToFile()
        {
            using (StreamWriter writetext = new StreamWriter(mFileName))
                foreach (Account cAccount in mAccounts)
                    writetext.WriteLine(serialize(cAccount));
        }
        private string serialize(Account account)
        {
            return account.ID + "," +
                   account.Name + "," +
                   account.Currency + "," +
                   account.StartDate.ToString() + "," +
                   account.EndDate.ToString() + "," +
                   account.Closed + "," +
                   account.Organisation + "," +
                   account.Bank + "," +
                   account.Number + "," +
                   account.Comment;
        }

        private Account deserialize(string str)
        {
            string[] values = str.Split(',');

            Account account = new Account();
            account.ID = int.Parse(values[0]);
            account.Name = values[1];
            account.Currency = values[2];
            account.StartDate = System.DateTime.Parse(values[3]);
            account.EndDate = System.DateTime.Parse(values[4]);
            account.Closed = bool.Parse(values[5]);
            account.Organisation = values[6];
            account.Bank = values[7];
            account.Number = values[8];
            account.Comment = values[9];

            return account;
        }
        public int size()
        {
            return mAccounts.Count;
        }

        private const string mFileName = "accounts.txt";

        private List<Account> mAccounts = new List<Account>();
    }
}
