﻿using System.Collections.Generic;
using System.IO;

/*
 * TODO: Create base class for DAO - at least writeToFile/serialize/deserialize logic can be generalized (same for DB I suppose)
 */
namespace OrdersManager
{
    public class StockDao
    {
        public StockDao(StockType stockType)
        {
            switch (stockType)
            {
                case StockType.Material:
                    mFileName = "materials.txt";
                    break;
                case StockType.Tool:
                    mFileName = "tools.txt";
                    break;
                case StockType.CustomMaterial:
                    mFileName = "customMaterials.txt";
                    break;
                case StockType.Trim:
                    mFileName = "trims.txt";
                    break;
            }

            if (!File.Exists(mFileName))
                return;

            using (StreamReader reader = new StreamReader(mFileName))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    addStock(
                        deserialize(line), false
                    );
                }
            }
        }
        public bool addStock(Stock stock, bool write)
        {
            if (write)
            {
                int count = mStocks.Count;
                if (count != 0)
                    stock.ID = mStocks[mStocks.Count - 1].ID + 1;
                else
                    stock.ID = 1;
            }

            mStocks.Add(stock);

            if (write)
                writeToFile();

            return true;
        }
        public Stock getStock(int index)
        {
            return mStocks[index];
        }

        public Stock getStockByID(int id)
        {
            return mStocks[getIndexByID(id)];
        }

        public void modifyStock(Stock stock)
        {
            int index = getIndexByID(stock.ID);

            if (index != -1)
            {
                mStocks[index] = stock;

                writeToFile();
            }
        }

        public void removeStock(int ID)
        {
            foreach (Stock stock in mStocks)
                if (stock.ID == ID)
                {
                    mStocks.Remove(stock);

                    writeToFile();

                    return;
                }
        }

        private int getIndexByID(int ID)
        {
            for (int i = 0; i < size(); ++i)
                if (mStocks[i].ID == ID)
                    return i;

            return -1;
        }

        private void writeToFile()
        {
            using (StreamWriter writetext = new StreamWriter(mFileName))
                foreach (Stock cStock in mStocks)
                    writetext.WriteLine(serialize(cStock));
        }
        private string serialize(Stock stock)
        {
            return stock.ID + "," +
                   stock.Name + "," +
                   stock.Unit + "," +
                   stock.PricePerUnite + "," +
                   stock.CountBegin + "," +
                   stock.CountEnd + ",";
        }

        private Stock deserialize(string str)
        {
            string[] values = str.Split(',');

            Stock stock = new Stock();
            stock.ID = int.Parse(values[0]);
            stock.Name = values[1];
            stock.Unit = values[2];
            stock.PricePerUnite = double.Parse(values[3]);
            stock.CountBegin = int.Parse(values[4]);
            stock.CountEnd = int.Parse(values[5]);

            return stock;
        }
        public int size()
        {
            return mStocks.Count;
        }

        private string mFileName;

        private List<Stock> mStocks = new List<Stock>();
    }
}
