﻿using System.Collections.Generic;
using System.IO;

/*
 * TODO: Create base class for DAO - at least writeToFile/serialize/deserialize logic can be generalized (same for DB I suppose)
 */
namespace OrdersManager
{
    public class WorkLogDao
    {
        public WorkLogDao()
        {
            if (!File.Exists(mFileName))
                return;

            using (StreamReader reader = new StreamReader(mFileName))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    addWorkLog(
                        deserialize(line), false
                    );
                }
            }
        }
        public bool addWorkLog(WorkLog workLog, bool write)
        {
            mWorkLogs.Add(workLog);

            if (write)
                writeToFile();

            return true;
        }
        public WorkLog getWorkLog(int index)
        {
            return mWorkLogs[index];
        }

        public void removeWorkLogs(int ID)
        {
            foreach (WorkLog workLog in mWorkLogs)
                if (workLog.WorkDayID == ID)
                    mWorkLogs.Remove(workLog);

            writeToFile();
        }

        private void writeToFile()
        {
            using (StreamWriter writetext = new StreamWriter(mFileName))
                foreach (WorkLog cWorkLog in mWorkLogs)
                    writetext.WriteLine(serialize(cWorkLog));
        }
        private string serialize(WorkLog workLog)
        {
            return workLog.WorkerID + "," +
                   workLog.WorkDayID + "," +
                   workLog.Logged.ToString() + "," +
                   workLog.Missed.ToString() + "," +
                   workLog.PaidSick.ToString() + "," +
                   workLog.UnpaidSick.ToString() + "," +
                   workLog.PaidLeave.ToString() + "," +
                   workLog.UnpaidLeave.ToString();
        }

        private WorkLog deserialize(string str)
        {
            string[] values = str.Split(',');

            WorkLog workLog = new WorkLog();
            workLog.WorkerID = int.Parse(values[0]);
            workLog.WorkDayID = int.Parse(values[1]);
            workLog.Logged = int.Parse(values[2]);
            workLog.Missed = int.Parse(values[3]);
            workLog.PaidSick = int.Parse(values[4]);
            workLog.UnpaidSick = int.Parse(values[5]);
            workLog.PaidLeave = int.Parse(values[6]);
            workLog.UnpaidLeave = int.Parse(values[7]);

            return workLog;
        }
        public int size()
        {
            return mWorkLogs.Count;
        }

        private const string mFileName = "workLogs.txt";

        private List<WorkLog> mWorkLogs = new List<WorkLog>();
    }
}
