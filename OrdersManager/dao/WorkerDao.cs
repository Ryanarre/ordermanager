﻿using System.Collections.Generic;
using System.IO;

namespace OrdersManager
{
    public class WorkerDao
    {
        public WorkerDao()
        {
            if (!File.Exists(mFileName))
                return;

            using (StreamReader reader = new StreamReader(mFileName))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                    addWorker(deserialize(line), false);
            }
        }

        public void addWorker(Worker worker, bool write)
        {
            if (write)
            {
                int count = mWorkers.Count;
                if (count != 0)
                    worker.ID = mWorkers[mWorkers.Count - 1].ID + 1;
                else
                    worker.ID = 1;
            }

            mWorkers.Add(worker);

            if (write)
                writeToFile();
        }

        public void modifyWorker(Worker worker)
        {
            int index = getIndexByID(worker.ID);

            if (index != -1)
            {
                mWorkers[index] = worker;

                writeToFile();
            }
        }

        public void removeWorker(int ID)
        {
            foreach (Worker worker in mWorkers)
                if (worker.ID == ID)
                {
                    mWorkers.Remove(worker);

                    writeToFile();

                    return;
                }
        }

        public Worker getWorkerByID(int id)
        {
            return mWorkers[getIndexByID(id)];
        }

        private int getIndexByID(int ID)
        {
            for (int i = 0; i < size(); ++i)
                if (mWorkers[i].ID == ID)
                    return i;

            return -1;
        }

        public Worker getWorker(int index)
        {
            return mWorkers[index];
        }

        public int size()
        {
            return mWorkers.Count;
        }
        private void writeToFile()
        {
            using (StreamWriter writetext = new StreamWriter(mFileName))
                foreach (Worker cWorker in mWorkers)
                    writetext.WriteLine(serialize(cWorker));
        }

        private string serialize(Worker worker)
        {
            return worker.ID + "," +
                   worker.Surname + "," +
                   worker.Name + "," +
                   worker.Patronymic + "," +
                   worker.Phone + "," +
                   worker.Email + "," +
                   worker.Title + "," +
                   worker.BirthDate + "," +
                   worker.HireDate + "," +
                   worker.FireDate + "," +
                   worker.Salary + "," +
                   worker.Status + "," +
                   worker.Passport + "," +
                   worker.Comment;
        }

        private Worker deserialize(string str)
        {
            string[] values = str.Split(',');

            Worker worker = new Worker();
            worker.ID = int.Parse(values[0]);
            worker.Surname = values[1];
            worker.Name = values[2];
            worker.Patronymic = values[3];
            worker.Phone = values[4];
            worker.Email = values[5];
            worker.Title = values[6];
            worker.BirthDate = System.DateTime.Parse(values[7]);
            worker.HireDate = System.DateTime.Parse(values[8]);
            worker.FireDate = System.DateTime.Parse(values[9]);
            worker.Salary = double.Parse(values[10]);
            worker.Status = bool.Parse(values[11]);
            worker.Passport = values[12];
            worker.Comment = values[13];

            return worker;
        }

        private List<Worker> mWorkers = new List<Worker>();

        private const string mFileName = "workers.txt";
    }
}
