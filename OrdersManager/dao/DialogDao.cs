﻿using System.Collections.Generic;
using System.IO;

/*
 * TODO: Create base class for DAO - at least writeToFile/serialize/deserialize logic can be generalized (same for DB I suppose)
 */
namespace OrdersManager
{
    public class DetailDao
    {
        public DetailDao(DetailType detailType)
        {
            switch (detailType)
            {
                default:
                case DetailType.PlanJob:
                    mFileName = "planJobs.txt";
                    break;
                case DetailType.PlanLoss:
                    mFileName = "planLosses.txt";
                    break;
                case DetailType.PlanMaterial:
                    mFileName = "planMaterials.txt";
                    break;
            }    

            if (!File.Exists(mFileName))
                return;

            using (StreamReader reader = new StreamReader(mFileName))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    addDetail(
                        deserialize(line), false
                    );
                }
            }
        }
        public bool addDetail(Detail detail, bool write)
        {
            if (write)
            {
                int count = mDetails.Count;
                if (count != 0)
                    detail.ID = mDetails[mDetails.Count - 1].ID + 1;
                else
                    detail.ID = 1;
            }

            mDetails.Add(detail);

            if (write)
                writeToFile();

            return true;
        }
        public Detail getDetail(int index)
        {
            return mDetails[index];
        }

        public Detail getDetailByID(int id)
        {
            foreach (Detail detail in mDetails)
                if (detail.ID == id)
                    return detail;

            return new Detail();
        }

        public void modifyDetail(Detail detail)
        {
            int index = getIndexByID(detail.ID);

            if (index != -1)
            {
                mDetails[index] = detail;

                writeToFile();
            }
        }

        public void removeDetail(int ID)
        {
            foreach (Detail detail in mDetails)
                if (detail.ID == ID)
                {
                    mDetails.Remove(detail);

                    writeToFile();

                    return;
                }
        }

        private int getIndexByID(int id)
        {
            for (int i = 0; i < size(); ++i)
                if (mDetails[i].ID == id)
                    return i;

            return -1;
        }

        private void writeToFile()
        {
            using (StreamWriter writetext = new StreamWriter(mFileName))
                foreach (Detail cDetail in mDetails)
                    writetext.WriteLine(serialize(cDetail));
        }
        private string serialize(Detail detail)
        {
            return detail.ID + "," +
                   (int)detail.Type + "," +
                   detail.Name + "," +
                   detail.Unit + "," +
                   detail.PricePerUnit + "," +
                   detail.Comment + "," +
                   detail.Comment + "," +
                   detail.OrderName;
        }

        private Detail deserialize(string str)
        {
            string[] values = str.Split(',');

            Detail detail = new Detail();
            detail.ID = int.Parse(values[0]);
            detail.Type = (DetailType)int.Parse(values[1]);
            detail.Name = values[2];
            detail.Unit = values[3];
            detail.PricePerUnit = double.Parse(values[4]);
            detail.Comment = values[5];
            detail.Comment = values[6];
            detail.OrderName = values[7];

            return detail;
        }
        public int size()
        {
            return mDetails.Count;
        }

        private string mFileName;

        private List<Detail> mDetails = new List<Detail>();
    }
}
