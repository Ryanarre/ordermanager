﻿using System.Collections.Generic;
using System.IO;

/*
 * TODO: Create base class for DAO - at least writeToFile/serialize/deserialize logic can be generalized (same for DB I suppose)
 */
namespace OrdersManager
{
    public class StockHistoryDao
    {
        public StockHistoryDao()
        {
            if (!File.Exists(mFileName))
                return;

            using (StreamReader reader = new StreamReader(mFileName))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    addStockHistory(
                        deserialize(line), false
                    );
                }
            }
        }
        public bool addStockHistory(StockHistory stockHistory, bool write)
        {
            mStockHistorys.Add(stockHistory);

            if (write)
                writeToFile();

            return true;
        }
        public StockHistory getStockHistory(int index)
        {
            return mStockHistorys[index];
        }

        private void writeToFile()
        {
            using (StreamWriter writetext = new StreamWriter(mFileName))
                foreach (StockHistory cStockHistory in mStockHistorys)
                    writetext.WriteLine(serialize(cStockHistory));
        }
        private string serialize(StockHistory stockHistory)
        {
            return stockHistory.OrderName + "," +
                   stockHistory.Count + "," +
                   stockHistory.StockID + "," +
                   stockHistory.Date.ToString() + "," +
                   stockHistory.IsGuarantee.ToString() + "," +
                   stockHistory.Side.ToString() + "," +
                   stockHistory.Comment + "," +
                   (int)stockHistory.Type;
        }

        private StockHistory deserialize(string str)
        {
            string[] values = str.Split(',');

            StockHistory stockHistory = new StockHistory();
            stockHistory.OrderName = values[0];
            stockHistory.Count = int.Parse(values[1]);
            stockHistory.StockID = int.Parse(values[2]);
            stockHistory.Date = System.DateTime.Parse(values[3]);
            stockHistory.IsGuarantee = bool.Parse(values[4]);
            stockHistory.Side = bool.Parse(values[5]);
            stockHistory.Comment = values[6];
            stockHistory.Type = (StockType)int.Parse(values[7]);

            return stockHistory;
        }
        public int size()
        {
            return mStockHistorys.Count;
        }

        private const string mFileName = "stockHistorys.txt";

        private List<StockHistory> mStockHistorys = new List<StockHistory>();
    }
}
