﻿using System;
using System.Windows.Forms;

namespace OrdersManager
{
    public partial class SalaryDialog : Form
    {
        public SalaryDialog(
                MainWindow parent
            ,   DatabaseManager dbManager
            ,   int workerId
        )
        {
            InitializeComponent();

            mParent = parent;
            mDbManager = dbManager;
            mWorkerId = workerId;

            initGrid();
        }

        private void addMenu_Click(object sender, EventArgs e)
        {
            Form form = new SalaryAddDialog(
                    this
                ,   mDbManager
                ,   mWorkerId
            );

            form.ShowDialog();

            initGrid();
        }

        private void initGrid()
        {
            dataGridView1.Columns.Clear();

            dataGridView1.Columns.Add("date", "Дата");
            dataGridView1.Columns.Add("summ", "Сумма");
            dataGridView1.Columns.Add("title", "Должность");

            for (int i = 0; i < mDbManager.getSalaryDao().size(); ++i)
            {
                Salary salary = mDbManager.getSalaryDao().getSalary(i);
                if (salary.WorkerID != mWorkerId)
                    continue;

                dataGridView1.Rows.Add();

                dataGridView1.Rows[i].Cells[0].Value = salary.Date;
                dataGridView1.Rows[i].Cells[1].Value = salary.Amount;
                dataGridView1.Rows[i].Cells[2].Value = salary.Title;
            }
        }

        public void reset()
        {
            mParent.reset();
        }

        MainWindow mParent;
        DatabaseManager mDbManager;
        int mWorkerId;
    }
}
