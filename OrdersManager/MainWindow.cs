﻿using System;
using System.Windows.Forms;

namespace OrdersManager
{
    public partial class MainWindow : Form
    {
        public MainWindow(
                Form parent
            ,   int role
        )
        {
            mRole = role;
            mParent = parent;

            InitializeComponent();

            switch (role)
            {
                case 0:
                    Text = "Секретарь";
                    break;
                case 1:
                    Text = "Бухгалтер";
                    break;
                case 2:
                    Text = "Конструктор";
                    break;
                case 3:
                    Text = "Директор";
                    break;
            }

            menuClickHandle(MenuElem.Orders);
        }

        private void MainWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            mParent.Close();
        }

        /* ----------------- Grid initializers ---------------------------- */

        private void initOrdersGrid()
        {
            dataGridView1.Columns.Add("ID", "ID");
            dataGridView1.Columns.Add("number", "Номер договора");
            dataGridView1.Columns.Add("name", "Заказчик");
            dataGridView1.Columns.Add("type", "Отдел");
            dataGridView1.Columns.Add("dateOpen", "Дата открытия");

            if (mMenuElem == MenuElem.Orders)
            {
                dataGridView1.Columns.Add("dateClose", "Дата закрытия");
                dataGridView1.Columns.Add("debtClients", "Задолженность заказчика, грн");
                dataGridView1.Columns.Add("debtWorkers", "Задолженность перед работниками, грн");
                dataGridView1.Columns.Add("status", "Статус");
            }

            dataGridView1.Columns.Add("summ", "Сумма договора, грн");

            OrderDao dao = mDbManager.getOrderDao();

            for (int i = 0; i < dao.size(); ++i)
            {
                dataGridView1.Rows.Add();

                Order order = dao.getOrder(i);

                dataGridView1.Rows[i].Cells[0].Value = order.ID;
                dataGridView1.Rows[i].Cells[1].Value = order.Number;
                dataGridView1.Rows[i].Cells[2].Value = order.Name;
                dataGridView1.Rows[i].Cells[3].Value = order.Type;
                dataGridView1.Rows[i].Cells[4].Value = order.StartDate;

                if (mMenuElem == MenuElem.Orders)
                {
                    if (order.Status == OrderStatus.Close)
                        dataGridView1.Rows[i].Cells[5].Value = order.EndDate;


                    double profitSum = 0;
                    for (int j = 0; j < mDbManager.getProfitDao().size(); ++j)
                    {
                        Flow flow = mDbManager.getProfitDao().getFlow(j);

                        if (flow.Number == order.Number)
                            profitSum += flow.Summ;
                    }

                    double chargeSum = 0;
                    for (int j = 0; j < mDbManager.getChargeDao().size(); ++j)
                    {
                        Flow flow = mDbManager.getChargeDao().getFlow(j);

                        if (flow.Number == order.Number)
                            chargeSum += flow.Summ;
                    }

                    dataGridView1.Rows[i].Cells[6].Value = order.Summ - profitSum;
                    dataGridView1.Rows[i].Cells[7].Value = chargeSum;

                    switch (order.Status)
                    {
                        case OrderStatus.Open:
                            dataGridView1.Rows[i].Cells[8].Value = "Открыт";
                            break;
                        case OrderStatus.Close:
                            dataGridView1.Rows[i].Cells[8].Value = "Закрыть";
                            break;
                        default:
                            break;
                    }
                }

                if (mRole > 0)
                {
                    int index = mMenuElem == MenuElem.Orders ? 9 : 5;
                    dataGridView1.Rows[i].Cells[index].Value = order.Summ;
                }

            }
        }

        private void initAccountsGrid()
        {
            dataGridView1.Columns.Clear();

            dataGridView1.Columns.Add("ID", "ID");
            dataGridView1.Columns.Add("number", "Номер р/c");
            dataGridView1.Columns.Add("name", "Наименование счёта");
            dataGridView1.Columns.Add("bankName", "Наименование банка");
            dataGridView1.Columns.Add("orgName", "Наименование организации");
            dataGridView1.Columns.Add("currency", "Валюта");
            dataGridView1.Columns.Add("dateOpen", "Дата открытия");
            dataGridView1.Columns.Add("dateClose", "Дата закрытия");
            dataGridView1.Columns.Add("status", "Статус");
            dataGridView1.Columns.Add("comment", "Примечание");

            for (int i = 0; i < mDbManager.getAccountDao().size(); ++i)
            {
                Account account = mDbManager.getAccountDao().getAccount(i);

                dataGridView1.Rows.Add();
                int index = dataGridView1.Rows.Count - 1;

                dataGridView1.Rows[index].Cells[0].Value = account.ID;
                dataGridView1.Rows[index].Cells[1].Value = account.Number;
                dataGridView1.Rows[index].Cells[2].Value = account.Name;
                dataGridView1.Rows[index].Cells[3].Value = account.Bank;
                dataGridView1.Rows[index].Cells[4].Value = account.Organisation;
                dataGridView1.Rows[index].Cells[4].Value = account.Currency;
                dataGridView1.Rows[index].Cells[5].Value = account.StartDate;

                if (account.Closed)
                    dataGridView1.Rows[index].Cells[6].Value = account.EndDate;

                dataGridView1.Rows[index].Cells[7].Value = account.Closed ?
                    "Закрыт" : "Открыт"
                ;

                dataGridView1.Rows[index].Cells[8].Value = account.Comment;
            }
        }

        private void initFlowsGrid(FlowType type)
        {
            dataGridView1.Columns.Add("ID", "ID");
            dataGridView1.Columns.Add("number", "Номер заказа");
            dataGridView1.Columns.Add("date", "Дата");
            dataGridView1.Columns.Add(
                    "agent"
                ,   type == FlowType.Profit ? "Отправитель" : "Получатель"
            );
            dataGridView1.Columns.Add("purpose", "Целевое назначение");
            dataGridView1.Columns.Add("currency", "Валюта");
            dataGridView1.Columns.Add("summ", "Сумма");
            dataGridView1.Columns.Add("comment", "Комментарий");

            if (mMenuElem == MenuElem.Charges)
            {
                dataGridView1.Columns.Add("type", "Вид начисления");
                dataGridView1.Columns.Add("paid", "Выплачено");
                dataGridView1.Columns.Add("debt", "Задолженность");
                dataGridView1.Columns.Add("status", "Статус");
            }

            FlowDao dao = mDbManager.getFlowDao(type);

            for (int i = 0; i < dao.size(); ++i)
            {
                Flow flow = dao.getFlow(i);

                dataGridView1.Rows.Add();
                int index = dataGridView1.Rows.Count - 1;

                dataGridView1.Rows[index].Cells[0].Value = flow.ID;
                dataGridView1.Rows[index].Cells[1].Value = flow.Number;
                dataGridView1.Rows[index].Cells[2].Value = flow.Date;
                dataGridView1.Rows[index].Cells[3].Value = flow.Agent;
                dataGridView1.Rows[index].Cells[4].Value = flow.Purpose;
                dataGridView1.Rows[index].Cells[5].Value = flow.Currency;
                dataGridView1.Rows[index].Cells[6].Value = flow.Summ;
                dataGridView1.Rows[index].Cells[7].Value = flow.Comment;
                if (mMenuElem == MenuElem.Charges)
                {
                    dataGridView1.Rows[index].Cells[8].Value = flow.WithAgent ?
                        "Выполненная работа" : "ЗП";

                    double summ = 0;
                    for (int j = 0; j < mDbManager.getPaymentDao().size(); ++j)
                    {
                        if (mDbManager.getPaymentDao().getFlow(j).Number == flow.Number)
                        {
                            summ += mDbManager.getPaymentDao().getFlow(j).Summ;
                        }
                    }

                    dataGridView1.Rows[index].Cells[9].Value = summ;
                    dataGridView1.Rows[index].Cells[10].Value = flow.Summ - summ;

                    if (summ == 0)
                        dataGridView1.Rows[index].Cells[10].Value = "Не оплачено";
                    else if (flow.Summ == summ)
                        dataGridView1.Rows[index].Cells[10].Value = "Оплачено";
                    else
                        dataGridView1.Rows[index].Cells[10].Value = "Оплачено частично";
                }
            }
        }

        private void initOrganisationsGrid()
        {
            dataGridView1.Columns.Add("ID", "ID");
            dataGridView1.Columns.Add("name", "Наименование");
            dataGridView1.Columns.Add("type", "Форма");
            dataGridView1.Columns.Add("startDate", "Дата открытия");
            dataGridView1.Columns.Add("endDate", "Дата закрытия");
            dataGridView1.Columns.Add("status", "Статус");
            dataGridView1.Columns.Add("comment", "Примечания");

            OrganisationDao dao = mDbManager.getOrganisationDao();

            for (int i = 0; i < dao.size(); ++i)
            {
                Organisation organisation = dao.getOrganisation(i);

                dataGridView1.Rows.Add();
                int index = dataGridView1.Rows.Count - 1;

                dataGridView1.Rows[index].Cells[0].Value = organisation.ID;
                dataGridView1.Rows[index].Cells[1].Value = organisation.Name;
                dataGridView1.Rows[index].Cells[2].Value = organisation.Type;
                dataGridView1.Rows[index].Cells[3].Value = organisation.StartDate;

                if (organisation.Closed)
                    dataGridView1.Rows[index].Cells[4].Value = organisation.EndDate;

                dataGridView1.Rows[index].Cells[5].Value = organisation.Closed ?
                    "Закрыта" : "Открыта"
                ;

                dataGridView1.Rows[index].Cells[6].Value = organisation.Comment;
            }
        }
        private void initAgentsGrid(AgentType agentType)
        {
            dataGridView1.Columns.Add("ID", "ID");
            dataGridView1.Columns.Add("name", "Наименование");
            dataGridView1.Columns.Add("phone", "Номер телефона");
            dataGridView1.Columns.Add("email", "Электронная почта");

            AgentDao dao = mDbManager.getAgentDao(agentType);

            for (int i = 0; i < dao.size(); ++i)
            {
                Agent agent = dao.getAgent(i);

                dataGridView1.Rows.Add();
                int index = dataGridView1.Rows.Count - 1;

                dataGridView1.Rows[index].Cells[0].Value = agent.ID;
                dataGridView1.Rows[index].Cells[1].Value = agent.Name;
                dataGridView1.Rows[index].Cells[2].Value = agent.Phone;
                dataGridView1.Rows[index].Cells[3].Value = agent.Email;
            }
        }

        private void initReferenceGrid()
        {
            dataGridView1.Columns.Add("ID", "ID");
            dataGridView1.Columns.Add("name", "Наименование");
            dataGridView1.Columns.Add("comment", "Примечание");

            ReferenceDao dao = mDbManager.getReferenceDao(mMenuElem);

            for (int i = 0; i < dao.size(); ++i)
            {
                Reference reference = dao.getReference(i);

                dataGridView1.Rows.Add();
                int index = dataGridView1.Rows.Count - 1;

                dataGridView1.Rows[index].Cells[0].Value = reference.ID;
                dataGridView1.Rows[index].Cells[1].Value = reference.Name;
                dataGridView1.Rows[index].Cells[2].Value = reference.Comment;
            }
        }

        private void initJobTypeGrid()
        {
            dataGridView1.Columns.Add("ID", "ID");
            dataGridView1.Columns.Add("name", "Наименование работы");
            dataGridView1.Columns.Add("unit", "Единица измерения");
            dataGridView1.Columns.Add("pricePerUnit", "Стоимость за единицу");
            dataGridView1.Columns.Add("comment", "Примечание");

            JobTypeDao dao = mDbManager.getJobTypeDao();

            for (int i = 0; i < dao.size(); ++i)
            {
                JobType jobType = dao.getJobType(i);

                dataGridView1.Rows.Add();
                int index = dataGridView1.Rows.Count - 1;

                dataGridView1.Rows[index].Cells[0].Value = jobType.ID;
                dataGridView1.Rows[index].Cells[1].Value = jobType.Name;
                dataGridView1.Rows[index].Cells[2].Value = jobType.Unit;
                dataGridView1.Rows[index].Cells[3].Value = jobType.PricePerUnit;
                dataGridView1.Rows[index].Cells[4].Value = jobType.Comment;
            }
        }

        private void initCalendarGrid()
        {
            dataGridView1.Columns.Add("ID", "ID");
            dataGridView1.Columns.Add("year", "Год");
            dataGridView1.Columns.Add("comment", "Примечание");

            CalendarDao dao = mDbManager.getCalendarDao();

            for (int i = 0; i < dao.size(); ++i)
            {
                Calendar calendar = dao.getCalendar(i);

                dataGridView1.Rows.Add();
                int index = dataGridView1.Rows.Count - 1;

                dataGridView1.Rows[index].Cells[0].Value = calendar.ID;
                dataGridView1.Rows[index].Cells[1].Value = calendar.Year;
                dataGridView1.Rows[index].Cells[2].Value = calendar.Comment;
            }
        }

        private void initWorkLogGrid()
        {
            dataGridView1.Columns.Add("ID", "ID");
            dataGridView1.Columns.Add("date", "Дата");
            dataGridView1.Columns.Add("comment", "Примечание");

            WorkDayDao dao = mDbManager.getWorkDayDao();

            for (int i = 0; i < dao.size(); ++i)
            {
                WorkDay workDay = dao.getWorkDay(i);

                dataGridView1.Rows.Add();
                int index = dataGridView1.Rows.Count - 1;

                dataGridView1.Rows[index].Cells[0].Value = workDay.ID;
                dataGridView1.Rows[index].Cells[1].Value = workDay.Date;
                dataGridView1.Rows[index].Cells[2].Value = workDay.Comment;
            }
        }

        private void initBasicJobGrid()
        {
            dataGridView1.Columns.Add("ID", "ID");
            dataGridView1.Columns.Add("unit", "Количественная характеристика");
            dataGridView1.Columns.Add("name", "Наименование работы");
            dataGridView1.Columns.Add("pricePerUnit", "Объём работы на единицу характеристики");
            dataGridView1.Columns.Add("comment", "Примечание");

            BasicJobDao dao = mDbManager.getBasicJobDao();

            for (int i = 0; i < dao.size(); ++i)
            {
                BasicJob basicJob = dao.getBasicJob(i);

                dataGridView1.Rows.Add();
                int index = dataGridView1.Rows.Count - 1;

                dataGridView1.Rows[index].Cells[0].Value = basicJob.ID;
                dataGridView1.Rows[index].Cells[1].Value = basicJob.Unit;
                dataGridView1.Rows[index].Cells[2].Value = basicJob.Name;
                dataGridView1.Rows[index].Cells[3].Value = basicJob.PricePerUnit;
                dataGridView1.Rows[index].Cells[4].Value = basicJob.Comment;
            }
        }

        private void initStockGrid(StockType type)
        {
            dataGridView1.Columns.Add("ID", "ID");
            dataGridView1.Columns.Add("name", "Наименование");
            dataGridView1.Columns.Add("pricePerUnit", "Балансовая стоимость за единицу");
            dataGridView1.Columns.Add("countBegin", "Количество на начало периода");
            dataGridView1.Columns.Add("income", "Приход");
            dataGridView1.Columns.Add("outcome", "Расход");
            dataGridView1.Columns.Add("countEnd", "Количество на конец периода");

            if (mMenuElem != MenuElem.StockTool)
                dataGridView1.Columns.Add("unit", "Единица измерения");

            StockDao dao = mDbManager.getStockDao(type);

            for (int i = 0; i < dao.size(); ++i)
            {
                Stock stock = dao.getStock(i);

                dataGridView1.Rows.Add();
                int index = dataGridView1.Rows.Count - 1;

                dataGridView1.Rows[index].Cells[0].Value = stock.ID;
                dataGridView1.Rows[index].Cells[1].Value = stock.Name;
                dataGridView1.Rows[index].Cells[2].Value = stock.PricePerUnite;
                dataGridView1.Rows[index].Cells[3].Value = stock.CountBegin;

                int income = 0;
                int outcome = 0;
                for (int j = 0; j < mDbManager.getStockHistoryDao().size(); ++j)
                {
                    StockHistory stockHistory = mDbManager.getStockHistoryDao().getStockHistory(j);

                    if (stockHistory.StockID == stock.ID)
                    {
                        if (stockHistory.Side)
                            income += stockHistory.Count;
                        else
                            outcome += stockHistory.Count;
                    }
                }

                dataGridView1.Rows[index].Cells[4].Value = income;
                dataGridView1.Rows[index].Cells[5].Value = outcome;

                dataGridView1.Rows[index].Cells[6].Value = stock.CountBegin + income - outcome;

                if (mMenuElem != MenuElem.StockTool)
                    dataGridView1.Rows[index].Cells[7].Value = stock.Unit;
            }
        }

        private void initStockInfoGrid(StockType type)
        {
            dataGridView1.Columns.Add("ID", "ID");
            dataGridView1.Columns.Add("title", "Артикул");
            dataGridView1.Columns.Add("name", "Наименование");
            dataGridView1.Columns.Add("unit", "Единица измерения");
            dataGridView1.Columns.Add("comment", "Примечание");

            StockInfoDao dao = mDbManager.getStockInfoDao(type);

            for (int i = 0; i < dao.size(); ++i)
            {
                StockInfo stockInfo = dao.getStockInfo(i);

                dataGridView1.Rows.Add();
                int index = dataGridView1.Rows.Count - 1;

                dataGridView1.Rows[index].Cells[0].Value = stockInfo.ID;
                dataGridView1.Rows[index].Cells[1].Value = stockInfo.Title;
                dataGridView1.Rows[index].Cells[2].Value = stockInfo.Name;
                dataGridView1.Rows[index].Cells[3].Value = stockInfo.Unit;
                dataGridView1.Rows[index].Cells[4].Value = stockInfo.Comment;
            }
        }

        private void initWorkersGrid()
        {
            dataGridView1.Columns.Add("ID", "ID");
            dataGridView1.Columns.Add("surname", "Фамилия");
            dataGridView1.Columns.Add("name", "Имя");
            dataGridView1.Columns.Add("patronymic", "Отчество");
            dataGridView1.Columns.Add("birthDate", "Дата рождения");
            dataGridView1.Columns.Add("phone", "Номер телефона");
            dataGridView1.Columns.Add("email", "Электронная почта");
            dataGridView1.Columns.Add("hireDate", "Дата приёма на работу");
            dataGridView1.Columns.Add("fireDate", "Дата увольнения");
            dataGridView1.Columns.Add("title", "Должность");
            dataGridView1.Columns.Add("status", "Статус");
            dataGridView1.Columns.Add("salary", "Ставка");

            WorkerDao dao = mDbManager.getWorkerDao();

            for (int i = 0; i < dao.size(); ++i)
            {
                Worker worker = dao.getWorker(i);

                dataGridView1.Rows.Add();
                int index = dataGridView1.Rows.Count - 1;

                dataGridView1.Rows[index].Cells[0].Value = worker.ID;
                dataGridView1.Rows[index].Cells[1].Value = worker.Surname;
                dataGridView1.Rows[index].Cells[2].Value = worker.Name;
                dataGridView1.Rows[index].Cells[3].Value = worker.Patronymic;
                dataGridView1.Rows[index].Cells[4].Value = worker.BirthDate;
                dataGridView1.Rows[index].Cells[5].Value = worker.Phone;
                dataGridView1.Rows[index].Cells[6].Value = worker.Email;
                dataGridView1.Rows[index].Cells[7].Value = worker.HireDate;

                if (!worker.Status)
                    dataGridView1.Rows[index].Cells[8].Value = worker.FireDate;

                dataGridView1.Rows[index].Cells[9].Value = worker.Title;
                dataGridView1.Rows[index].Cells[10].Value = worker.Status ? "Работает" : "Уволен";
                dataGridView1.Rows[index].Cells[11].Value = worker.Salary;
            }
        }

        /* ------------------------ Service functions ------------------- */
        private void showDialog(bool isNew)
        {
            int ID = 0;

            if (!isNew)
            {
                int index = 0;
                if (!detectCurrentRow(ref index))
                    return;

                ID = (int)dataGridView1.Rows[index].Cells[0].Value;
            }

            Form form = new Form();
            switch (mMenuElem)
            {
                case MenuElem.Department:
                    form = new ReferenceDialog(
                            mDbManager.getDepartmentDao()
                        ,   ID
                    );
                    break;
                case MenuElem.PurposeFinancial:
                    form = new ReferenceDialog(
                            mDbManager.getPurposeFinancialDao()
                        , ID
                    );
                    break;
                case MenuElem.PurposeMaterial:
                    form = new ReferenceDialog(
                            mDbManager.getPurposeMaterialDao()
                        ,   ID
                    );
                    break;
                case MenuElem.BasicJob:
                    form = new BasicJobDialog(
                          mDbManager
                        , ID
                    );
                    break;
                case MenuElem.WorkLog:
                    form = new WorkLogDialog(
                          mDbManager
                        , ID
                    );
                    break;
                case MenuElem.Holidays:
                    form = new CalendarDialog(
                          mDbManager
                        , ID
                    );
                    break;
                case MenuElem.JobType:
                    form = new JobTypeDialog(
                          mDbManager.getJobTypeDao()
                        , ID
                    );
                    break;
                case MenuElem.Accounts:
                    form = new AccountAddDialog(
                            mDbManager
                        ,   ID
                    );
                    break;
                case MenuElem.Profits:
                    form = new FlowDialog(
                            mDbManager
                        ,   FlowType.Profit
                        ,   ID
                    );
                    break;
                case MenuElem.Losses:
                    form = new FlowDialog(
                            mDbManager
                        ,   FlowType.Loss
                        ,   ID
                    );
                    break;
                case MenuElem.Charges:
                    {
                        if (isNew)
                        {
                            form = new SalaryChangeDialog(
                                  mDbManager
                                , ID
                            );
                        }
                        else
                        {
                            if (mDbManager.getChargeDao().getFlowByID(ID).WithAgent)
                            {
                                form = new ChargeDialog(
                                      mDbManager
                                    , ID
                                );
                            }
                            else
                            {
                                form = new SalaryChangeDialog(
                                      mDbManager
                                    , ID
                                );
                            }
                        }
                        break;
                    }
                case MenuElem.Payments:
                    form = new FlowDialog(
                            mDbManager
                        ,   FlowType.Payment
                        ,   ID
                    );
                    break;
                case MenuElem.Supplier:
                    form = new AgentDialog(
                            mDbManager.getSupplierDao()
                        ,   AgentType.Supplier
                        ,   ID
                    );
                    break;
                case MenuElem.Customers:
                    form = new AgentDialog(
                            mDbManager.getCustomerDao()
                        ,   AgentType.Customer
                        ,   ID
                    );
                    break;
                case MenuElem.Contractors:
                    form = new AgentDialog(
                            mDbManager.getContractorDao()
                        ,   AgentType.Contractor
                        ,   ID
                    );
                    break;
                case MenuElem.Organisations:
                    form = new OrganisationDialog(
                            mDbManager.getOrganisationDao()
                        ,   ID
                    );
                    break;
                case MenuElem.Workers:
                    form = new WorkerDialog(
                            mDbManager.getWorkerDao()
                        ,   ID
                    );
                    break;
                case MenuElem.Material:
                    form = new StockInfoDialog(
                            mDbManager.getMaterialInfoDao()
                        ,   ID
                    );
                    break;
                case MenuElem.Tool:
                    form = new StockInfoDialog(
                            mDbManager.getToolInfoDao()
                        , ID
                    );
                    break;
                case MenuElem.CustomTool:
                    form = new StockInfoDialog(
                            mDbManager.getCustomMaterialInfoDao()
                        , ID
                    );
                    break;
                case MenuElem.Trim:
                    form = new StockInfoDialog(
                            mDbManager.getTrimInfoDao()
                        , ID
                    );
                    break;
                case MenuElem.StockMaterial:
                    if (isNew)
                    {
                        form = new StockDialog(
                                mDbManager
                            , StockType.Material
                            , ID
                        );
                    }
                    else
                    {
                        form = new StockLossDialog(
                                this
                            , mDbManager
                            , StockType.Material
                            , ID
                            , false
                        );
                    }
                    break;
                case MenuElem.StockTool:
                    if (isNew)
                    {
                        form = new StockDialog(
                                mDbManager
                            , StockType.Tool
                            , ID
                        );
                    }
                    else
                    {
                        form = new StockLossDialog(
                                this
                            , mDbManager
                            , StockType.Tool
                            , ID
                            , false
                        );
                    }
                    break;
                case MenuElem.StockCustomTool:
                    if (isNew)
                    {
                        form = new StockDialog(
                                mDbManager
                            , StockType.CustomMaterial
                            , ID
                        );
                    }
                    else
                    {
                        form = new StockLossDialog(
                                this
                            , mDbManager
                            , StockType.CustomMaterial
                            , ID
                            , false
                        );
                    }
                    break;
                case MenuElem.StockTrim:
                    if (isNew)
                    {
                        form = new StockDialog(
                                mDbManager
                            , StockType.CustomMaterial
                            , ID
                        );
                    }
                    else
                    {
                        form = new StockLossDialog(
                                this
                            , mDbManager
                            , StockType.CustomMaterial
                            , ID
                            , false
                        );
                    }
                    break;
                default: // TODO: Should be either Orders or OrderTemplates and throw error otherwise
                    {
                    form  = new OrderDialog(
                            mDbManager.getOrderDao()
                        ,   mDbManager.getCustomerDao()
                        ,   ID
                    );
                        break;
                }
            }

            form.ShowDialog(this);

            form.Dispose();
        }

        public void reset()
        {
            menuClickHandle(mMenuElem);
        }

        private void menuClickHandle(MenuElem menuElem)
        {
            dataGridView1.Columns.Clear();

            mMenuElem = menuElem;

            switch (menuElem)
            {
                case MenuElem.Department:
                case MenuElem.PurposeMaterial:
                case MenuElem.PurposeFinancial:
                    initReferenceGrid();
                    break;
                case MenuElem.BasicJob:
                    initBasicJobGrid();
                    break;
                case MenuElem.WorkLog:
                    initWorkLogGrid();
                    break;
                case MenuElem.Holidays:
                    initCalendarGrid();
                    break;
                case MenuElem.JobType:
                    initJobTypeGrid();
                    break;
                case MenuElem.Accounts:
                    initAccountsGrid();
                    break;
                case MenuElem.Orders:
                default:
                    initOrdersGrid();
                    break;
                case MenuElem.Profits:
                    initFlowsGrid(FlowType.Profit);
                    break;
                case MenuElem.Losses:
                    initFlowsGrid(FlowType.Loss);
                    break;
                case MenuElem.Charges:
                    initFlowsGrid(FlowType.Charge);
                    break;
                case MenuElem.Payments:
                    initFlowsGrid(FlowType.Payment);
                    break;
                case MenuElem.Organisations:
                    initOrganisationsGrid();
                    break;
                case MenuElem.Supplier:
                    initAgentsGrid(AgentType.Supplier);
                    break;
                case MenuElem.Customers:
                    initAgentsGrid(AgentType.Customer);
                    break;
                case MenuElem.Contractors:
                    initAgentsGrid(AgentType.Contractor);
                    break;
                case MenuElem.Workers:
                    initWorkersGrid();
                    break;
                case MenuElem.StockMaterial:
                    initStockGrid(StockType.Material);
                    break;
                case MenuElem.StockTool:
                    initStockGrid(StockType.Tool);
                    break;
                case MenuElem.StockCustomTool:
                    initStockGrid(StockType.CustomMaterial);
                    break;
                case MenuElem.StockTrim:
                    initStockGrid(StockType.Trim);
                    break;
                case MenuElem.Material:
                    initStockInfoGrid(StockType.Material);
                    break;
                case MenuElem.Tool:
                    initStockInfoGrid(StockType.Tool);
                    break;
                case MenuElem.CustomTool:
                    initStockInfoGrid(StockType.CustomMaterial);
                    break;
                case MenuElem.Trim:
                    initStockInfoGrid(StockType.Trim);
                    break;
                    // TODO: Maybe add default clause with error throw
            }

            initWidgetsVisibility();

            resetMenus();
        }

        private void resetMenus()
        {
            /* -------------------- Simple menus --------------------------- */

            ordersMenu.BackColor = getMenuColor(mMenuElem == MenuElem.Orders);

            workersMenu.BackColor = getMenuColor(mMenuElem == MenuElem.Workers);

            organisationsMenu.BackColor = getMenuColor(mMenuElem == MenuElem.Organisations);

            accountMenu.BackColor = getMenuColor(mMenuElem == MenuElem.Accounts);

            jobListMenu.BackColor = getMenuColor(mMenuElem == MenuElem.JobType);

            baseJobMenu.BackColor = getMenuColor(mMenuElem == MenuElem.BasicJob);

            jobDoneMenu.BackColor = getMenuColor(mMenuElem == MenuElem.JobDone);

            /* ---------------- Contragents [Complex] ---------------------- */

            agentsMenu.BackColor = getMenuColor(
                mMenuElem == MenuElem.Supplier ||
                mMenuElem == MenuElem.Customers ||
                mMenuElem == MenuElem.Contractors
            );

            suppliersMenu.BackColor = getMenuColor(mMenuElem == MenuElem.Supplier);

            customersMenu.BackColor = getMenuColor(mMenuElem == MenuElem.Customers);

            contractorMenu.BackColor = getMenuColor(mMenuElem == MenuElem.Contractors);

            /* ---------------- Stock [Complex] -------------------------- */

            stockMenu.BackColor = getMenuColor(
                mMenuElem == MenuElem.StockMaterial ||
                mMenuElem == MenuElem.StockTool ||
                mMenuElem == MenuElem.StockCustomTool ||
                mMenuElem == MenuElem.StockTrim
            ); 

            stockMaterialMenu.BackColor = getMenuColor(mMenuElem == MenuElem.StockMaterial);

            stockToolMenu.BackColor = getMenuColor(mMenuElem == MenuElem.StockTool);

            stockCustomToolMenu.BackColor = getMenuColor(mMenuElem == MenuElem.StockCustomTool);

            trimMenu.BackColor = getMenuColor(mMenuElem == MenuElem.StockTrim);

            /* ---------------- Stock info [Complex] -------------------------- */

            materialListMenu.BackColor = getMenuColor(
                mMenuElem == MenuElem.Material ||
                mMenuElem == MenuElem.Tool ||
                mMenuElem == MenuElem.CustomTool ||
                mMenuElem == MenuElem.Trim
            );

            materialInfoMenu.BackColor = getMenuColor(mMenuElem == MenuElem.Material);

            toolInfoMenu.BackColor = getMenuColor(mMenuElem == MenuElem.Tool);

            givenMaterialMenu.BackColor = getMenuColor(mMenuElem == MenuElem.CustomTool);

            trimInfoMenu.BackColor = getMenuColor(mMenuElem == MenuElem.Trim);

            /* ---------------- Reference [Complex] -------------------------- */

            referenceMenu.BackColor = getMenuColor(
                mMenuElem == MenuElem.Department ||
                mMenuElem == MenuElem.PurposeFinancial ||
                mMenuElem == MenuElem.PurposeMaterial
            );

            departmentMenu.BackColor = getMenuColor(mMenuElem == MenuElem.Department);

            purposeFinancialMenu.BackColor = getMenuColor(mMenuElem == MenuElem.PurposeFinancial);

            purposeMaterialMenu.BackColor = getMenuColor(mMenuElem == MenuElem.PurposeMaterial);

            /* ---------------- Work Log [Complex] -------------------------- */

            workLogMenu.BackColor = getMenuColor(
                mMenuElem == MenuElem.WorkLog ||
                mMenuElem == MenuElem.Holidays ||
                mMenuElem == MenuElem.SalaryCharge
            );

            dailyReportMenu.BackColor = getMenuColor(mMenuElem == MenuElem.WorkLog);

            holidayMenu.BackColor = getMenuColor(mMenuElem == MenuElem.Holidays);

            salaryChangeMenu.BackColor = getMenuColor(mMenuElem == MenuElem.SalaryCharge);

            /* ---------------- Flows [Complex] -------------------------- */

            operationsMenu.BackColor = getMenuColor(
                mMenuElem == MenuElem.Profits ||
                mMenuElem == MenuElem.Losses ||
                mMenuElem == MenuElem.Charges ||
                mMenuElem == MenuElem.Payments
            );

            profitsMenu.BackColor = getMenuColor(mMenuElem == MenuElem.Profits);

            lossesMenu.BackColor = getMenuColor(mMenuElem == MenuElem.Losses);

            chargesMenu.BackColor = getMenuColor(mMenuElem == MenuElem.Charges);

            paymentsMenu.BackColor = getMenuColor(mMenuElem == MenuElem.Payments);
        }

        private System.Drawing.Color getMenuColor(bool isActive)
        {
            return isActive ?
                System.Drawing.Color.Gray :
                System.Drawing.Color.Transparent
            ;
        }

        private void initWidgetVisibility(
                ToolStripMenuItem menuItem
            ,   string text
        )
        {
            menuItem.Text = text;
            menuItem.Visible = text != "";
        }

        private void initWidgetsVisibility(
                string addTxt
            ,   string editTxt
            ,   string removeTxt
            ,   string detailsTxt
            ,   string nextStepTxt
        )
        {
            initWidgetVisibility(addMenu, addTxt);
            initWidgetVisibility(editMenu, editTxt);
            initWidgetVisibility(removeMenu, removeTxt);
            initWidgetVisibility(detailsMenu, detailsTxt);
            initWidgetVisibility(nextStepMenu, nextStepTxt);
        }

        private void initWidgetsVisibility()
        {
            switch (mMenuElem)
            {
                default:
                    {
                        initWidgetsVisibility(
                              "Добавить"
                            , "Редактировать"
                            , "Удалить"
                            , ""
                            , ""
                        );

                        break;
                    }
                case MenuElem.Orders:
                    {
                        initWidgetsVisibility(
                              "Добавить"
                            , "Редактировать"
                            , "Удалить"
                            , "Подробно"
                            , "Закрыть"
                        );

                        break;
                    }
                case MenuElem.Organisations:
                    {
                        initWidgetsVisibility(
                              "Добавить"
                            , "Редактировать"
                            , "Удалить"
                            , ""
                            , "Закрыть"
                        );

                        break;
                    }
                case MenuElem.Workers:
                    {
                        initWidgetsVisibility(
                              "Добавить"
                            , "Редактировать"
                            , "Удалить"
                            , "Изменить ставку"
                            , "Возобновить/уволить"
                        );

                        break;
                    }
                case MenuElem.Profits:
                case MenuElem.Losses:
                case MenuElem.Payments:
                    {
                        initWidgetsVisibility(
                              "Провести"
                            , "Редактировать"
                            , "Удалить"
                            , ""
                            , ""
                        );

                        break;
                    }
                case MenuElem.Charges:
                    {
                        initWidgetsVisibility(
                              "Начислить ЗП"
                            , "Редактировать"
                            , "Удалить"
                            , "Начислить оплату"
                            , "Провести"
                        );

                        break;
                    }
                case MenuElem.StockMaterial:
                case MenuElem.StockTool:
                case MenuElem.StockCustomTool:
                case MenuElem.StockTrim:
                    {
                        initWidgetsVisibility(
                              "Приход"
                            , "Расход"
                            , "Списание"
                            , "Инвертаризация"
                            , ""
                        );

                        break;
                    }
            }
        }

        private void removeRow(int index, bool preConfirmed = false)
        {
            int ID = (int)dataGridView1.Rows[index].Cells[0].Value;

            switch (mMenuElem)
            {
                case MenuElem.Department:
                    if (!KillProcess("удалить"))
                        return;
                    mDbManager.getDepartmentDao().removeReference(ID);
                    break;
                case MenuElem.PurposeMaterial:
                    if (!KillProcess("удалить"))
                        return;
                    mDbManager.getPurposeMaterialDao().removeReference(ID);
                    break;
                case MenuElem.PurposeFinancial:
                    if (!KillProcess("удалить"))
                        return;
                    mDbManager.getPurposeFinancialDao().removeReference(ID);
                    break;
                case MenuElem.JobType:
                    if (!KillProcess("удалить"))
                        return;
                    mDbManager.getJobTypeDao().removeJobType(ID);
                    break;
                case MenuElem.BasicJob:
                    if (!KillProcess("удалить"))
                        return;
                    mDbManager.getBasicJobDao().removeBasicJob(ID);
                    break;
                case MenuElem.WorkLog:
                    if (!KillProcess("удалить"))
                        return;
                    mDbManager.getWorkDayDao().removeWorkDay(ID);
                    break;
                case MenuElem.Holidays:
                    if (!KillProcess("удалить"))
                        return;
                    mDbManager.getCalendarDao().removeCalendar(ID);
                    mDbManager.getHolidayDao().removeHolidays(ID);
                    break;
                case MenuElem.Material:
                    if (!KillProcess("удалить"))
                        return;
                    mDbManager.getMaterialInfoDao().removeStockInfo(ID);
                    break;
                case MenuElem.Tool:
                    if (!KillProcess("удалить"))
                        return;
                    mDbManager.getToolInfoDao().removeStockInfo(ID);
                    break;
                case MenuElem.CustomTool:
                    if (!KillProcess("удалить"))
                        return;
                    mDbManager.getCustomMaterialInfoDao().removeStockInfo(ID);
                    break;
                case MenuElem.Trim:
                    if (!KillProcess("удалить"))
                        return;
                    mDbManager.getTrimInfoDao().removeStockInfo(ID);
                    break;
                case MenuElem.Accounts:
                    if (!KillProcess("удалить"))
                        return;
                    mDbManager.getAccountDao().removeAccount(ID);
                    break;
                case MenuElem.Supplier:
                    if (!KillProcess("удалить"))
                        return;
                    mDbManager.getSupplierDao().removeAgent(ID);
                    break;
                case MenuElem.Customers:
                    if (!KillProcess("удалить"))
                        return;
                    mDbManager.getCustomerDao().removeAgent(ID);
                    break;
                case MenuElem.Contractors:
                    if (!KillProcess("удалить"))
                        return;
                    mDbManager.getContractorDao().removeAgent(ID);
                    break;
                case MenuElem.Orders:
                    if (!preConfirmed)
                        if (!KillProcess("удалить"))
                            return;

                    mDbManager.getOrderDao().removeOrder(ID);
                    break;
                case MenuElem.Organisations:
                    if (!KillProcess("удалить"))
                        return;

                    string orgName = mDbManager.getOrganisationDao().getOrganisationByID(ID).Name;

                    for (int i = 0; i < mDbManager.getAccountDao().size(); ++i)
                        if (mDbManager.getAccountDao().getAccount(i).Organisation == orgName)
                        {
                            {
                                MessageBox.Show(
                                    "Невозможно удалить организацию, для которой существуют счета"
                                  , "Ошибка"
                                  , MessageBoxButtons.OK
                                  , MessageBoxIcon.Error
                                );

                                return;
                            }
                        }

                        mDbManager.getOrganisationDao().removeOrganisation(ID);
                        break;
                case MenuElem.Workers:
                    if (!KillProcess("удалить"))
                        return;
                    mDbManager.getWorkerDao().removeWorker(ID);
                    break;
                case MenuElem.Profits:
                    if (!KillProcess("удалить"))
                        return;
                    mDbManager.getProfitDao().removeFlow(ID);
                    break;
                case MenuElem.Losses:
                    if (!KillProcess("удалить"))
                        return;
                    mDbManager.getLossDao().removeFlow(ID);
                    break;
                case MenuElem.Charges:
                    if (!KillProcess("удалить"))
                        return;
                    mDbManager.getChargeDao().removeFlow(ID);
                    break;
                case MenuElem.Payments:
                    if (!KillProcess("удалить"))
                        return;
                    mDbManager.getPaymentDao().removeFlow(ID);
                    break;
                case MenuElem.StockMaterial:
                    {
                        StockLossDialog sld = new StockLossDialog(
                                this
                            , mDbManager
                            , StockType.Material
                            , ID
                            , true
                        );
                        sld.ShowDialog();

                        break;
                    }
                case MenuElem.StockTool:
                    {
                        StockLossDialog sld = new StockLossDialog(
                                this
                            , mDbManager
                            , StockType.Tool
                            , ID
                            , true
                        );
                        sld.ShowDialog();

                        break;
                    }
                case MenuElem.StockCustomTool:
                    {
                        StockLossDialog sld = new StockLossDialog(
                                this
                            ,   mDbManager
                            ,   StockType.CustomMaterial
                            ,   ID
                            ,   true
                        );
                        sld.ShowDialog();

                        break;
                    }
                case MenuElem.StockTrim:
                    {
                        StockLossDialog sld = new StockLossDialog(
                                this
                            , mDbManager
                            , StockType.CustomMaterial
                            , ID
                            , true
                        );
                        sld.ShowDialog();

                        break;
                    }
            }
        }

        private void removeCurrentRow()
        {
            int index = 0;
            if (detectCurrentRow(ref index))
                removeRow(index);
        }

        /* TODO: We use this func in other Forms too, so either make it Base or move to Utils */
        private bool detectCurrentRow(ref int index)
        {
            DataGridViewCell cell = dataGridView1.CurrentCell;

            if (cell == null)
                return false;

            index = cell.RowIndex;
            return true;
        }

        /* ---------------- Vertical menu handlers ----------------- */

        private void profitsMenu_Click(object sender, EventArgs e)
        {
            menuClickHandle(MenuElem.Profits);
        }
        private void lossesMenu_Click(object sender, EventArgs e)
        {
            menuClickHandle(MenuElem.Losses);
        }

        private void chargesMenu_Click(object sender, EventArgs e)
        {
            menuClickHandle(MenuElem.Charges);
        }

        private void paymentsMenu_Click(object sender, EventArgs e)
        {
            menuClickHandle(MenuElem.Payments);
        }

        private void ordersMenu_Click(object sender, EventArgs e)
        {
            menuClickHandle(MenuElem.Orders);
        }

        private void organisationsMenu_Click(object sender, EventArgs e)
        {
            menuClickHandle(MenuElem.Organisations);
        }
        private void suppliersMenu_Click(object sender, EventArgs e)
        {
            menuClickHandle(MenuElem.Supplier);
        }

        private void customersMenu_Click(object sender, EventArgs e)
        {
            menuClickHandle(MenuElem.Customers);
        }

        private void contractorMenu_Click(object sender, EventArgs e)
        {
            menuClickHandle(MenuElem.Contractors);
        }

        private void workersMenu_Click(object sender, EventArgs e)
        {
            menuClickHandle(MenuElem.Workers);
        }

        private void stockMaterialMenu_Click(object sender, EventArgs e)
        {
            menuClickHandle(MenuElem.StockMaterial);
        }

        private void stockToolMenu_Click(object sender, EventArgs e)
        {
            menuClickHandle(MenuElem.StockTool);
        }

        private void stockCustomToolMenu_Click(object sender, EventArgs e)
        {
            menuClickHandle(MenuElem.StockCustomTool);
        }

        private void trimMenu_Click(object sender, EventArgs e)
        {
            menuClickHandle(MenuElem.StockTrim);
        }
        private void accountMenu_Click(object sender, EventArgs e)
        {
            menuClickHandle(MenuElem.Accounts);
        }

        private void jobListMenu_Click(object sender, EventArgs e)
        {
            menuClickHandle(MenuElem.JobType);
        }

        private void baseJobMenu_Click(object sender, EventArgs e)
        {
            menuClickHandle(MenuElem.BasicJob);
        }

        private void materialInfoMenu_Click(object sender, EventArgs e)
        {
            menuClickHandle(MenuElem.Material);
        }

        private void givenMaterialMenu_Click(object sender, EventArgs e)
        {
            menuClickHandle(MenuElem.CustomTool);
        }

        private void trimInfoMenu_Click(object sender, EventArgs e)
        {
            menuClickHandle(MenuElem.Trim);
        }

        private void toolInfoMenu_Click(object sender, EventArgs e)
        {
            menuClickHandle(MenuElem.Tool);
        }

        private void departmentMenu_Click(object sender, EventArgs e)
        {
            menuClickHandle(MenuElem.Department);
        }

        private void purposeMaterialMenu_Click(object sender, EventArgs e)
        {
            menuClickHandle(MenuElem.PurposeMaterial);
        }

        private void purposeFinancialMenu_Click(object sender, EventArgs e)
        {
            menuClickHandle(MenuElem.PurposeFinancial);
        }

        private void dailyReportMenu_Click(object sender, EventArgs e)
        {
            menuClickHandle(MenuElem.WorkLog);
        }

        private void holidayMenu_Click(object sender, EventArgs e)
        {
            menuClickHandle(MenuElem.Holidays);
        }

        private void salaryChangeMenu_Click(object sender, EventArgs e)
        {
            menuClickHandle(MenuElem.SalaryCharge);
        }

        private void jobDoneMenu_Click(object sender, EventArgs e)
        {
            menuClickHandle(MenuElem.JobDone);
        }

        /* --------------------- Horizontal menu handlers ------------------- */

        private void addMenu_Click(object sender, EventArgs e)
        {
            showDialog(true);

            menuClickHandle(mMenuElem);
        }

        private void editMenu_Click(object sender, EventArgs e)
        {
            showDialog(false);

            menuClickHandle(mMenuElem);
        }

        private void removeMenu_Click(object sender, EventArgs e)
        {
            removeCurrentRow();

            menuClickHandle(mMenuElem);
        }

        private void detailsMenu_Click(object sender, EventArgs e)
        {
            Form form = new Form();
            if (mMenuElem == MenuElem.Charges)
            {
                form = new ChargeDialog(
                    mDbManager, 0
                );

                form.ShowDialog();

                menuClickHandle(mMenuElem);

                return;
            }

            int index = 0;
            if (!detectCurrentRow(ref index))
                return;

            switch (mMenuElem)
            {
                case MenuElem.Orders:
                    form = new DetailDialog(
                            mDbManager
                        ,   mDbManager.getOrderDao().getOrderByID(
                                (int)dataGridView1.Rows[index].Cells[0].Value
                            ).Number
                        ,   true
                    );
                    break;
                case MenuElem.Workers:
                    form = new SalaryDialog(
                            this
                        ,   mDbManager
                        ,   (int)dataGridView1.Rows[index].Cells[0].Value
                    );
                    break;
            }    

            form.ShowDialog();
        }

        private void nextStepMenu_Click(object sender, EventArgs e)
        {
            int index = 0;
            if (detectCurrentRow(ref index))
            {
                int ID = (int)dataGridView1.Rows[index].Cells[0].Value;

                switch (mMenuElem)
                {
                    case MenuElem.Orders:
                        {
                            if (!KillProcess("закрыть"))
                                return;

                            Order order = mDbManager.getOrderDao().getOrderByID(ID);

                            order.Status = OrderStatus.Close;
                            order.EndDate = DateTime.Now;
                            mDbManager.getOrderDao().modifyOrder(order);

                            break;
                        }
                    case MenuElem.Organisations:
                        {
                            if (!KillProcess("закрыть"))
                                return;

                            // TODO: Implement Close method in DAO
                            Organisation organisation = mDbManager.getOrganisationDao().getOrganisationByID(ID);

                            organisation.Closed = true;
                            organisation.EndDate = DateTime.Now;
                            mDbManager.getOrganisationDao().modifyOrganisation(organisation);

                            break;
                        }
                    case MenuElem.Workers:
                        {
                            if (!KillProcess("уволить/возобновить"))
                                return;

                            Worker worker = mDbManager.getWorkerDao().getWorkerByID(ID);

                            if (worker.Status)
                                worker.FireDate = DateTime.Now;
                            worker.Status = !worker.Status;

                            mDbManager.getWorkerDao().modifyWorker(worker);

                            break;
                        }
                }
            }

            menuClickHandle(mMenuElem);
        }

        private bool KillProcess(string action)
        {
            DialogResult result = MessageBox.Show(
                    "Действительно хотите " + action + "?"
                ,   "Подтвердите действие"
                ,   MessageBoxButtons.YesNo
                ,   MessageBoxIcon.Question
            );

            return result == DialogResult.Yes;
        }

        /* ------------------------- Private fields -------------------- */

        private DatabaseManager mDbManager = new DatabaseManager();

        private MenuElem mMenuElem = MenuElem.Orders;

        private int mRole;
        /*
        * 0 = Секретарь
        * 1 = Бухгалтер
        * 2 = Конструктор
        * 3 = Директор
        */

        private Form mParent;
    }
}
