﻿using System;
using System.Windows.Forms;

namespace OrdersManager
{
    public partial class ChargeDialog : Form
    {
        public ChargeDialog(
                DatabaseManager dbManager
            ,   int ID
        )
        {
            InitializeComponent();

            mDbManager = dbManager;

            mID = ID;

            for (int i = 0; i < dbManager.getContractorDao().size(); ++i)
                contractorCmb.Items.Add(dbManager.getContractorDao().getAgent(i).Name);

            for (int i = 0; i < dbManager.getOrderDao().size(); ++i)
                orderCmb.Items.Add(dbManager.getOrderDao().getOrder(i).Number);

            if (mID != 0)
            {
                Flow flow = mDbManager.getChargeDao().getFlowByID(mID);

                contractorCmb.Text = flow.Agent;
                workNameTxt.Text = flow.Purpose;
                orderCmb.Text = flow.Number;
                unitTxt.Text = flow.Unit;
                countTxt.Text = (flow.Summ / flow.PricePerUnit).ToString();
                summTxt.Text = flow.Summ.ToString();
            }
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            Flow flow = new Flow();

            int count = 0;

            flow.Agent = contractorCmb.Text;
            flow.Purpose = workNameTxt.Text;
            flow.Number = orderCmb.Text;
            flow.Unit = unitTxt.Text;
            flow.WithAgent = true;

            if (!double.TryParse(summTxt.Text, out flow.Summ) ||
                !int.TryParse(countTxt.Text, out count)
            )
            {
                MessageBox.Show(
                    "Невозможно добавить начисление: некорректный формат сумм"
                  , "Ошибка"
                  , MessageBoxButtons.OK
                  , MessageBoxIcon.Error
                );
                return;
            }

            flow.PricePerUnit = flow.Summ / count;

            if (mID != 0)
                mDbManager.getChargeDao().modifyFlow(flow);
            else
                mDbManager.getChargeDao().addFlow(flow, true);

            Close();
        }

        private void skipBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private DatabaseManager mDbManager;

        private int mID;
    }
}
