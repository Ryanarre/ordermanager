﻿using System;
using System.Windows.Forms;

namespace OrdersManager
{
    public partial class AccountAddDialog : Form
    {
        public AccountAddDialog(
              DatabaseManager dbManager
            , int number
        )
        {
            InitializeComponent();

            mDbManager = dbManager;
            mID = number;

            Text = mID != 0 ?
                "Редактирование счёта" :
                "Добавление счёта"
            ;

            orgCmb.Items.Clear();
            for (int i = 0; i < mDbManager.getOrganisationDao().size(); ++i)
                orgCmb.Items.Add(
                    mDbManager.getOrganisationDao().getOrganisation(i).Name
                ); 

            if (mID != 0)
            {
                Account account = mDbManager.getAccountDao().getAccountByID(number);

                numberTxt.Text = account.Number;
                nameTxt.Text = account.Name;
                bankTxt.Text = account.Bank;
                orgCmb.Text = account.Organisation;
                currencyCmb.Text = account.Currency;
                openDate.Value = account.StartDate;
                commentTxt.Text = account.Comment;
            }
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            Account account = new Account();

            if (
                nameTxt.Text.Length == 0 ||
                bankTxt.Text.Length == 0 ||
                orgCmb.Text.Length == 0 ||
                currencyCmb.Text.Length == 0
            )
            {
                MessageBox.Show("Не все обязательные поля заполнены!");

                return;
            }

            account.Number = numberTxt.Text;
            account.ID = mID;
            account.Name = nameTxt.Text;
            account.Bank = bankTxt.Text;
            account.Organisation = orgCmb.Text;
            account.Currency = currencyCmb.Text;
            account.StartDate = openDate.Value;
            account.Comment = commentTxt.Text;

            if (mID != 0)
                mDbManager.getAccountDao().modifyAccount(account);
            else
                mDbManager.getAccountDao().addAccount(account, true);

            Close();
        }

        private void skipBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private DatabaseManager mDbManager;

        private int mID;
    }
}
