﻿
namespace OrdersManager
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.ordersMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.operationsMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.profitsMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.lossesMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.chargesMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.paymentsMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.workersMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.agentsMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.suppliersMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.customersMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.contractorMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.organisationsMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.stockMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.stockMaterialMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.stockToolMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.stockCustomToolMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.trimMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.accountMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.materialListMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.materialInfoMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.givenMaterialMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.trimInfoMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolInfoMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.jobListMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.referenceMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.departmentMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.purposeMaterialMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.purposeFinancialMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.baseJobMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.workLogMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.dailyReportMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.holidayMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.salaryChangeMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.jobDoneMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.startDate = new System.Windows.Forms.DateTimePicker();
            this.endDate = new System.Windows.Forms.DateTimePicker();
            this.toLbl = new System.Windows.Forms.Label();
            this.fromLbl = new System.Windows.Forms.Label();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.addMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.editMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.removeMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.detailsMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.nextStepMenu = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.menuStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(408, 0);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(20);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersWidth = 62;
            this.dataGridView1.RowTemplate.Height = 28;
            this.dataGridView1.Size = new System.Drawing.Size(1150, 1012);
            this.dataGridView1.TabIndex = 0;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.Left;
            this.menuStrip1.GripMargin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ordersMenu,
            this.operationsMenu,
            this.workersMenu,
            this.agentsMenu,
            this.organisationsMenu,
            this.stockMenu,
            this.accountMenu,
            this.materialListMenu,
            this.jobListMenu,
            this.referenceMenu,
            this.baseJobMenu,
            this.workLogMenu,
            this.jobDoneMenu});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(353, 1024);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // ordersMenu
            // 
            this.ordersMenu.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ordersMenu.Name = "ordersMenu";
            this.ordersMenu.Size = new System.Drawing.Size(340, 36);
            this.ordersMenu.Text = "Заказы";
            this.ordersMenu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ordersMenu.Click += new System.EventHandler(this.ordersMenu_Click);
            // 
            // operationsMenu
            // 
            this.operationsMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.profitsMenu,
            this.lossesMenu,
            this.chargesMenu,
            this.paymentsMenu});
            this.operationsMenu.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.operationsMenu.Name = "operationsMenu";
            this.operationsMenu.Size = new System.Drawing.Size(340, 36);
            this.operationsMenu.Text = "Финансовые операции";
            this.operationsMenu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // profitsMenu
            // 
            this.profitsMenu.Name = "profitsMenu";
            this.profitsMenu.Size = new System.Drawing.Size(254, 40);
            this.profitsMenu.Text = "Приходы";
            this.profitsMenu.Click += new System.EventHandler(this.profitsMenu_Click);
            // 
            // lossesMenu
            // 
            this.lossesMenu.Name = "lossesMenu";
            this.lossesMenu.Size = new System.Drawing.Size(254, 40);
            this.lossesMenu.Text = "Расходы";
            this.lossesMenu.Click += new System.EventHandler(this.lossesMenu_Click);
            // 
            // chargesMenu
            // 
            this.chargesMenu.Name = "chargesMenu";
            this.chargesMenu.Size = new System.Drawing.Size(254, 40);
            this.chargesMenu.Text = "Начисления";
            this.chargesMenu.Click += new System.EventHandler(this.chargesMenu_Click);
            // 
            // paymentsMenu
            // 
            this.paymentsMenu.Name = "paymentsMenu";
            this.paymentsMenu.Size = new System.Drawing.Size(254, 40);
            this.paymentsMenu.Text = "Выплаты";
            this.paymentsMenu.Click += new System.EventHandler(this.paymentsMenu_Click);
            // 
            // workersMenu
            // 
            this.workersMenu.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.workersMenu.Name = "workersMenu";
            this.workersMenu.Size = new System.Drawing.Size(340, 36);
            this.workersMenu.Text = "Сотрудники";
            this.workersMenu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.workersMenu.Click += new System.EventHandler(this.workersMenu_Click);
            // 
            // agentsMenu
            // 
            this.agentsMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.suppliersMenu,
            this.customersMenu,
            this.contractorMenu});
            this.agentsMenu.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.agentsMenu.Name = "agentsMenu";
            this.agentsMenu.Size = new System.Drawing.Size(340, 36);
            this.agentsMenu.Text = "Контрагенты";
            this.agentsMenu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // suppliersMenu
            // 
            this.suppliersMenu.Name = "suppliersMenu";
            this.suppliersMenu.Size = new System.Drawing.Size(257, 40);
            this.suppliersMenu.Text = "Поставщики";
            this.suppliersMenu.Click += new System.EventHandler(this.suppliersMenu_Click);
            // 
            // customersMenu
            // 
            this.customersMenu.Name = "customersMenu";
            this.customersMenu.Size = new System.Drawing.Size(257, 40);
            this.customersMenu.Text = "Заказчики";
            this.customersMenu.Click += new System.EventHandler(this.customersMenu_Click);
            // 
            // contractorMenu
            // 
            this.contractorMenu.Name = "contractorMenu";
            this.contractorMenu.Size = new System.Drawing.Size(257, 40);
            this.contractorMenu.Text = "Подрядчики";
            this.contractorMenu.Click += new System.EventHandler(this.contractorMenu_Click);
            // 
            // organisationsMenu
            // 
            this.organisationsMenu.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.organisationsMenu.Name = "organisationsMenu";
            this.organisationsMenu.Size = new System.Drawing.Size(340, 36);
            this.organisationsMenu.Text = "Организации";
            this.organisationsMenu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.organisationsMenu.Click += new System.EventHandler(this.organisationsMenu_Click);
            // 
            // stockMenu
            // 
            this.stockMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.stockMaterialMenu,
            this.stockToolMenu,
            this.stockCustomToolMenu,
            this.trimMenu});
            this.stockMenu.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.stockMenu.Name = "stockMenu";
            this.stockMenu.Size = new System.Drawing.Size(340, 36);
            this.stockMenu.Text = "Склад";
            this.stockMenu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // stockMaterialMenu
            // 
            this.stockMaterialMenu.Name = "stockMaterialMenu";
            this.stockMaterialMenu.Size = new System.Drawing.Size(481, 40);
            this.stockMaterialMenu.Text = "Склад материалов";
            this.stockMaterialMenu.Click += new System.EventHandler(this.stockMaterialMenu_Click);
            // 
            // stockToolMenu
            // 
            this.stockToolMenu.Name = "stockToolMenu";
            this.stockToolMenu.Size = new System.Drawing.Size(481, 40);
            this.stockToolMenu.Text = "Склад оборудования";
            this.stockToolMenu.Click += new System.EventHandler(this.stockToolMenu_Click);
            // 
            // stockCustomToolMenu
            // 
            this.stockCustomToolMenu.Name = "stockCustomToolMenu";
            this.stockCustomToolMenu.Size = new System.Drawing.Size(481, 40);
            this.stockCustomToolMenu.Text = "Склад давальческих материалов";
            this.stockCustomToolMenu.Click += new System.EventHandler(this.stockCustomToolMenu_Click);
            // 
            // trimMenu
            // 
            this.trimMenu.Name = "trimMenu";
            this.trimMenu.Size = new System.Drawing.Size(481, 40);
            this.trimMenu.Text = "Склад обрези";
            this.trimMenu.Click += new System.EventHandler(this.trimMenu_Click);
            // 
            // accountMenu
            // 
            this.accountMenu.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.accountMenu.Name = "accountMenu";
            this.accountMenu.Size = new System.Drawing.Size(340, 36);
            this.accountMenu.Text = "Счета";
            this.accountMenu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.accountMenu.Click += new System.EventHandler(this.accountMenu_Click);
            // 
            // materialListMenu
            // 
            this.materialListMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.materialInfoMenu,
            this.givenMaterialMenu,
            this.trimInfoMenu,
            this.toolInfoMenu});
            this.materialListMenu.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.materialListMenu.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.materialListMenu.Name = "materialListMenu";
            this.materialListMenu.Size = new System.Drawing.Size(340, 36);
            this.materialListMenu.Text = "Материалы и оборудование";
            this.materialListMenu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // materialInfoMenu
            // 
            this.materialInfoMenu.Name = "materialInfoMenu";
            this.materialInfoMenu.Size = new System.Drawing.Size(405, 40);
            this.materialInfoMenu.Text = "Материалы";
            this.materialInfoMenu.Click += new System.EventHandler(this.materialInfoMenu_Click);
            // 
            // givenMaterialMenu
            // 
            this.givenMaterialMenu.Name = "givenMaterialMenu";
            this.givenMaterialMenu.Size = new System.Drawing.Size(405, 40);
            this.givenMaterialMenu.Text = "Давальческие материалы";
            this.givenMaterialMenu.Click += new System.EventHandler(this.givenMaterialMenu_Click);
            // 
            // trimInfoMenu
            // 
            this.trimInfoMenu.Name = "trimInfoMenu";
            this.trimInfoMenu.Size = new System.Drawing.Size(405, 40);
            this.trimInfoMenu.Text = "Обрезь";
            this.trimInfoMenu.Click += new System.EventHandler(this.trimInfoMenu_Click);
            // 
            // toolInfoMenu
            // 
            this.toolInfoMenu.Name = "toolInfoMenu";
            this.toolInfoMenu.Size = new System.Drawing.Size(405, 40);
            this.toolInfoMenu.Text = "Оборудование";
            this.toolInfoMenu.Click += new System.EventHandler(this.toolInfoMenu_Click);
            // 
            // jobListMenu
            // 
            this.jobListMenu.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.jobListMenu.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.jobListMenu.Name = "jobListMenu";
            this.jobListMenu.Size = new System.Drawing.Size(340, 36);
            this.jobListMenu.Text = "Виды работ";
            this.jobListMenu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.jobListMenu.Click += new System.EventHandler(this.jobListMenu_Click);
            // 
            // referenceMenu
            // 
            this.referenceMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.departmentMenu,
            this.purposeMaterialMenu,
            this.purposeFinancialMenu});
            this.referenceMenu.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.referenceMenu.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.referenceMenu.Name = "referenceMenu";
            this.referenceMenu.Size = new System.Drawing.Size(340, 36);
            this.referenceMenu.Text = "Условные обозначения";
            this.referenceMenu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // departmentMenu
            // 
            this.departmentMenu.Name = "departmentMenu";
            this.departmentMenu.Size = new System.Drawing.Size(692, 40);
            this.departmentMenu.Text = "Подразделения";
            this.departmentMenu.Click += new System.EventHandler(this.departmentMenu_Click);
            // 
            // purposeMaterialMenu
            // 
            this.purposeMaterialMenu.Name = "purposeMaterialMenu";
            this.purposeMaterialMenu.Size = new System.Drawing.Size(692, 40);
            this.purposeMaterialMenu.Text = "Целевые назначение для операций с материалами";
            this.purposeMaterialMenu.Click += new System.EventHandler(this.purposeMaterialMenu_Click);
            // 
            // purposeFinancialMenu
            // 
            this.purposeFinancialMenu.Name = "purposeFinancialMenu";
            this.purposeFinancialMenu.Size = new System.Drawing.Size(692, 40);
            this.purposeFinancialMenu.Text = "Целевые назначения для финансовых операций";
            this.purposeFinancialMenu.Click += new System.EventHandler(this.purposeFinancialMenu_Click);
            // 
            // baseJobMenu
            // 
            this.baseJobMenu.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.baseJobMenu.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.baseJobMenu.Name = "baseJobMenu";
            this.baseJobMenu.Size = new System.Drawing.Size(340, 36);
            this.baseJobMenu.Text = "Базовые работы";
            this.baseJobMenu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.baseJobMenu.Click += new System.EventHandler(this.baseJobMenu_Click);
            // 
            // workLogMenu
            // 
            this.workLogMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.dailyReportMenu,
            this.holidayMenu,
            this.salaryChangeMenu});
            this.workLogMenu.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.workLogMenu.Name = "workLogMenu";
            this.workLogMenu.Size = new System.Drawing.Size(340, 36);
            this.workLogMenu.Text = "Учёт рабочего времени";
            this.workLogMenu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dailyReportMenu
            // 
            this.dailyReportMenu.Name = "dailyReportMenu";
            this.dailyReportMenu.Size = new System.Drawing.Size(321, 40);
            this.dailyReportMenu.Text = "Отчёты по дням";
            this.dailyReportMenu.Click += new System.EventHandler(this.dailyReportMenu_Click);
            // 
            // holidayMenu
            // 
            this.holidayMenu.Name = "holidayMenu";
            this.holidayMenu.Size = new System.Drawing.Size(321, 40);
            this.holidayMenu.Text = "Праздничные дни";
            this.holidayMenu.Click += new System.EventHandler(this.holidayMenu_Click);
            // 
            // salaryChangeMenu
            // 
            this.salaryChangeMenu.Name = "salaryChangeMenu";
            this.salaryChangeMenu.Size = new System.Drawing.Size(321, 40);
            this.salaryChangeMenu.Text = "Начисления ЗП";
            this.salaryChangeMenu.Click += new System.EventHandler(this.salaryChangeMenu_Click);
            // 
            // jobDoneMenu
            // 
            this.jobDoneMenu.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.jobDoneMenu.Name = "jobDoneMenu";
            this.jobDoneMenu.Size = new System.Drawing.Size(340, 36);
            this.jobDoneMenu.Text = "Учёт выполненных работ";
            this.jobDoneMenu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.jobDoneMenu.Click += new System.EventHandler(this.jobDoneMenu_Click);
            // 
            // startDate
            // 
            this.startDate.Location = new System.Drawing.Point(55, 601);
            this.startDate.Name = "startDate";
            this.startDate.Size = new System.Drawing.Size(222, 26);
            this.startDate.TabIndex = 6;
            // 
            // endDate
            // 
            this.endDate.Location = new System.Drawing.Point(55, 654);
            this.endDate.Name = "endDate";
            this.endDate.Size = new System.Drawing.Size(222, 26);
            this.endDate.TabIndex = 7;
            // 
            // toLbl
            // 
            this.toLbl.AutoSize = true;
            this.toLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toLbl.Location = new System.Drawing.Point(15, 658);
            this.toLbl.Name = "toLbl";
            this.toLbl.Size = new System.Drawing.Size(25, 15);
            this.toLbl.TabIndex = 20;
            this.toLbl.Text = "По";
            // 
            // fromLbl
            // 
            this.fromLbl.AutoSize = true;
            this.fromLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.fromLbl.Location = new System.Drawing.Point(15, 611);
            this.fromLbl.Name = "fromLbl";
            this.fromLbl.Size = new System.Drawing.Size(16, 15);
            this.fromLbl.TabIndex = 19;
            this.fromLbl.Text = "C";
            // 
            // menuStrip2
            // 
            this.menuStrip2.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.menuStrip2.Dock = System.Windows.Forms.DockStyle.Right;
            this.menuStrip2.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.menuStrip2.GripMargin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.menuStrip2.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addMenu,
            this.editMenu,
            this.removeMenu,
            this.detailsMenu,
            this.nextStepMenu});
            this.menuStrip2.Location = new System.Drawing.Point(1672, 0);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(226, 1024);
            this.menuStrip2.TabIndex = 26;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // addMenu
            // 
            this.addMenu.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.addMenu.Name = "addMenu";
            this.addMenu.Size = new System.Drawing.Size(213, 42);
            this.addMenu.Text = "Добавить";
            this.addMenu.Click += new System.EventHandler(this.addMenu_Click);
            // 
            // editMenu
            // 
            this.editMenu.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.editMenu.Name = "editMenu";
            this.editMenu.Size = new System.Drawing.Size(213, 42);
            this.editMenu.Text = "Редактировать";
            this.editMenu.Click += new System.EventHandler(this.editMenu_Click);
            // 
            // removeMenu
            // 
            this.removeMenu.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.removeMenu.Name = "removeMenu";
            this.removeMenu.Size = new System.Drawing.Size(213, 42);
            this.removeMenu.Text = "Удалить";
            this.removeMenu.Click += new System.EventHandler(this.removeMenu_Click);
            // 
            // detailsMenu
            // 
            this.detailsMenu.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.detailsMenu.Name = "detailsMenu";
            this.detailsMenu.Size = new System.Drawing.Size(213, 42);
            this.detailsMenu.Text = "Подробно";
            this.detailsMenu.Click += new System.EventHandler(this.detailsMenu_Click);
            // 
            // nextStepMenu
            // 
            this.nextStepMenu.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.nextStepMenu.Name = "nextStepMenu";
            this.nextStepMenu.Size = new System.Drawing.Size(213, 42);
            this.nextStepMenu.Text = "В работу";
            this.nextStepMenu.Click += new System.EventHandler(this.nextStepMenu_Click);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1898, 1024);
            this.Controls.Add(this.toLbl);
            this.Controls.Add(this.fromLbl);
            this.Controls.Add(this.endDate);
            this.Controls.Add(this.startDate);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.menuStrip2);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Учёт заказов";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainWindow_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem ordersMenu;
        private System.Windows.Forms.DateTimePicker startDate;
        private System.Windows.Forms.DateTimePicker endDate;
        private System.Windows.Forms.Label toLbl;
        private System.Windows.Forms.Label fromLbl;
        private System.Windows.Forms.ToolStripMenuItem stockMenu;
        private System.Windows.Forms.ToolStripMenuItem stockMaterialMenu;
        private System.Windows.Forms.ToolStripMenuItem stockToolMenu;
        private System.Windows.Forms.ToolStripMenuItem stockCustomToolMenu;
        private System.Windows.Forms.ToolStripMenuItem operationsMenu;
        private System.Windows.Forms.ToolStripMenuItem profitsMenu;
        private System.Windows.Forms.ToolStripMenuItem lossesMenu;
        private System.Windows.Forms.ToolStripMenuItem chargesMenu;
        private System.Windows.Forms.ToolStripMenuItem paymentsMenu;
        private System.Windows.Forms.ToolStripMenuItem workersMenu;
        private System.Windows.Forms.ToolStripMenuItem agentsMenu;
        private System.Windows.Forms.ToolStripMenuItem suppliersMenu;
        private System.Windows.Forms.ToolStripMenuItem customersMenu;
        private System.Windows.Forms.ToolStripMenuItem contractorMenu;
        private System.Windows.Forms.ToolStripMenuItem organisationsMenu;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem addMenu;
        private System.Windows.Forms.ToolStripMenuItem editMenu;
        private System.Windows.Forms.ToolStripMenuItem removeMenu;
        private System.Windows.Forms.ToolStripMenuItem detailsMenu;
        private System.Windows.Forms.ToolStripMenuItem nextStepMenu;
        private System.Windows.Forms.ToolStripMenuItem accountMenu;
        private System.Windows.Forms.ToolStripMenuItem materialListMenu;
        private System.Windows.Forms.ToolStripMenuItem jobListMenu;
        private System.Windows.Forms.ToolStripMenuItem trimMenu;
        private System.Windows.Forms.ToolStripMenuItem referenceMenu;
        private System.Windows.Forms.ToolStripMenuItem baseJobMenu;
        private System.Windows.Forms.ToolStripMenuItem workLogMenu;
        private System.Windows.Forms.ToolStripMenuItem jobDoneMenu;
        private System.Windows.Forms.ToolStripMenuItem dailyReportMenu;
        private System.Windows.Forms.ToolStripMenuItem holidayMenu;
        private System.Windows.Forms.ToolStripMenuItem salaryChangeMenu;
        private System.Windows.Forms.ToolStripMenuItem departmentMenu;
        private System.Windows.Forms.ToolStripMenuItem purposeMaterialMenu;
        private System.Windows.Forms.ToolStripMenuItem purposeFinancialMenu;
        private System.Windows.Forms.ToolStripMenuItem materialInfoMenu;
        private System.Windows.Forms.ToolStripMenuItem givenMaterialMenu;
        private System.Windows.Forms.ToolStripMenuItem trimInfoMenu;
        private System.Windows.Forms.ToolStripMenuItem toolInfoMenu;
    }
}