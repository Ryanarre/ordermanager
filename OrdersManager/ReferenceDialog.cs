﻿using System;
using System.Windows.Forms;

namespace OrdersManager
{
    public partial class ReferenceDialog : Form
    {
        public ReferenceDialog(
                ReferenceDao referenceDao
            ,   int ID
        )
        {
            InitializeComponent();

            mReferenceDao = referenceDao;
            mID = ID;

            Text = mID != 0 ?
                "Редактирование условного обозначения" :
                "Добавление условного обозначения"
            ;

            if (mID != 0)
            {
                Reference reference = referenceDao.getReferenceByID(ID);

                nameTxt.Text = reference.Name;
                commentTxt.Text = reference.Comment;
            }
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            Reference reference = new Reference();

            if (nameTxt.Text.Length == 0)
            {
                MessageBox.Show("Не все обязательные поля заполнены!");

                return;
            }

            reference.ID = mID;
            reference.Name = nameTxt.Text;
            reference.Comment = commentTxt.Text;

            if (mID != 0)
                mReferenceDao.modifyReference(reference);
            else
                mReferenceDao.addReference(reference, true);

            Close();
        }

        private void skipBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private ReferenceDao mReferenceDao;

        private int mID;
    }
}
