﻿
namespace OrdersManager
{
    partial class FlowDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.agentLbl = new System.Windows.Forms.Label();
            this.saveBtn = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.numberTxt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.purposeTxt = new System.Windows.Forms.TextBox();
            this.openDate = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.summTxt = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.skipBtn = new System.Windows.Forms.Button();
            this.agentCmb = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.currencyCmb = new System.Windows.Forms.ComboBox();
            this.commentTxt = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.guaranteeChk = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // agentLbl
            // 
            this.agentLbl.AutoSize = true;
            this.agentLbl.Location = new System.Drawing.Point(12, 15);
            this.agentLbl.Name = "agentLbl";
            this.agentLbl.Size = new System.Drawing.Size(102, 20);
            this.agentLbl.TabIndex = 1;
            this.agentLbl.Text = "Получатель";
            // 
            // saveBtn
            // 
            this.saveBtn.Location = new System.Drawing.Point(16, 423);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(197, 55);
            this.saveBtn.TabIndex = 9;
            this.saveBtn.Text = "Сохранить";
            this.saveBtn.UseVisualStyleBackColor = true;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 20);
            this.label2.TabIndex = 10;
            this.label2.Text = "Номер заказа";
            // 
            // numberTxt
            // 
            this.numberTxt.Location = new System.Drawing.Point(244, 60);
            this.numberTxt.Name = "numberTxt";
            this.numberTxt.Size = new System.Drawing.Size(222, 26);
            this.numberTxt.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 113);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(169, 20);
            this.label3.TabIndex = 12;
            this.label3.Text = "Целевое назначение";
            // 
            // purposeTxt
            // 
            this.purposeTxt.Location = new System.Drawing.Point(244, 110);
            this.purposeTxt.Name = "purposeTxt";
            this.purposeTxt.Size = new System.Drawing.Size(222, 26);
            this.purposeTxt.TabIndex = 13;
            // 
            // openDate
            // 
            this.openDate.Location = new System.Drawing.Point(244, 161);
            this.openDate.Name = "openDate";
            this.openDate.Size = new System.Drawing.Size(222, 26);
            this.openDate.TabIndex = 15;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 161);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 20);
            this.label7.TabIndex = 14;
            this.label7.Text = "Дата";
            // 
            // summTxt
            // 
            this.summTxt.Location = new System.Drawing.Point(244, 213);
            this.summTxt.Name = "summTxt";
            this.summTxt.Size = new System.Drawing.Size(222, 26);
            this.summTxt.TabIndex = 17;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 216);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(58, 20);
            this.label5.TabIndex = 16;
            this.label5.Text = "Сумма";
            // 
            // skipBtn
            // 
            this.skipBtn.Location = new System.Drawing.Point(269, 423);
            this.skipBtn.Name = "skipBtn";
            this.skipBtn.Size = new System.Drawing.Size(197, 55);
            this.skipBtn.TabIndex = 20;
            this.skipBtn.Text = "Отмена";
            this.skipBtn.UseVisualStyleBackColor = true;
            this.skipBtn.Click += new System.EventHandler(this.skipBtn_Click);
            // 
            // agentCmb
            // 
            this.agentCmb.FormattingEnabled = true;
            this.agentCmb.Location = new System.Drawing.Point(244, 15);
            this.agentCmb.Name = "agentCmb";
            this.agentCmb.Size = new System.Drawing.Size(222, 28);
            this.agentCmb.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 267);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 20);
            this.label6.TabIndex = 22;
            this.label6.Text = "Валюта";
            // 
            // currencyCmb
            // 
            this.currencyCmb.FormattingEnabled = true;
            this.currencyCmb.Items.AddRange(new object[] {
            "UAH",
            "EUR",
            "USD"});
            this.currencyCmb.Location = new System.Drawing.Point(244, 264);
            this.currencyCmb.Name = "currencyCmb";
            this.currencyCmb.Size = new System.Drawing.Size(222, 28);
            this.currencyCmb.TabIndex = 21;
            // 
            // commentTxt
            // 
            this.commentTxt.Location = new System.Drawing.Point(244, 320);
            this.commentTxt.Name = "commentTxt";
            this.commentTxt.Size = new System.Drawing.Size(222, 26);
            this.commentTxt.TabIndex = 24;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(12, 323);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(113, 20);
            this.label8.TabIndex = 23;
            this.label8.Text = "Комментарий";
            // 
            // guaranteeChk
            // 
            this.guaranteeChk.AutoSize = true;
            this.guaranteeChk.Location = new System.Drawing.Point(16, 380);
            this.guaranteeChk.Name = "guaranteeChk";
            this.guaranteeChk.Size = new System.Drawing.Size(130, 24);
            this.guaranteeChk.TabIndex = 25;
            this.guaranteeChk.Text = "По гарантии";
            this.guaranteeChk.UseVisualStyleBackColor = true;
            // 
            // FlowDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(491, 495);
            this.Controls.Add(this.guaranteeChk);
            this.Controls.Add(this.commentTxt);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.currencyCmb);
            this.Controls.Add(this.skipBtn);
            this.Controls.Add(this.summTxt);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.openDate);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.purposeTxt);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.numberTxt);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.saveBtn);
            this.Controls.Add(this.agentLbl);
            this.Controls.Add(this.agentCmb);
            this.Name = "FlowDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FlowDialog";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label agentLbl;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox numberTxt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox purposeTxt;
        private System.Windows.Forms.DateTimePicker openDate;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox summTxt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button skipBtn;
        private System.Windows.Forms.ComboBox agentCmb;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox currencyCmb;
        private System.Windows.Forms.TextBox commentTxt;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.CheckBox guaranteeChk;
    }
}