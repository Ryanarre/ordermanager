﻿
namespace OrdersManager
{
    partial class ChargeDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.contractorCmb = new System.Windows.Forms.ComboBox();
            this.skipBtn = new System.Windows.Forms.Button();
            this.saveBtn = new System.Windows.Forms.Button();
            this.nameTxt = new System.Windows.Forms.Label();
            this.workNameTxt = new System.Windows.Forms.TextBox();
            this.unitTxt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.countTxt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.summTxt = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.orderCmb = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Подрядчик";
            // 
            // contractorCmb
            // 
            this.contractorCmb.FormattingEnabled = true;
            this.contractorCmb.Location = new System.Drawing.Point(252, 16);
            this.contractorCmb.Name = "contractorCmb";
            this.contractorCmb.Size = new System.Drawing.Size(240, 28);
            this.contractorCmb.TabIndex = 1;
            // 
            // skipBtn
            // 
            this.skipBtn.Location = new System.Drawing.Point(542, 107);
            this.skipBtn.Name = "skipBtn";
            this.skipBtn.Size = new System.Drawing.Size(197, 55);
            this.skipBtn.TabIndex = 33;
            this.skipBtn.Text = "Отмена";
            this.skipBtn.UseVisualStyleBackColor = true;
            this.skipBtn.Click += new System.EventHandler(this.skipBtn_Click);
            // 
            // saveBtn
            // 
            this.saveBtn.Location = new System.Drawing.Point(542, 16);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(197, 55);
            this.saveBtn.TabIndex = 32;
            this.saveBtn.Text = "Сохранить";
            this.saveBtn.UseVisualStyleBackColor = true;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // nameTxt
            // 
            this.nameTxt.AutoSize = true;
            this.nameTxt.Location = new System.Drawing.Point(22, 75);
            this.nameTxt.Name = "nameTxt";
            this.nameTxt.Size = new System.Drawing.Size(182, 20);
            this.nameTxt.TabIndex = 34;
            this.nameTxt.Text = "Наименование работы";
            // 
            // workNameTxt
            // 
            this.workNameTxt.Location = new System.Drawing.Point(252, 72);
            this.workNameTxt.Name = "workNameTxt";
            this.workNameTxt.Size = new System.Drawing.Size(240, 26);
            this.workNameTxt.TabIndex = 35;
            // 
            // unitTxt
            // 
            this.unitTxt.Location = new System.Drawing.Point(252, 124);
            this.unitTxt.Name = "unitTxt";
            this.unitTxt.Size = new System.Drawing.Size(240, 26);
            this.unitTxt.TabIndex = 37;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 130);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(162, 20);
            this.label2.TabIndex = 36;
            this.label2.Text = "Единица измерения";
            // 
            // countTxt
            // 
            this.countTxt.Location = new System.Drawing.Point(252, 186);
            this.countTxt.Name = "countTxt";
            this.countTxt.Size = new System.Drawing.Size(240, 26);
            this.countTxt.TabIndex = 39;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 186);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 20);
            this.label3.TabIndex = 38;
            this.label3.Text = "Количество";
            // 
            // summTxt
            // 
            this.summTxt.Location = new System.Drawing.Point(252, 244);
            this.summTxt.Name = "summTxt";
            this.summTxt.Size = new System.Drawing.Size(240, 26);
            this.summTxt.TabIndex = 41;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 244);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 20);
            this.label4.TabIndex = 40;
            this.label4.Text = "Сумма, грн";
            // 
            // orderCmb
            // 
            this.orderCmb.FormattingEnabled = true;
            this.orderCmb.Location = new System.Drawing.Point(252, 291);
            this.orderCmb.Name = "orderCmb";
            this.orderCmb.Size = new System.Drawing.Size(240, 28);
            this.orderCmb.TabIndex = 43;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(22, 294);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(114, 20);
            this.label5.TabIndex = 42;
            this.label5.Text = "Номер заказа";
            // 
            // ChargeDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 342);
            this.Controls.Add(this.orderCmb);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.summTxt);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.countTxt);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.unitTxt);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.workNameTxt);
            this.Controls.Add(this.nameTxt);
            this.Controls.Add(this.skipBtn);
            this.Controls.Add(this.saveBtn);
            this.Controls.Add(this.contractorCmb);
            this.Controls.Add(this.label1);
            this.Name = "ChargeDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Начислить оплату за выполненную работу";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox contractorCmb;
        private System.Windows.Forms.Button skipBtn;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.Label nameTxt;
        private System.Windows.Forms.TextBox workNameTxt;
        private System.Windows.Forms.TextBox unitTxt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox countTxt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox summTxt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox orderCmb;
        private System.Windows.Forms.Label label5;
    }
}