﻿using System;
using System.Windows.Forms;

namespace OrdersManager
{
    public partial class SalaryAddDialog : Form
    {
        public SalaryAddDialog(
              SalaryDialog parent
            , DatabaseManager dbManager
            , int workerId
        )
        {
            InitializeComponent();

            mParent = parent;
            mDbManager = dbManager;
            mWorkerId = workerId;
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            if (salaryTxt.Text.Length == 0 || titleTxt.Text.Length == 0)
            {
                MessageBox.Show("Не все обязательные поля заполнены!");

                return;
            }

            Salary salary = new Salary();

            if (!double.TryParse(salaryTxt.Text, out salary.Amount))
            {
                MessageBox.Show(
                    "Невозможно добавить данные: некорректный формат числовых значений"
                  , "Ошибка"
                  , MessageBoxButtons.OK
                  , MessageBoxIcon.Error
                );
                return;
            }

            salary.Date = DateTime.Now;
            salary.Title = titleTxt.Text;
            salary.WorkerID = mWorkerId;

            mDbManager.getSalaryDao().addSalary(salary, true);

            Worker worker = mDbManager.getWorkerDao().getWorkerByID(mWorkerId);
            worker.Salary = salary.Amount;
            worker.Title = salary.Title;
            mDbManager.getWorkerDao().modifyWorker(worker);

            mParent.reset();

            Close();
        }

        private void skipBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private SalaryDialog mParent;

        private DatabaseManager mDbManager;

        private int mWorkerId;
    }
}
