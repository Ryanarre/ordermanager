﻿
namespace OrdersManager
{
    partial class AccountAddDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.skipBtn = new System.Windows.Forms.Button();
            this.saveBtn = new System.Windows.Forms.Button();
            this.nameTxt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.currencyCmb = new System.Windows.Forms.ComboBox();
            this.openDate = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.numberTxt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.bankTxt = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.orgCmb = new System.Windows.Forms.ComboBox();
            this.commentTxt = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // skipBtn
            // 
            this.skipBtn.Location = new System.Drawing.Point(274, 346);
            this.skipBtn.Name = "skipBtn";
            this.skipBtn.Size = new System.Drawing.Size(197, 55);
            this.skipBtn.TabIndex = 33;
            this.skipBtn.Text = "Отмена";
            this.skipBtn.UseVisualStyleBackColor = true;
            this.skipBtn.Click += new System.EventHandler(this.skipBtn_Click);
            // 
            // saveBtn
            // 
            this.saveBtn.Location = new System.Drawing.Point(25, 346);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(197, 55);
            this.saveBtn.TabIndex = 32;
            this.saveBtn.Text = "Сохранить";
            this.saveBtn.UseVisualStyleBackColor = true;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // nameTxt
            // 
            this.nameTxt.Location = new System.Drawing.Point(249, 59);
            this.nameTxt.Name = "nameTxt";
            this.nameTxt.Size = new System.Drawing.Size(222, 26);
            this.nameTxt.TabIndex = 31;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(170, 20);
            this.label1.TabIndex = 30;
            this.label1.Text = "Наименование счёта";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 194);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 20);
            this.label2.TabIndex = 34;
            this.label2.Text = "Валюта";
            // 
            // currencyCmb
            // 
            this.currencyCmb.FormattingEnabled = true;
            this.currencyCmb.Items.AddRange(new object[] {
            "UAH",
            "USD",
            "EUR"});
            this.currencyCmb.Location = new System.Drawing.Point(249, 191);
            this.currencyCmb.Name = "currencyCmb";
            this.currencyCmb.Size = new System.Drawing.Size(222, 28);
            this.currencyCmb.TabIndex = 35;
            // 
            // openDate
            // 
            this.openDate.Location = new System.Drawing.Point(249, 238);
            this.openDate.Name = "openDate";
            this.openDate.Size = new System.Drawing.Size(222, 26);
            this.openDate.TabIndex = 38;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(21, 238);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(125, 20);
            this.label7.TabIndex = 36;
            this.label7.Text = "Дата открытия";
            // 
            // numberTxt
            // 
            this.numberTxt.Location = new System.Drawing.Point(249, 18);
            this.numberTxt.Name = "numberTxt";
            this.numberTxt.Size = new System.Drawing.Size(222, 26);
            this.numberTxt.TabIndex = 40;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 20);
            this.label3.TabIndex = 39;
            this.label3.Text = "Номер р/с";
            // 
            // bankTxt
            // 
            this.bankTxt.Location = new System.Drawing.Point(249, 100);
            this.bankTxt.Name = "bankTxt";
            this.bankTxt.Size = new System.Drawing.Size(222, 26);
            this.bankTxt.TabIndex = 42;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 103);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(170, 20);
            this.label4.TabIndex = 41;
            this.label4.Text = "Наименование банка";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(21, 146);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(222, 20);
            this.label5.TabIndex = 43;
            this.label5.Text = "Наименование организации";
            // 
            // orgCmb
            // 
            this.orgCmb.FormattingEnabled = true;
            this.orgCmb.Items.AddRange(new object[] {
            "UAH",
            "USD",
            "EUR"});
            this.orgCmb.Location = new System.Drawing.Point(249, 143);
            this.orgCmb.Name = "orgCmb";
            this.orgCmb.Size = new System.Drawing.Size(222, 28);
            this.orgCmb.TabIndex = 44;
            // 
            // commentTxt
            // 
            this.commentTxt.Location = new System.Drawing.Point(249, 285);
            this.commentTxt.Name = "commentTxt";
            this.commentTxt.Size = new System.Drawing.Size(222, 26);
            this.commentTxt.TabIndex = 46;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(21, 285);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(104, 20);
            this.label6.TabIndex = 45;
            this.label6.Text = "Примечание";
            // 
            // AccountAddDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(507, 434);
            this.Controls.Add(this.commentTxt);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.orgCmb);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.bankTxt);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.numberTxt);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.openDate);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.currencyCmb);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.skipBtn);
            this.Controls.Add(this.saveBtn);
            this.Controls.Add(this.nameTxt);
            this.Controls.Add(this.label1);
            this.Name = "AccountAddDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AccountAddDialog";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button skipBtn;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.TextBox nameTxt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox currencyCmb;
        private System.Windows.Forms.DateTimePicker openDate;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox numberTxt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox bankTxt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox orgCmb;
        private System.Windows.Forms.TextBox commentTxt;
        private System.Windows.Forms.Label label6;
    }
}