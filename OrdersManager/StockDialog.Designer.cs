﻿
namespace OrdersManager
{
    partial class StockDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.skipBtn = new System.Windows.Forms.Button();
            this.saveBtn = new System.Windows.Forms.Button();
            this.unitTxt = new System.Windows.Forms.TextBox();
            this.unitLbl = new System.Windows.Forms.Label();
            this.pricePerUniteTxt = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.countTxt = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.stockCmb = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(122, 20);
            this.label1.TabIndex = 24;
            this.label1.Text = "Наименование";
            // 
            // skipBtn
            // 
            this.skipBtn.Location = new System.Drawing.Point(526, 112);
            this.skipBtn.Name = "skipBtn";
            this.skipBtn.Size = new System.Drawing.Size(196, 55);
            this.skipBtn.TabIndex = 23;
            this.skipBtn.Text = "Отмена";
            this.skipBtn.UseVisualStyleBackColor = true;
            this.skipBtn.Click += new System.EventHandler(this.skipBtn_Click);
            // 
            // saveBtn
            // 
            this.saveBtn.Location = new System.Drawing.Point(526, 12);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(196, 55);
            this.saveBtn.TabIndex = 22;
            this.saveBtn.Text = "Сохранить";
            this.saveBtn.UseVisualStyleBackColor = true;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // unitTxt
            // 
            this.unitTxt.Location = new System.Drawing.Point(215, 178);
            this.unitTxt.Name = "unitTxt";
            this.unitTxt.Size = new System.Drawing.Size(222, 26);
            this.unitTxt.TabIndex = 27;
            // 
            // unitLbl
            // 
            this.unitLbl.AutoSize = true;
            this.unitLbl.Location = new System.Drawing.Point(18, 178);
            this.unitLbl.Name = "unitLbl";
            this.unitLbl.Size = new System.Drawing.Size(162, 20);
            this.unitLbl.TabIndex = 26;
            this.unitLbl.Text = "Единица измерения";
            // 
            // pricePerUniteTxt
            // 
            this.pricePerUniteTxt.Location = new System.Drawing.Point(215, 68);
            this.pricePerUniteTxt.Name = "pricePerUniteTxt";
            this.pricePerUniteTxt.Size = new System.Drawing.Size(222, 26);
            this.pricePerUniteTxt.TabIndex = 29;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 71);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(181, 20);
            this.label3.TabIndex = 28;
            this.label3.Text = "Стоимость за единицу";
            // 
            // countTxt
            // 
            this.countTxt.Location = new System.Drawing.Point(215, 123);
            this.countTxt.Name = "countTxt";
            this.countTxt.Size = new System.Drawing.Size(222, 26);
            this.countTxt.TabIndex = 31;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(14, 126);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 20);
            this.label4.TabIndex = 30;
            this.label4.Text = "Количество";
            // 
            // stockCmb
            // 
            this.stockCmb.FormattingEnabled = true;
            this.stockCmb.Location = new System.Drawing.Point(215, 15);
            this.stockCmb.Name = "stockCmb";
            this.stockCmb.Size = new System.Drawing.Size(222, 28);
            this.stockCmb.TabIndex = 32;
            this.stockCmb.SelectedIndexChanged += new System.EventHandler(this.stockCmb_SelectedIndexChanged);
            // 
            // StockDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 227);
            this.Controls.Add(this.stockCmb);
            this.Controls.Add(this.countTxt);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.pricePerUniteTxt);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.unitTxt);
            this.Controls.Add(this.unitLbl);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.skipBtn);
            this.Controls.Add(this.saveBtn);
            this.Name = "StockDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Приход материала";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button skipBtn;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.TextBox unitTxt;
        private System.Windows.Forms.Label unitLbl;
        private System.Windows.Forms.TextBox pricePerUniteTxt;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox countTxt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox stockCmb;
    }
}