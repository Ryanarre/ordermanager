﻿using System;
using System.Windows.Forms;

namespace OrdersManager
{
    public partial class LoginWindow : Form
    {
        public LoginWindow()
        {
            InitializeComponent();
        }

        ~LoginWindow()
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (loginTxt.Text.Length == 0 || passTxt.Text.Length == 0)
            {
                MessageBox.Show("Введите логин и пароль!");
                return;
            }

            if (loginTxt.Text == "Секретарь" && passTxt.Text == "test0")
                moveToMain(0);
            else if (loginTxt.Text == "Бухгалтер" && passTxt.Text == "test1")
                moveToMain(1);
            else if (loginTxt.Text == "Конструктор" && passTxt.Text == "test2")
                moveToMain(2);
            else if (loginTxt.Text == "Директор" && passTxt.Text == "test3")
                moveToMain(3);
            else
                MessageBox.Show("Некорректный логин или пароль!");
        }

        private void moveToMain(int role)
        {
            MainWindow mainWindow = new MainWindow(this, role);

            Hide();

            mainWindow.Show();
        }
    }
}
