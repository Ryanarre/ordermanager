﻿namespace OrdersManager
{
    public class Stock
    {
        public string Name;
        public string Unit;

        public double PricePerUnite;

        public int CountBegin;
        public int CountEnd;

        public int ID;
    }
}
