﻿namespace OrdersManager
{
    public class WorkLog
    {
        public int WorkerID;
        public int WorkDayID;

        public int Logged;
        public int Missed;
        public int PaidSick;
        public int UnpaidSick;
        public int PaidLeave;
        public int UnpaidLeave;
    }
}
