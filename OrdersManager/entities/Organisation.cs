﻿namespace OrdersManager
{
    public class Organisation
    {
        public string Name;
        public string Type;
        public string Comment;

        public System.DateTime StartDate;
        public System.DateTime EndDate;

        public int ID;

        public bool Closed;
    }
}
