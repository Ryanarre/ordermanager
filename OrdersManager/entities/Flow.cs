﻿namespace OrdersManager
{
    public struct Flow
    {
        public string Number;
        public string Purpose;
        public string Comment;
        public string Agent;
        public string Currency;
        public string Unit;

        public System.DateTime Date;

        public double Summ;
        public double PricePerUnit;

        public int ID;
        public int Count;

        public bool IsGuarantee;
        public bool WithAgent;
    }
}
