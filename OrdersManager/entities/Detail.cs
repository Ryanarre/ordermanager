﻿namespace OrdersManager
{ 
    public class Detail
    {
        public string Name;
        public string Unit;
        public string Comment;
        public string OrderName;

        public double PricePerUnit;

        public int ID;

        public DetailType Type;
    };
}
