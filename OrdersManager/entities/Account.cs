﻿namespace OrdersManager
{
    public class Account
    {
        public string Number;
        public string Name;
        public string Currency;
        public string Bank;
        public string Comment;
        public string Organisation;

        public System.DateTime StartDate;
        public System.DateTime EndDate;

        public int ID;

        public bool Closed;
    }
}
