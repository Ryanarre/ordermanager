﻿namespace OrdersManager
{
    public class StockInfo
    {
        public string Title;
        public string Name;
        public string Unit;
        public string Comment;

        public int ID;
    }
}
