﻿namespace OrdersManager
{
    public class StockHistory
    {
        public string OrderName;
        public string Comment;

        public System.DateTime Date;

        public int Count;

        public int StockID;

        public bool IsGuarantee;

        /*
         * true = Increment
         * false = Decrement
         */
        public bool Side;

        public StockType Type;
    }
}
