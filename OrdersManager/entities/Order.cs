﻿namespace OrdersManager
{
    public struct Order
    {
        public string Number;
        public string Name;
        public string Type;

        public System.DateTime StartDate;
        public System.DateTime EndDate;

        public double DebtClients;
        public double DebtWorkers;

        public double Summ;

        public int ID;

        public OrderStatus Status;
    }
}
