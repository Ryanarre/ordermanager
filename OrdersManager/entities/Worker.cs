﻿namespace OrdersManager
{
    public class Worker
    {
        public string Surname;
        public string Name;
        public string Patronymic;
        public string Phone;
        public string Email;
        public string Title;
        public string Passport;
        public string Comment;

        public System.DateTime BirthDate;
        public System.DateTime HireDate;
        public System.DateTime FireDate;

        public double Salary;

        public int ID;

        public bool Status = true;
    }
}
