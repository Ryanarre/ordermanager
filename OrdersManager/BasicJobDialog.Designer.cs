﻿
namespace OrdersManager
{
    partial class BasicJobDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.skipBtn = new System.Windows.Forms.Button();
            this.saveBtn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.unitCmb = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.nameCmb = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.priceTxt = new System.Windows.Forms.TextBox();
            this.commentTxt = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // skipBtn
            // 
            this.skipBtn.Location = new System.Drawing.Point(334, 216);
            this.skipBtn.Name = "skipBtn";
            this.skipBtn.Size = new System.Drawing.Size(197, 55);
            this.skipBtn.TabIndex = 37;
            this.skipBtn.Text = "Отмена";
            this.skipBtn.UseVisualStyleBackColor = true;
            this.skipBtn.Click += new System.EventHandler(this.skipBtn_Click);
            // 
            // saveBtn
            // 
            this.saveBtn.Location = new System.Drawing.Point(12, 216);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(197, 55);
            this.saveBtn.TabIndex = 36;
            this.saveBtn.Text = "Сохранить";
            this.saveBtn.UseVisualStyleBackColor = true;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(261, 20);
            this.label1.TabIndex = 38;
            this.label1.Text = "Количественная характеристика";
            // 
            // unitCmb
            // 
            this.unitCmb.FormattingEnabled = true;
            this.unitCmb.Items.AddRange(new object[] {
            "Площадь фасадного остекления",
            "Площадь оконно-дверных работ в фасадном остеклении",
            "Площадь оконно-дверных систем"});
            this.unitCmb.Location = new System.Drawing.Point(296, 13);
            this.unitCmb.Name = "unitCmb";
            this.unitCmb.Size = new System.Drawing.Size(235, 28);
            this.unitCmb.TabIndex = 39;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(182, 20);
            this.label2.TabIndex = 40;
            this.label2.Text = "Наименование работы";
            // 
            // nameCmb
            // 
            this.nameCmb.FormattingEnabled = true;
            this.nameCmb.Items.AddRange(new object[] {
            "Площадь фасадного остекления",
            "Площадь оконно-дверных работ в фасадном остеклении",
            "Площадь оконно-дверных систем"});
            this.nameCmb.Location = new System.Drawing.Point(296, 59);
            this.nameCmb.Name = "nameCmb";
            this.nameCmb.Size = new System.Drawing.Size(235, 28);
            this.nameCmb.TabIndex = 41;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(210, 20);
            this.label3.TabIndex = 42;
            this.label3.Text = "Объём работы на единицу";
            // 
            // priceTxt
            // 
            this.priceTxt.Location = new System.Drawing.Point(296, 112);
            this.priceTxt.Name = "priceTxt";
            this.priceTxt.Size = new System.Drawing.Size(235, 26);
            this.priceTxt.TabIndex = 43;
            // 
            // commentTxt
            // 
            this.commentTxt.Location = new System.Drawing.Point(296, 160);
            this.commentTxt.Name = "commentTxt";
            this.commentTxt.Size = new System.Drawing.Size(235, 26);
            this.commentTxt.TabIndex = 45;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 163);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 20);
            this.label4.TabIndex = 44;
            this.label4.Text = "Примечание";
            // 
            // BasicJobDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(554, 290);
            this.Controls.Add(this.commentTxt);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.priceTxt);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.nameCmb);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.unitCmb);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.skipBtn);
            this.Controls.Add(this.saveBtn);
            this.Name = "BasicJobDialog";
            this.Text = "BasicJobDialog";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button skipBtn;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox unitCmb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox nameCmb;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox priceTxt;
        private System.Windows.Forms.TextBox commentTxt;
        private System.Windows.Forms.Label label4;
    }
}