﻿namespace OrdersManager
{
    /*
    * 0 = Заказы
    * 1 = Склад материалов
    * 2 = Склад оборудование
    * 3 = Склад давальческого оборудования
    * 4 = Склад обрези
    * 5 = Приходы
    * 6 = Расходы
    * 7 = Начисления
    * 8 = Выплаты
    * 9 = Сотрудники
    * 10 = Поставщики
    * 11 = Подрядчики
    * 12 = Организации
    * 13 = Счета
    * 14 = Виды работ
    * 15 = Базовые работы
    * 16 = Материалы
    * 17 = Оборудование
    * 18 = Давальческое оборудование
    * 19 = Обрезь
    * 20 = Подразделения
    * 21 = Целевые для материалов
    * 22 = Целевые для финансовых операций
    * 23 = Учёт рабочего времени
    * 24 = Праздничные дни
    * 25 = Начисления ЗП
    * 26 = Учёт выполненных работ
    */
    public enum MenuElem
    {
        Orders, 
        StockMaterial,
        StockTool,
        StockCustomTool,
        StockTrim,
        Profits,
        Losses, 
        Charges,
        Payments,
        Workers,
        Supplier,
        Customers,
        Contractors,
        Organisations, 
        Accounts,
        JobType,
        BasicJob,
        Material,
        Tool,
        CustomTool,
        Trim,
        Department,
        PurposeMaterial,
        PurposeFinancial,
        WorkLog,
        Holidays,
        SalaryCharge,
        JobDone
    }
}
