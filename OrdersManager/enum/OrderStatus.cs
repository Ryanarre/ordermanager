﻿namespace OrdersManager
{
    /*
     * 0 - КП
     * 1 - Открыт
     * 2 - Закрыт
     */
    public enum OrderStatus
    {
        CommercialOffer = 0,
        Open,
        Close
    }
}
