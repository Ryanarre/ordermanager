﻿namespace OrdersManager
{
    public enum DetailType
    {
        PlanJob = 0,
        PlanMaterial,
        PlanLoss,
        FactJob,
        FactMaterial,
        FactLoss,
        GuaranteeJob,
        GuaranteeMaterial,
        GuaranteeLoss
    }
}
