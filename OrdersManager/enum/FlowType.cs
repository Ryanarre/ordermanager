﻿namespace OrdersManager
{
    /*
    * 0 = Приход
    * 1 = Расход
    * 2 = Начисление ЗП
    * 3 = Начисление оплаты за работу
    * 4 = Выплаты
    */
    public enum FlowType : int
    {
        Profit,
        Loss,
        Charge,
        Payment
    }
}
