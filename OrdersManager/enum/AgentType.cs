﻿namespace OrdersManager
{
    /*
     * 0 = Поставщик
     * 1 = Заказчик
     * 2 = Подрядчик
     */
    public enum AgentType
    {
        Supplier = 0,
        Customer,
        Contractor
    };
}
