﻿namespace OrdersManager
{
    public enum StockType
    {
        Material = 0,
        Tool,
        CustomMaterial,
        Trim
    }
}
