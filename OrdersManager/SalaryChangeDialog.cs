﻿using System;
using System.Windows.Forms;

namespace OrdersManager
{
    public partial class SalaryChangeDialog : Form
    {
        public SalaryChangeDialog(
                DatabaseManager dbManager
            ,   int ID
        )
        {
            InitializeComponent();

            mDbManager = dbManager;

            mID = ID;

            for (int i = 0; i < dbManager.getWorkerDao().size(); ++i)
                workerCmb.Items.Add(
                    dbManager.getWorkerDao().getWorker(i).Surname + " " +
                    dbManager.getWorkerDao().getWorker(i).Name + " " +
                    dbManager.getWorkerDao().getWorker(i).Patronymic
                );

            if (mID != 0)
            {
                Flow flow = mDbManager.getChargeDao().getFlowByID(mID);

                workerCmb.Text = flow.Agent;
                commentTxt.Text = flow.Comment;
            }
        }

        private void skipBtn_Click(object sender, EventArgs e)
        { 
            Close();
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            Flow flow = new Flow();

            flow.Agent = workerCmb.Text;
            flow.WithAgent = false;
            flow.Date = DateTime.Now;
            flow.Comment = commentTxt.Text;

            double bonus = 0;
            double vacation = 0;
            if (
                !double.TryParse(summBonusTxt.Text, out bonus) ||
                !double.TryParse(summVacTxt.Text, out vacation)
            )
            {
                MessageBox.Show(
                    "Невозможно добавить начисление: некорректный формат сумм"
                  , "Ошибка"
                  , MessageBoxButtons.OK
                  , MessageBoxIcon.Error
                );
                return;
            }

            for (int i = 0; i < mDbManager.getWorkerDao().size(); ++i)
                if (mDbManager.getWorkerDao().getWorker(i).Name == flow.Agent)
                    flow.Summ = mDbManager.getWorkerDao().getWorker(i).Salary + bonus + vacation;

            if (mID != 0)
                mDbManager.getChargeDao().modifyFlow(flow);
            else
                mDbManager.getChargeDao().addFlow(flow, true);

            Close();
        }

        private DatabaseManager mDbManager;

        private int mID;
    }
}
