﻿
namespace OrdersManager
{
    partial class OrderDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.numberLbl = new System.Windows.Forms.Label();
            this.numberTxt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.saveBtn = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.dateLbl = new System.Windows.Forms.Label();
            this.summTxt = new System.Windows.Forms.TextBox();
            this.openDate = new System.Windows.Forms.DateTimePicker();
            this.skipBtn = new System.Windows.Forms.Button();
            this.typeCmb = new System.Windows.Forms.ComboBox();
            this.nameCmb = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // numberLbl
            // 
            this.numberLbl.AutoSize = true;
            this.numberLbl.Location = new System.Drawing.Point(15, 15);
            this.numberLbl.Name = "numberLbl";
            this.numberLbl.Size = new System.Drawing.Size(59, 20);
            this.numberLbl.TabIndex = 0;
            this.numberLbl.Text = "Номер";
            // 
            // numberTxt
            // 
            this.numberTxt.Location = new System.Drawing.Point(151, 12);
            this.numberTxt.Name = "numberTxt";
            this.numberTxt.Size = new System.Drawing.Size(222, 26);
            this.numberTxt.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Заказчик";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(14, 103);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "Отдел";
            // 
            // saveBtn
            // 
            this.saveBtn.Location = new System.Drawing.Point(18, 162);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(196, 55);
            this.saveBtn.TabIndex = 8;
            this.saveBtn.Text = "Сохранить";
            this.saveBtn.UseVisualStyleBackColor = true;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(399, 57);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(167, 20);
            this.label5.TabIndex = 11;
            this.label5.Text = "Сумма договора, грн";
            // 
            // dateLbl
            // 
            this.dateLbl.AutoSize = true;
            this.dateLbl.Location = new System.Drawing.Point(399, 15);
            this.dateLbl.Name = "dateLbl";
            this.dateLbl.Size = new System.Drawing.Size(125, 20);
            this.dateLbl.TabIndex = 9;
            this.dateLbl.Text = "Дата открытия";
            // 
            // summTxt
            // 
            this.summTxt.Location = new System.Drawing.Point(605, 51);
            this.summTxt.Name = "summTxt";
            this.summTxt.Size = new System.Drawing.Size(222, 26);
            this.summTxt.TabIndex = 12;
            // 
            // openDate
            // 
            this.openDate.Location = new System.Drawing.Point(605, 12);
            this.openDate.Name = "openDate";
            this.openDate.Size = new System.Drawing.Size(222, 26);
            this.openDate.TabIndex = 13;
            // 
            // skipBtn
            // 
            this.skipBtn.Location = new System.Drawing.Point(245, 162);
            this.skipBtn.Name = "skipBtn";
            this.skipBtn.Size = new System.Drawing.Size(196, 55);
            this.skipBtn.TabIndex = 15;
            this.skipBtn.Text = "Отмена";
            this.skipBtn.UseVisualStyleBackColor = true;
            this.skipBtn.Click += new System.EventHandler(this.skipBtn_Click);
            // 
            // typeCmb
            // 
            this.typeCmb.FormattingEnabled = true;
            this.typeCmb.Items.AddRange(new object[] {
            "ПВХ",
            "Аллюминий"});
            this.typeCmb.Location = new System.Drawing.Point(151, 95);
            this.typeCmb.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.typeCmb.Name = "typeCmb";
            this.typeCmb.Size = new System.Drawing.Size(222, 28);
            this.typeCmb.TabIndex = 17;
            // 
            // nameCmb
            // 
            this.nameCmb.FormattingEnabled = true;
            this.nameCmb.Location = new System.Drawing.Point(151, 51);
            this.nameCmb.Name = "nameCmb";
            this.nameCmb.Size = new System.Drawing.Size(222, 28);
            this.nameCmb.TabIndex = 18;
            // 
            // OrderDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(839, 235);
            this.Controls.Add(this.nameCmb);
            this.Controls.Add(this.typeCmb);
            this.Controls.Add(this.skipBtn);
            this.Controls.Add(this.openDate);
            this.Controls.Add(this.summTxt);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.dateLbl);
            this.Controls.Add(this.saveBtn);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.numberTxt);
            this.Controls.Add(this.numberLbl);
            this.Name = "OrderDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Редактировать заказ";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label numberLbl;
        private System.Windows.Forms.TextBox numberTxt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label dateLbl;
        private System.Windows.Forms.TextBox summTxt;
        private System.Windows.Forms.DateTimePicker openDate;
        private System.Windows.Forms.Button skipBtn;
        private System.Windows.Forms.ComboBox typeCmb;
        private System.Windows.Forms.ComboBox nameCmb;
    }
}