﻿using System;
using System.Windows.Forms;

namespace OrdersManager
{
    public partial class DetailAddDialog : Form
    {
        public DetailAddDialog(
              DetailDao detailDao
            , DetailType detailType
            , string name
            , int number
        )
        {
            InitializeComponent();

            mDetailDao = detailDao;
            mOrderName = name;
            mDetailType = detailType;
            mID = number;

            Text = mID != 0 ?
                "Редактирование " :
                "Добавление "
            ;
            Text += getEntityName(detailType);

            if (mID != 0)
            {
                Detail detail = mDetailDao.getDetailByID(number);

                nameTxt.Text = detail.Name;
                unitTxt.Text = detail.Unit;
                priceTxt.Text = detail.PricePerUnit.ToString();
                commentTxt.Text = detail.Comment;
            }
        }

        private string getEntityName(DetailType detailType)
        {
            switch (detailType)
            {
                case DetailType.PlanJob:
                    return " плановой работы";
                case DetailType.PlanMaterial:
                    return " планового материала";
                case DetailType.PlanLoss:
                default:
                    return " планового расхода";
            }
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            Detail detail = new Detail();

            detail.ID = mID;
            detail.Name = nameTxt.Text;
            detail.Unit = unitTxt.Text;
            detail.PricePerUnit = double.Parse(priceTxt.Text);
            detail.Comment = commentTxt.Text;
            detail.Type = mDetailType;
            detail.OrderName = mOrderName;

            if (mID != 0)
                mDetailDao.modifyDetail(detail);
            else
                mDetailDao.addDetail(detail, true);

            Close();
        }

        private void skipBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private DetailDao mDetailDao;

        private DetailType mDetailType;

        private string mOrderName;

        private int mID;
    }
}
