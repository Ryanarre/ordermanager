﻿
namespace OrdersManager
{
    partial class SalaryChangeDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.workerCmb = new System.Windows.Forms.ComboBox();
            this.monthCmb = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.skipBtn = new System.Windows.Forms.Button();
            this.saveBtn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.countTxt = new System.Windows.Forms.TextBox();
            this.summVacTxt = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.summBonusTxt = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.commentTxt = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Сотрудник";
            // 
            // workerCmb
            // 
            this.workerCmb.FormattingEnabled = true;
            this.workerCmb.Location = new System.Drawing.Point(183, 26);
            this.workerCmb.Name = "workerCmb";
            this.workerCmb.Size = new System.Drawing.Size(248, 28);
            this.workerCmb.TabIndex = 1;
            // 
            // monthCmb
            // 
            this.monthCmb.FormattingEnabled = true;
            this.monthCmb.Items.AddRange(new object[] {
            "Январь",
            "Февраль",
            "Март",
            "Апрель",
            "Май",
            "Июнь",
            "Июль",
            "Август",
            "Сентябрь",
            "Октябрь",
            "Ноябрь",
            "Декабрь"});
            this.monthCmb.Location = new System.Drawing.Point(183, 77);
            this.monthCmb.Name = "monthCmb";
            this.monthCmb.Size = new System.Drawing.Size(248, 28);
            this.monthCmb.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Месяц";
            // 
            // skipBtn
            // 
            this.skipBtn.Location = new System.Drawing.Point(482, 122);
            this.skipBtn.Name = "skipBtn";
            this.skipBtn.Size = new System.Drawing.Size(197, 55);
            this.skipBtn.TabIndex = 31;
            this.skipBtn.Text = "Отмена";
            this.skipBtn.UseVisualStyleBackColor = true;
            this.skipBtn.Click += new System.EventHandler(this.skipBtn_Click);
            // 
            // saveBtn
            // 
            this.saveBtn.Location = new System.Drawing.Point(482, 26);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(197, 55);
            this.saveBtn.TabIndex = 30;
            this.saveBtn.Text = "Сохранить";
            this.saveBtn.UseVisualStyleBackColor = true;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 139);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(142, 20);
            this.label3.TabIndex = 32;
            this.label3.Text = "Количество дней";
            // 
            // countTxt
            // 
            this.countTxt.Location = new System.Drawing.Point(183, 136);
            this.countTxt.Name = "countTxt";
            this.countTxt.Size = new System.Drawing.Size(248, 26);
            this.countTxt.TabIndex = 33;
            // 
            // summVacTxt
            // 
            this.summVacTxt.Location = new System.Drawing.Point(183, 194);
            this.summVacTxt.Name = "summVacTxt";
            this.summVacTxt.Size = new System.Drawing.Size(248, 26);
            this.summVacTxt.TabIndex = 35;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 197);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(139, 20);
            this.label4.TabIndex = 34;
            this.label4.Text = "Сумма отпускных";
            // 
            // summBonusTxt
            // 
            this.summBonusTxt.Location = new System.Drawing.Point(183, 259);
            this.summBonusTxt.Name = "summBonusTxt";
            this.summBonusTxt.Size = new System.Drawing.Size(248, 26);
            this.summBonusTxt.TabIndex = 37;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(22, 262);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(131, 20);
            this.label5.TabIndex = 36;
            this.label5.Text = "Сумма бонусных";
            // 
            // commentTxt
            // 
            this.commentTxt.Location = new System.Drawing.Point(183, 318);
            this.commentTxt.Name = "commentTxt";
            this.commentTxt.Size = new System.Drawing.Size(248, 26);
            this.commentTxt.TabIndex = 39;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(22, 321);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(113, 20);
            this.label6.TabIndex = 38;
            this.label6.Text = "Комментарий";
            // 
            // SalaryChangeDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(718, 372);
            this.Controls.Add(this.commentTxt);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.summBonusTxt);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.summVacTxt);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.countTxt);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.skipBtn);
            this.Controls.Add(this.saveBtn);
            this.Controls.Add(this.monthCmb);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.workerCmb);
            this.Controls.Add(this.label1);
            this.Name = "SalaryChangeDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Начислить ЗП";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox workerCmb;
        private System.Windows.Forms.ComboBox monthCmb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button skipBtn;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox countTxt;
        private System.Windows.Forms.TextBox summVacTxt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox summBonusTxt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox commentTxt;
        private System.Windows.Forms.Label label6;
    }
}