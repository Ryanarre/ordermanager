﻿using System;
using System.Windows.Forms;

namespace OrdersManager
{
    public partial class JobTypeDialog : Form
    {
        public JobTypeDialog(
                JobTypeDao jobTypeDao
            ,   int ID
        )
        {
            InitializeComponent();

            mJobTypeDao = jobTypeDao;
            mID = ID;

            Text = mID != 0 ?
                "Редактирование вида работы" :
                "Добавление вида работы"
            ;

            if (mID != 0)
            {
                JobType jobType = jobTypeDao.getJobTypeByID(ID);

                unitTxt.Text = jobType.Unit;
                nameTxt.Text = jobType.Name;
                priceTxt.Text = jobType.PricePerUnit.ToString();
                commentTxt.Text = jobType.Comment;
            }
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            JobType jobType = new JobType();

            if (
                nameTxt.Text.Length == 0 ||
                unitTxt.Text.Length == 0 ||
                priceTxt.Text.Length == 0
            )
            {
                MessageBox.Show("Не все обязательные поля заполнены!");

                return;
            }

            if (!double.TryParse(priceTxt.Text, out jobType.PricePerUnit))
            {
                MessageBox.Show(
                    "Невозможно добавить данные: некорректный формат числовых значений"
                  , "Ошибка"
                  , MessageBoxButtons.OK
                  , MessageBoxIcon.Error
                );
                return;
            }

            jobType.ID = mID;
            jobType.Name = nameTxt.Text;
            jobType.Unit = unitTxt.Text;
            jobType.Comment = commentTxt.Text;

            if (mID != 0)
                mJobTypeDao.modifyJobType(jobType);
            else
                mJobTypeDao.addJobType(jobType, true);

            Close();
        }

        private void skipBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private JobTypeDao mJobTypeDao;

        private int mID;
    }
}
