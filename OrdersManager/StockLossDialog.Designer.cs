﻿
namespace OrdersManager
{
    partial class StockLossDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.orderLbl = new System.Windows.Forms.Label();
            this.orderCmb = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lossDate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.countTxt = new System.Windows.Forms.TextBox();
            this.skipBtn = new System.Windows.Forms.Button();
            this.saveBtn = new System.Windows.Forms.Button();
            this.guaranteeChk = new System.Windows.Forms.CheckBox();
            this.commentTxt = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // orderLbl
            // 
            this.orderLbl.AutoSize = true;
            this.orderLbl.Location = new System.Drawing.Point(22, 189);
            this.orderLbl.Name = "orderLbl";
            this.orderLbl.Size = new System.Drawing.Size(76, 20);
            this.orderLbl.TabIndex = 0;
            this.orderLbl.Text = "На заказ";
            // 
            // orderCmb
            // 
            this.orderCmb.FormattingEnabled = true;
            this.orderCmb.Location = new System.Drawing.Point(171, 186);
            this.orderCmb.Name = "orderCmb";
            this.orderCmb.Size = new System.Drawing.Size(261, 28);
            this.orderCmb.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Дата";
            // 
            // lossDate
            // 
            this.lossDate.Location = new System.Drawing.Point(171, 18);
            this.lossDate.Name = "lossDate";
            this.lossDate.Size = new System.Drawing.Size(261, 26);
            this.lossDate.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "Количество";
            // 
            // countTxt
            // 
            this.countTxt.Location = new System.Drawing.Point(171, 74);
            this.countTxt.Name = "countTxt";
            this.countTxt.Size = new System.Drawing.Size(261, 26);
            this.countTxt.TabIndex = 5;
            // 
            // skipBtn
            // 
            this.skipBtn.Location = new System.Drawing.Point(474, 93);
            this.skipBtn.Name = "skipBtn";
            this.skipBtn.Size = new System.Drawing.Size(197, 55);
            this.skipBtn.TabIndex = 27;
            this.skipBtn.Text = "Отмена";
            this.skipBtn.UseVisualStyleBackColor = true;
            this.skipBtn.Click += new System.EventHandler(this.skipBtn_Click);
            // 
            // saveBtn
            // 
            this.saveBtn.Location = new System.Drawing.Point(474, 18);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(197, 55);
            this.saveBtn.TabIndex = 26;
            this.saveBtn.Text = "Списать";
            this.saveBtn.UseVisualStyleBackColor = true;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // guaranteeChk
            // 
            this.guaranteeChk.AutoSize = true;
            this.guaranteeChk.Location = new System.Drawing.Point(26, 251);
            this.guaranteeChk.Name = "guaranteeChk";
            this.guaranteeChk.Size = new System.Drawing.Size(130, 24);
            this.guaranteeChk.TabIndex = 28;
            this.guaranteeChk.Text = "По гарантии";
            this.guaranteeChk.UseVisualStyleBackColor = true;
            // 
            // commentTxt
            // 
            this.commentTxt.Location = new System.Drawing.Point(171, 130);
            this.commentTxt.Name = "commentTxt";
            this.commentTxt.Size = new System.Drawing.Size(261, 26);
            this.commentTxt.TabIndex = 30;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(22, 137);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(113, 20);
            this.label4.TabIndex = 29;
            this.label4.Text = "Комментарий";
            // 
            // StockLossDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(703, 287);
            this.Controls.Add(this.commentTxt);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.guaranteeChk);
            this.Controls.Add(this.skipBtn);
            this.Controls.Add(this.saveBtn);
            this.Controls.Add(this.countTxt);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lossDate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.orderCmb);
            this.Controls.Add(this.orderLbl);
            this.Name = "StockLossDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Списание со склада";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label orderLbl;
        private System.Windows.Forms.ComboBox orderCmb;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker lossDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox countTxt;
        private System.Windows.Forms.Button skipBtn;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.CheckBox guaranteeChk;
        private System.Windows.Forms.TextBox commentTxt;
        private System.Windows.Forms.Label label4;
    }
}