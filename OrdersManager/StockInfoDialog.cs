﻿using System;
using System.Windows.Forms;

namespace OrdersManager
{
    public partial class StockInfoDialog : Form
    {
        public StockInfoDialog(
                StockInfoDao stockInfoDao
            ,   int ID
        )
        {
            InitializeComponent();

            mStockInfoDao = stockInfoDao;
            mID = ID;

            Text = mID != 0 ?
                "Редактирование материала" :
                "Добавление материала"
            ;

            if (mID != 0)
            {
                StockInfo stockInfo = stockInfoDao.getStockInfoByID(ID);

                titleTxt.Text = stockInfo.Title;
                nameTxt.Text = stockInfo.Name;
                unitTxt.Text = stockInfo.Unit;
                commentTxt.Text = stockInfo.Comment;
            }
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            StockInfo stockInfo = new StockInfo();

            if (
                nameTxt.Text.Length == 0 ||
                unitTxt.Text.Length == 0
            )
            {
                MessageBox.Show("Не все обязательные поля заполнены!");

                return;
            }

            stockInfo.ID = mID;
            stockInfo.Title = titleTxt.Text;
            stockInfo.Name = nameTxt.Text;
            stockInfo.Unit = unitTxt.Text;
            stockInfo.Comment = commentTxt.Text;

            if (mID != 0)
                mStockInfoDao.modifyStockInfo(stockInfo);
            else
                mStockInfoDao.addStockInfo(stockInfo, true);

            Close();
        }

        private void skipBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private StockInfoDao mStockInfoDao;

        private int mID;
    }
}
