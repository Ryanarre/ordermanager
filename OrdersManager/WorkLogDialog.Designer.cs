﻿
namespace OrdersManager
{
    partial class WorkLogDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.skipBtn = new System.Windows.Forms.Button();
            this.saveBtn = new System.Windows.Forms.Button();
            this.commentTxt = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.worker = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.logged = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.missed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.paidSick = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unpaidSick = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.paidLeave = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.unpaidLeave = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label1 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.removeRowBtn = new System.Windows.Forms.Button();
            this.addRowBtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // skipBtn
            // 
            this.skipBtn.Location = new System.Drawing.Point(273, 583);
            this.skipBtn.Name = "skipBtn";
            this.skipBtn.Size = new System.Drawing.Size(197, 55);
            this.skipBtn.TabIndex = 27;
            this.skipBtn.Text = "Отмена";
            this.skipBtn.UseVisualStyleBackColor = true;
            this.skipBtn.Click += new System.EventHandler(this.skipBtn_Click);
            // 
            // saveBtn
            // 
            this.saveBtn.Location = new System.Drawing.Point(36, 583);
            this.saveBtn.Name = "saveBtn";
            this.saveBtn.Size = new System.Drawing.Size(197, 55);
            this.saveBtn.TabIndex = 26;
            this.saveBtn.Text = "Сохранить";
            this.saveBtn.UseVisualStyleBackColor = true;
            this.saveBtn.Click += new System.EventHandler(this.saveBtn_Click);
            // 
            // commentTxt
            // 
            this.commentTxt.Location = new System.Drawing.Point(319, 59);
            this.commentTxt.Name = "commentTxt";
            this.commentTxt.Size = new System.Drawing.Size(239, 26);
            this.commentTxt.TabIndex = 41;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(384, 24);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(104, 20);
            this.label8.TabIndex = 40;
            this.label8.Text = "Примечание";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.worker,
            this.logged,
            this.missed,
            this.paidSick,
            this.unpaidSick,
            this.paidLeave,
            this.unpaidLeave});
            this.dataGridView1.Location = new System.Drawing.Point(36, 113);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 62;
            this.dataGridView1.RowTemplate.Height = 28;
            this.dataGridView1.Size = new System.Drawing.Size(1115, 434);
            this.dataGridView1.TabIndex = 42;
            // 
            // worker
            // 
            this.worker.HeaderText = "Сотрудник";
            this.worker.MinimumWidth = 8;
            this.worker.Name = "worker";
            this.worker.Width = 150;
            // 
            // logged
            // 
            this.logged.HeaderText = "Часов отработано";
            this.logged.MinimumWidth = 8;
            this.logged.Name = "logged";
            this.logged.Width = 150;
            // 
            // missed
            // 
            this.missed.HeaderText = "Часов пропущено";
            this.missed.MinimumWidth = 8;
            this.missed.Name = "missed";
            this.missed.Width = 150;
            // 
            // paidSick
            // 
            this.paidSick.HeaderText = "Оплачиваемый больничный";
            this.paidSick.MinimumWidth = 8;
            this.paidSick.Name = "paidSick";
            this.paidSick.Width = 150;
            // 
            // unpaidSick
            // 
            this.unpaidSick.HeaderText = "Неоплачиваемый больничный";
            this.unpaidSick.MinimumWidth = 8;
            this.unpaidSick.Name = "unpaidSick";
            this.unpaidSick.Width = 150;
            // 
            // paidLeave
            // 
            this.paidLeave.HeaderText = "Оплачиваемый отпуск";
            this.paidLeave.MinimumWidth = 8;
            this.paidLeave.Name = "paidLeave";
            this.paidLeave.Width = 150;
            // 
            // unpaidLeave
            // 
            this.unpaidLeave.HeaderText = "Неоплачиваемый отпуск";
            this.unpaidLeave.MinimumWidth = 8;
            this.unpaidLeave.Name = "unpaidLeave";
            this.unpaidLeave.Width = 150;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(134, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 20);
            this.label1.TabIndex = 43;
            this.label1.Text = "Дата";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(69, 59);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(200, 26);
            this.dateTimePicker1.TabIndex = 44;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(507, 600);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 20);
            this.label2.TabIndex = 45;
            // 
            // removeRowBtn
            // 
            this.removeRowBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.removeRowBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.removeRowBtn.Location = new System.Drawing.Point(1072, 24);
            this.removeRowBtn.Name = "removeRowBtn";
            this.removeRowBtn.Size = new System.Drawing.Size(75, 63);
            this.removeRowBtn.TabIndex = 49;
            this.removeRowBtn.Text = "-";
            this.removeRowBtn.UseVisualStyleBackColor = true;
            this.removeRowBtn.Click += new System.EventHandler(this.removeRowBtn_Click);
            // 
            // addRowBtn
            // 
            this.addRowBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.addRowBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.addRowBtn.Location = new System.Drawing.Point(991, 24);
            this.addRowBtn.Name = "addRowBtn";
            this.addRowBtn.Size = new System.Drawing.Size(75, 63);
            this.addRowBtn.TabIndex = 48;
            this.addRowBtn.Text = "+";
            this.addRowBtn.UseVisualStyleBackColor = true;
            this.addRowBtn.Click += new System.EventHandler(this.addRowBtn_Click);
            // 
            // WorkLogDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1168, 652);
            this.Controls.Add(this.removeRowBtn);
            this.Controls.Add(this.addRowBtn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.commentTxt);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.skipBtn);
            this.Controls.Add(this.saveBtn);
            this.Name = "WorkLogDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "WorkLogDialog";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button skipBtn;
        private System.Windows.Forms.Button saveBtn;
        private System.Windows.Forms.TextBox commentTxt;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewComboBoxColumn worker;
        private System.Windows.Forms.DataGridViewTextBoxColumn logged;
        private System.Windows.Forms.DataGridViewTextBoxColumn missed;
        private System.Windows.Forms.DataGridViewTextBoxColumn paidSick;
        private System.Windows.Forms.DataGridViewTextBoxColumn unpaidSick;
        private System.Windows.Forms.DataGridViewTextBoxColumn paidLeave;
        private System.Windows.Forms.DataGridViewTextBoxColumn unpaidLeave;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button removeRowBtn;
        private System.Windows.Forms.Button addRowBtn;
    }
}