﻿
namespace OrdersManager
{
    partial class DetailDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.jobsMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.materialsMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.lossesMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.jobsFactMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.materialsFactMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.lossesFactMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.guaranteeJobMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.guaranteeMaterialMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.guaranteeLossMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.addMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.editMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.removeMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.menuStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.Left;
            this.menuStrip1.GripMargin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.jobsMenu,
            this.materialsMenu,
            this.lossesMenu,
            this.jobsFactMenu,
            this.materialsFactMenu,
            this.lossesFactMenu,
            this.guaranteeJobMenu,
            this.guaranteeMaterialMenu,
            this.guaranteeLossMenu});
            this.menuStrip1.Location = new System.Drawing.Point(0, 33);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(414, 1265);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // jobsMenu
            // 
            this.jobsMenu.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.jobsMenu.Name = "jobsMenu";
            this.jobsMenu.Size = new System.Drawing.Size(401, 36);
            this.jobsMenu.Text = "Список планируемых работ";
            this.jobsMenu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.jobsMenu.Click += new System.EventHandler(this.jobsMenu_Click);
            // 
            // materialsMenu
            // 
            this.materialsMenu.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.materialsMenu.Name = "materialsMenu";
            this.materialsMenu.Size = new System.Drawing.Size(401, 36);
            this.materialsMenu.Text = "Список планируемых материалов";
            this.materialsMenu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.materialsMenu.Click += new System.EventHandler(this.materialsMenu_Click);
            // 
            // lossesMenu
            // 
            this.lossesMenu.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lossesMenu.Name = "lossesMenu";
            this.lossesMenu.Size = new System.Drawing.Size(401, 36);
            this.lossesMenu.Text = "Список планируемых затрат";
            this.lossesMenu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lossesMenu.Click += new System.EventHandler(this.lossesMenu_Click);
            // 
            // jobsFactMenu
            // 
            this.jobsFactMenu.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.jobsFactMenu.Name = "jobsFactMenu";
            this.jobsFactMenu.Size = new System.Drawing.Size(401, 36);
            this.jobsFactMenu.Text = "Список фактических работ";
            this.jobsFactMenu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.jobsFactMenu.Click += new System.EventHandler(this.jobsFactMenu_Click);
            // 
            // materialsFactMenu
            // 
            this.materialsFactMenu.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.materialsFactMenu.Name = "materialsFactMenu";
            this.materialsFactMenu.Size = new System.Drawing.Size(401, 36);
            this.materialsFactMenu.Text = "Список фактических материалов";
            this.materialsFactMenu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.materialsFactMenu.Click += new System.EventHandler(this.materialsFactMenu_Click);
            // 
            // lossesFactMenu
            // 
            this.lossesFactMenu.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lossesFactMenu.Name = "lossesFactMenu";
            this.lossesFactMenu.Size = new System.Drawing.Size(401, 36);
            this.lossesFactMenu.Text = "Список фактических затрат";
            this.lossesFactMenu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lossesFactMenu.Click += new System.EventHandler(this.lossesFactMenu_Click);
            // 
            // guaranteeJobMenu
            // 
            this.guaranteeJobMenu.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guaranteeJobMenu.Name = "guaranteeJobMenu";
            this.guaranteeJobMenu.Size = new System.Drawing.Size(401, 36);
            this.guaranteeJobMenu.Text = "Гарантийные работы";
            this.guaranteeJobMenu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.guaranteeJobMenu.Click += new System.EventHandler(this.guaranteeJobMenu_Click);
            // 
            // guaranteeMaterialMenu
            // 
            this.guaranteeMaterialMenu.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guaranteeMaterialMenu.Name = "guaranteeMaterialMenu";
            this.guaranteeMaterialMenu.Size = new System.Drawing.Size(401, 36);
            this.guaranteeMaterialMenu.Text = "Гарантийные материалы";
            this.guaranteeMaterialMenu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.guaranteeMaterialMenu.Click += new System.EventHandler(this.guaranteeMaterialMenu_Click);
            // 
            // guaranteeLossMenu
            // 
            this.guaranteeLossMenu.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guaranteeLossMenu.Name = "guaranteeLossMenu";
            this.guaranteeLossMenu.Size = new System.Drawing.Size(401, 36);
            this.guaranteeLossMenu.Text = "Гарантийные затраты";
            this.guaranteeLossMenu.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.guaranteeLossMenu.Click += new System.EventHandler(this.guaranteeLossMenu_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.ActiveCaption;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(477, 38);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 62;
            this.dataGridView1.Size = new System.Drawing.Size(1635, 1254);
            this.dataGridView1.TabIndex = 1;
            // 
            // menuStrip2
            // 
            this.menuStrip2.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.menuStrip2.GripMargin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.menuStrip2.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addMenu,
            this.editMenu,
            this.removeMenu});
            this.menuStrip2.Location = new System.Drawing.Point(0, 0);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(2127, 33);
            this.menuStrip2.TabIndex = 2;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // addMenu
            // 
            this.addMenu.Name = "addMenu";
            this.addMenu.Size = new System.Drawing.Size(106, 29);
            this.addMenu.Text = "Добавить";
            this.addMenu.Click += new System.EventHandler(this.addMenu_Click);
            // 
            // editMenu
            // 
            this.editMenu.Name = "editMenu";
            this.editMenu.Size = new System.Drawing.Size(149, 29);
            this.editMenu.Text = "Редактировать";
            this.editMenu.Click += new System.EventHandler(this.editMenu_Click);
            // 
            // removeMenu
            // 
            this.removeMenu.Name = "removeMenu";
            this.removeMenu.Size = new System.Drawing.Size(92, 29);
            this.removeMenu.Text = "Удалить";
            this.removeMenu.Click += new System.EventHandler(this.removeMenu_Click);
            // 
            // DetailDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(2127, 1298);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.menuStrip2);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "DetailDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Подробно о КП";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem jobsMenu;
        private System.Windows.Forms.ToolStripMenuItem materialsMenu;
        private System.Windows.Forms.ToolStripMenuItem lossesMenu;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ToolStripMenuItem jobsFactMenu;
        private System.Windows.Forms.ToolStripMenuItem materialsFactMenu;
        private System.Windows.Forms.ToolStripMenuItem lossesFactMenu;
        private System.Windows.Forms.ToolStripMenuItem guaranteeJobMenu;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem addMenu;
        private System.Windows.Forms.ToolStripMenuItem editMenu;
        private System.Windows.Forms.ToolStripMenuItem removeMenu;
        private System.Windows.Forms.ToolStripMenuItem guaranteeMaterialMenu;
        private System.Windows.Forms.ToolStripMenuItem guaranteeLossMenu;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
    }
}