﻿using System;
using System.Windows.Forms;

namespace OrdersManager
{
    public partial class OrderDialog : Form
    {
        public OrderDialog(
                OrderDao orderDao
            ,   AgentDao agentDao
            ,   int number
        )
        {
            InitializeComponent();

            mOrderDao = orderDao;
            mID = number;

            for (int i = 0; i < agentDao.size(); ++i)
            {
                Agent agent = agentDao.getAgent(i);

                nameCmb.Items.Add(agent.Name);
            }

            if (mID != 0)
            {
                Text = "Редактирование заказа";

                Order order = mOrderDao.getOrderByID(number);

                numberTxt.Text = order.Number;
                nameCmb.Text = order.Name;
                typeCmb.Text = order.Type;

                openDate.Value = order.StartDate;

                summTxt.Text = order.Summ.ToString();
            }
            else
                Text = "Добавление заказа";
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            Order order = new Order();

            order.ID = mID;
            order.Number = numberTxt.Text;
            order.Name = nameCmb.Text;
            order.Type = typeCmb.Text;

            order.StartDate = openDate.Value;

            if (summTxt.Text.Length != 0)
                order.Summ = double.Parse(summTxt.Text);

            if (mID != 0)
                mOrderDao.modifyOrder(order);
            else
                mOrderDao.addOrder(order, true);

            Close();
        }

        private void skipBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private OrderDao mOrderDao;
        private int mID;
    }
}
