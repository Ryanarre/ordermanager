﻿using System;
using System.Windows.Forms;

namespace OrdersManager
{
    public partial class OrganisationDialog : Form
    {
        public OrganisationDialog(
                OrganisationDao organisationDao
            ,   int number
        )
        {
            InitializeComponent();

            mOrganisationDao = organisationDao;
            mID = number;

            Text = mID != 0 ? 
                "Редактирование организации" : 
                "Добавление организации"
            ;

            if (mID != 0)
            {
                Organisation organisation = mOrganisationDao.getOrganisationByID(number);

                nameTxt.Text = organisation.Name;
                typeCmb.Text = organisation.Type;
                openDate.Value = organisation.StartDate;
                commentTxt.Text = organisation.Comment;
            }
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            if (nameTxt.Text.Length == 0 || typeCmb.Text.Length == 0)
            {
                MessageBox.Show("Не все обязательные поля заполнены!");

                return;
            }

            Organisation organisation = new Organisation();

            organisation.ID = mID;
            organisation.Name = nameTxt.Text;
            organisation.Type = typeCmb.Text;
            organisation.StartDate = openDate.Value;
            organisation.Comment = commentTxt.Text;

            if (mID != 0)
                mOrganisationDao.modifyOrganisation(organisation);
            else
                mOrganisationDao.addOrganisation(organisation, true);

            Close();
        }

        private void skipBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private OrganisationDao mOrganisationDao;

        int mID;
    }
}
