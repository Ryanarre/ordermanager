﻿using System;
using System.Windows.Forms;

namespace OrdersManager
{
    public partial class BasicJobDialog : Form
    {
        public BasicJobDialog(
                DatabaseManager dbManager
            ,   int ID
        )
        {
            InitializeComponent();

            mDbManager = dbManager;
            mID = ID;

            Text = mID != 0 ?
                "Редактирование базовой работы" :
                "Добавление базовой работы"
            ;

            nameCmb.Items.Clear();
            for (int i = 0; i < dbManager.getJobTypeDao().size(); ++i)
                nameCmb.Items.Add(
                    dbManager.getJobTypeDao().getJobType(i).Name
                );

            if (mID != 0)
            {
                BasicJob basicJob = dbManager.getBasicJobDao().getBasicJobByID(ID);

                unitCmb.Text = basicJob.Unit;
                nameCmb.Text = basicJob.Name;
                priceTxt.Text = basicJob.PricePerUnit.ToString();
                commentTxt.Text = basicJob.Comment;
            }
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            BasicJob basicJob = new BasicJob();

            if (
                nameCmb.Text.Length == 0 ||
                unitCmb.Text.Length == 0 ||
                priceTxt.Text.Length == 0
            )
            {
                MessageBox.Show("Не все обязательные поля заполнены!");

                return;
            }

            if (!double.TryParse(priceTxt.Text, out basicJob.PricePerUnit))
            {
                MessageBox.Show(
                    "Невозможно добавить данные: некорректный формат числовых значений"
                  , "Ошибка"
                  , MessageBoxButtons.OK
                  , MessageBoxIcon.Error
                );
                return;
            }

            basicJob.ID = mID;
            basicJob.Name = nameCmb.Text;
            basicJob.Unit = unitCmb.Text;
            basicJob.Comment = commentTxt.Text;

            if (mID != 0)
                mDbManager.getBasicJobDao().modifyBasicJob(basicJob);
            else
                mDbManager.getBasicJobDao().addBasicJob(basicJob, true);

            Close();
        }

        private void skipBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private DatabaseManager mDbManager;

        private int mID;
    }
}
