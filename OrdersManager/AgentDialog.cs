﻿using System;
using System.Windows.Forms;

namespace OrdersManager
{
    public partial class AgentDialog : Form
    {
        public AgentDialog(
                AgentDao agentDao
            ,   AgentType agentType
            ,   int number
        )
        {
            InitializeComponent();

            mAgentDao = agentDao;
            mAgentType = agentType;
            mID = number;

            string title = mID != 0 ? "Редактирование " : "Добавление ";
            title += getEntityName(agentType);

            Text = title;

            if (mID != 0)
            {
                Agent agent = mAgentDao.getAgentByID(number);

                nameTxt.Text = agent.Name;
                phoneTxt.Text = agent.Phone;
                emailTxt.Text = agent.Email;
            }
        }

        private string getEntityName(AgentType agentType)
        {
            switch (agentType)
            {
                case AgentType.Supplier:
                default:
                    return "поставщика";
                case AgentType.Customer:
                    return "заказчика";
                case AgentType.Contractor:
                    return "подрядчика";
            }
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            Agent agent = new Agent();

            agent.ID = mID;
            agent.Name = nameTxt.Text;
            agent.Phone = phoneTxt.Text;
            agent.Email = emailTxt.Text;

            if (mID != 0)
                mAgentDao.modifyAgent(agent);
            else
                mAgentDao.addAgent(agent, true);

            Close();
        }

        private void skipBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private AgentDao mAgentDao;

        private AgentType mAgentType;

        private int mID;
    }
}
