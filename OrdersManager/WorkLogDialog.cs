﻿using System;
using System.Windows.Forms;

namespace OrdersManager
{
    public partial class WorkLogDialog : Form
    {
        public WorkLogDialog(
                DatabaseManager dbManager
            ,   int ID
        )
        {
            InitializeComponent();

            mDbManager = dbManager;
            mID = ID;

            Text = mID != 0 ?
                "Редактирование отчёта о времени" :
                "Добавление отчёта о времени"
            ;

            if (mID != 0)
            {
                WorkDay workDay = dbManager.getWorkDayDao().getWorkDayByID(ID);

                dateTimePicker1.Value = workDay.Date;
                commentTxt.Text = workDay.Comment;

                for (int i = 0; i < mDbManager.getWorkLogDao().size(); ++i)
                {
                    WorkLog workLog = mDbManager.getWorkLogDao().getWorkLog(i);

                    if (workLog.WorkDayID != ID)
                        continue;

                    dataGridView1.Rows.Add();
                    int index = dataGridView1.Rows.Count - 1;

                    //dataGridView1.Rows[index].Cells[0].Value = holiday.Date; -> WorkerID?
                    dataGridView1.Rows[index].Cells[1].Value = workLog.Logged;
                    dataGridView1.Rows[index].Cells[2].Value = workLog.Missed;
                    dataGridView1.Rows[index].Cells[3].Value = workLog.PaidSick;
                    dataGridView1.Rows[index].Cells[4].Value = workLog.UnpaidSick;
                    dataGridView1.Rows[index].Cells[5].Value = workLog.PaidLeave;
                    dataGridView1.Rows[index].Cells[6].Value = workLog.UnpaidLeave;
                }
            }
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            WorkDay workDay = new WorkDay();

            workDay.ID = mID;
            workDay.Date = dateTimePicker1.Value;
            workDay.Comment = commentTxt.Text;

            if (mID != 0)
                mDbManager.getWorkDayDao().modifyWorkDay(workDay);
            else
            {
                mDbManager.getWorkDayDao().addWorkDay(workDay, true);

                mID = mDbManager.getWorkDayDao().getWorkDay(
                    mDbManager.getWorkDayDao().size() - 1
                ).ID;
            }

            mDbManager.getWorkLogDao().removeWorkLogs(mID);
            for (int i = 0; i < dataGridView1.Rows.Count; ++i)
            {
                for (int j = 1; j < dataGridView1.Columns.Count; ++j) // TODO: Replace with 0
                    if (
                        dataGridView1.Rows[i].Cells[j].Value.ToString().Length == 0
                    )
                    {
                        MessageBox.Show("Не все обязательные поля заполнены!");

                        return;
                    }

                WorkLog workLog = new WorkLog();

                workLog.WorkDayID = mID;
                // WorkerID ?
                workLog.Logged = int.Parse(dataGridView1.Rows[i].Cells[1].Value.ToString());
                workLog.Missed = int.Parse(dataGridView1.Rows[i].Cells[2].Value.ToString());
                workLog.PaidSick = int.Parse(dataGridView1.Rows[i].Cells[3].Value.ToString());
                workLog.UnpaidSick = int.Parse(dataGridView1.Rows[i].Cells[4].Value.ToString());
                workLog.PaidLeave = int.Parse(dataGridView1.Rows[i].Cells[5].Value.ToString());
                workLog.UnpaidLeave = int.Parse(dataGridView1.Rows[i].Cells[6].Value.ToString());

                mDbManager.getWorkLogDao().addWorkLog(workLog, true);
            }

            Close();
        }

        private void skipBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void removeCurrentRow()
        {
            int index = 0;
            if (detectCurrentRow(ref index))
                dataGridView1.Rows.RemoveAt(index);
        }

        private bool detectCurrentRow(ref int index)
        {
            DataGridViewCell cell = dataGridView1.CurrentCell;

            if (cell == null)
                return false;

            index = cell.RowIndex;
            return true;
        }

        private void removeRowBtn_Click(object sender, EventArgs e)
        {
            removeCurrentRow();
        }

        private void addRowBtn_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Add();

            for (int i = 0; i < mDbManager.getWorkerDao().size(); ++i)
            {
                Worker worker = mDbManager.getWorkerDao().getWorker(i);

                DataGridViewComboBoxCell cell = 
                    (DataGridViewComboBoxCell) dataGridView1.Rows[dataGridView1.Rows.Count - 1].Cells[0];
                if (worker.Status)
                    cell.Items.Add(
                        '[' + worker.Passport + "] " +
                        worker.Surname + " " + 
                        worker.Name[0] + "." +
                        worker.Patronymic[0] + "."
                    );
            }
        }

        private DatabaseManager mDbManager;

        private int mID;
    }
}
