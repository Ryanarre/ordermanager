﻿using System;
using System.Windows.Forms;

namespace OrdersManager
{
    public partial class WorkerDialog : Form
    {
        public WorkerDialog(
              WorkerDao workerDao
            , int number
        )
        {
            InitializeComponent();

            mWorkerDao = workerDao;
            mID = number;

            Text = mID != 0 ?
                "Редактирование сотрудника" :
                "Добавление сотрудника"
            ;

            if (mID != 0)
            {
                Worker worker = mWorkerDao.getWorkerByID(number);

                surnameTxt.Text = worker.Surname;
                nameTxt.Text = worker.Name;
                patronimycTxt.Text = worker.Patronymic;
                phoneTxt.Text = worker.Phone;
                mailTxt.Text = worker.Email;
                titleTxt.Text = worker.Title;
                birthDate.Value = worker.BirthDate;
                startDate.Value = worker.HireDate;
                passportTxt.Text = worker.Passport;
                commentTxt.Text = worker.Comment;

                salaryTxt.Visible = false;
                salaryLbl.Visible = false;
            }
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            Worker worker = new Worker();

            if (
                nameTxt.Text.Length == 0 ||
                surnameTxt.Text.Length == 0 ||
                patronimycTxt.Text.Length == 0 ||
                phoneTxt.Text.Length == 0 ||
                passportTxt.Text.Length == 0
            )
            {
                MessageBox.Show("Не все обязательные поля заполнены!");

                return;
            }

            worker.ID = mID;
            worker.Surname = surnameTxt.Text;
            worker.Name = nameTxt.Text;
            worker.Patronymic = patronimycTxt.Text;
            worker.Phone = phoneTxt.Text;
            worker.Email = mailTxt.Text;
            worker.Title = titleTxt.Text;
            worker.BirthDate = birthDate.Value;
            worker.HireDate = startDate.Value;
            worker.Passport = passportTxt.Text;
            worker.Comment = commentTxt.Text;

            if (salaryTxt.Visible)
                if (!double.TryParse(salaryTxt.Text, out worker.Salary))
                {
                    MessageBox.Show(
                        "Невозможно добавить сотрудника: некорректный формат ставки"
                      , "Ошибка"
                      , MessageBoxButtons.OK
                      , MessageBoxIcon.Error
                    );
                    return;
                }

            if (mID != 0)
                mWorkerDao.modifyWorker(worker);
            else
                mWorkerDao.addWorker(worker, true);

            Close();
        }

        private void skipBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private WorkerDao mWorkerDao;

        private int mID;
    }
}
