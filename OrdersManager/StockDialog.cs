﻿using System;
using System.Windows.Forms;

namespace OrdersManager
{
    public partial class StockDialog : Form
    {
        public StockDialog(
                DatabaseManager dbManager
            ,   StockType stockType
            ,   int number
        )
        {
            InitializeComponent();

            mDbManager = dbManager;
            mStockType = stockType;
            mID = number;

            Text = mID != 0 ?
                "Редактирование материала" :
                "Добавление материала"
            ;

            if (mID != 0)
            {
                Stock stock = mDbManager.getStockDao(stockType).getStockByID(number);

                stockCmb.Text = stock.Name;
                unitTxt.Text = stock.Unit;
                pricePerUniteTxt.Text = stock.PricePerUnite.ToString();
                countTxt.Text = stock.CountBegin.ToString();
            }

            if (stockType == StockType.Tool)
            {
                unitTxt.Visible = false;
                unitLbl.Visible = false;
            }

            for (int i = 0; i < mDbManager.getStockDao(stockType).size(); ++i)
                stockCmb.Items.Add(mDbManager.getStockDao(stockType).getStock(i).Name);
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            int initStockBegin = 0;

            Stock stock = new Stock();

            stock.ID = mID;
            stock.Name = stockCmb.Text;

            for (int i = 0; i < mDbManager.getStockDao(mStockType).size(); ++i)
                if (mDbManager.getStockDao(mStockType).getStock(i).Name == stock.Name)
                {
                    mID = mDbManager.getStockDao(mStockType).getStock(i).ID;
                    initStockBegin = mDbManager.getStockDao(mStockType).getStock(i).CountBegin;
                }

            stock.Unit = unitTxt.Text;

            if (
                !double.TryParse(pricePerUniteTxt.Text, out stock.PricePerUnite) ||
                !int.TryParse(countTxt.Text, out stock.CountBegin)
            )
            {
                MessageBox.Show(
                    "Невозможно добавить данные: некорректный формат числовых значений"
                  , "Ошибка"
                  , MessageBoxButtons.OK
                  , MessageBoxIcon.Error
                );
                return;
            }

            stock.CountEnd += stock.CountBegin;
            if (mID != 0)
            {
                StockHistory stockHistory = new StockHistory();

                stockHistory.OrderName = stock.Name;
                stockHistory.Count = stock.CountBegin;
                stockHistory.Date = DateTime.Now;
                stockHistory.StockID = mID;
                stockHistory.Side = true;
                stockHistory.Type = mStockType;

                mDbManager.getStockHistoryDao().addStockHistory(stockHistory, true);

                stock.CountBegin = initStockBegin;
                mDbManager.getStockDao(mStockType).modifyStock(stock);
            }
            else
            {
                mDbManager.getStockDao(mStockType).addStock(stock, true);

                mID = mDbManager.getStockDao(mStockType).getStock(
                    mDbManager.getStockDao(mStockType).size() - 1
                ).ID;
            }

            Close();
        }

        private void skipBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void stockCmb_SelectedIndexChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < mDbManager.getStockDao(mStockType).size(); ++i)
                if (mDbManager.getStockDao(mStockType).getStock(i).Name == stockCmb.Text)
                {
                    unitTxt.Text = mDbManager.getStockDao(mStockType).getStock(i).Unit;
                    pricePerUniteTxt.Text = mDbManager.getStockDao(mStockType).getStock(i).PricePerUnite.ToString();
                }
        }

        private DatabaseManager mDbManager;

        private StockType mStockType;

        int mID;
    }
}
