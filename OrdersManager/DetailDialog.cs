﻿using System;
using System.Windows.Forms;

namespace OrdersManager
{
    public partial class DetailDialog : Form
    {
        public DetailDialog(
                DatabaseManager dbManager
            ,   string orderName
            ,   bool isMain
        )
        {
            InitializeComponent();

            mDbManager = dbManager;
            mOrderName = orderName;
            mIsMain = isMain;

            jobsFactMenu.Visible = isMain;
            materialsFactMenu.Visible = isMain;
            lossesFactMenu.Visible = isMain;

            guaranteeJobMenu.Visible = isMain;
            guaranteeMaterialMenu.Visible = isMain;
            guaranteeLossMenu.Visible = isMain;

            Text = isMain ? "Подробно о заказе" : "Подробно о КП";

            init(DetailType.PlanJob);
        }

        private void jobsMenu_Click(object sender, EventArgs e)
        {
            init(DetailType.PlanJob);
        }

        private void materialsMenu_Click(object sender, EventArgs e)
        {
            init(DetailType.PlanMaterial);
        }

        private void lossesMenu_Click(object sender, EventArgs e)
        {
            init(DetailType.PlanLoss);
        }

        private void jobsFactMenu_Click(object sender, EventArgs e)
        {
            init(DetailType.FactJob);
        }

        private void materialsFactMenu_Click(object sender, EventArgs e)
        {
            init(DetailType.FactMaterial);
        }

        private void lossesFactMenu_Click(object sender, EventArgs e)
        {
            init(DetailType.FactLoss);
        }

        private void guaranteeJobMenu_Click(object sender, EventArgs e)
        {
            init(DetailType.GuaranteeJob);
        }

        private void guaranteeMaterialMenu_Click(object sender, EventArgs e)
        {
            init(DetailType.GuaranteeMaterial);
        }

        private void guaranteeLossMenu_Click(object sender, EventArgs e)
        {
            init(DetailType.GuaranteeLoss);
        }

        private bool KillProcess()
        {
            DialogResult result = MessageBox.Show(
                  "Действительно хотите удалить?"
                , "Подтвердите действие"
                , MessageBoxButtons.YesNo
                , MessageBoxIcon.Question
            );

            return result == DialogResult.Yes;
        }

        private void init(DetailType detailType)
        {
            mDetailType = detailType;

            bool isPlanned =
                detailType == DetailType.PlanJob ||
                detailType == DetailType.PlanMaterial ||
                detailType == DetailType.PlanLoss            
            ;

            menuStrip2.Visible = isPlanned;

            dataGridView1.Columns.Clear();

            dataGridView1.Columns.Add("ID", "ID");
            dataGridView1.Columns.Add("name", "Наименование");
            dataGridView1.Columns.Add("unit", "Единица измерения");
            dataGridView1.Columns.Add("pricePerUnit", "Стоимость за единицу");
            dataGridView1.Columns.Add("comment", "Комментарий");
            if (!isPlanned)
                dataGridView1.Columns.Add("date", "Дата");

            if (isPlanned)
            {
                for (int i = 0; i < mDbManager.getDetailDao(detailType).size(); ++i)
                {
                    Detail detail = mDbManager.getDetailDao(detailType).getDetail(i);

                    if (detail.Type != detailType || detail.OrderName != mOrderName)
                        continue;

                    dataGridView1.Rows.Add();
                    int index = dataGridView1.Rows.Count - 1;

                    dataGridView1.Rows[index].Cells[0].Value = index + 1;
                    dataGridView1.Rows[index].Cells[1].Value = detail.Name;
                    dataGridView1.Rows[index].Cells[2].Value = detail.Unit;
                    dataGridView1.Rows[index].Cells[3].Value = detail.PricePerUnit;
                    dataGridView1.Rows[index].Cells[4].Value = detail.Comment;
                }
            }
            else if (
                detailType == DetailType.FactLoss || 
                detailType == DetailType.GuaranteeLoss ||
                detailType == DetailType.FactJob ||
                detailType == DetailType.GuaranteeLoss
            )
            {
                FlowDao flowDao = detailType == DetailType.FactLoss || detailType == DetailType.GuaranteeLoss ?
                    mDbManager.getLossDao() : mDbManager.getChargeDao()
                ;

                for (int i = 0; i < flowDao.size(); ++i)
                {
                    Flow flow = flowDao.getFlow(i);

                    if (
                        flow.Number == mOrderName &&
                        flow.IsGuarantee == (detailType == DetailType.GuaranteeLoss))
                    {
                        dataGridView1.Rows.Add();
                        int index = dataGridView1.Rows.Count - 1;

                        dataGridView1.Rows[index].Cells[0].Value = index + 1;
                        dataGridView1.Rows[index].Cells[1].Value = flow.Purpose;
                        dataGridView1.Rows[index].Cells[2].Value = flow.Unit;
                        dataGridView1.Rows[index].Cells[3].Value = flow.PricePerUnit;
                        dataGridView1.Rows[index].Cells[4].Value = flow.Comment;
                        dataGridView1.Rows[index].Cells[5].Value = flow.Date;
                    }
                }
            }
            else
            {
                for (int i = 0; i < mDbManager.getStockHistoryDao().size(); ++i)
                {
                    StockHistory stockHistory = mDbManager.getStockHistoryDao().getStockHistory(i);

                    if (
                        stockHistory.OrderName == mOrderName &&
                        stockHistory.IsGuarantee == (detailType == DetailType.GuaranteeMaterial))
                    {
                        dataGridView1.Rows.Add();
                        int index = dataGridView1.Rows.Count - 1;

                        Stock stock = mDbManager.getStockDao(stockHistory.Type).getStockByID(stockHistory.StockID);

                        dataGridView1.Rows[index].Cells[0].Value = index + 1;
                        dataGridView1.Rows[index].Cells[1].Value = stock.Name;
                        dataGridView1.Rows[index].Cells[2].Value = stock.Unit;
                        dataGridView1.Rows[index].Cells[3].Value = stock.PricePerUnite;
                        dataGridView1.Rows[index].Cells[4].Value = stockHistory.Comment;
                        dataGridView1.Rows[index].Cells[5].Value = stockHistory.Date;
                    }
                }
            }

            resetMenus();
        }

        private void addMenu_Click(object sender, EventArgs e)
        {
            DetailAddDialog detailAddDialog = new DetailAddDialog(
                    mDbManager.getDetailDao(mDetailType)
                ,   mDetailType
                ,   mOrderName
                ,   0
            );

            detailAddDialog.ShowDialog();

            init(mDetailType);
        }

        private bool detectCurrentRow(ref int index)
        {
            DataGridViewCell cell = dataGridView1.CurrentCell;

            if (cell == null)
                return false;

            index = cell.RowIndex;
            return true;
        }

        private void editMenu_Click(object sender, EventArgs e)
        {
            string number = "";
                
            int index = 0;
            if (!detectCurrentRow(ref index))
                return;

            number = dataGridView1.Rows[index].Cells[0].Value.ToString();

            DetailAddDialog detailAddDialog = new DetailAddDialog(
                    mDbManager.getDetailDao(mDetailType)
                ,   mDetailType
                ,   mOrderName
                ,   int.Parse(number)
            );

            detailAddDialog.ShowDialog();

            init(mDetailType);
        }

        private void removeMenu_Click(object sender, EventArgs e)
        {
            if (!KillProcess())
                return;

            int index = 0;
            if (detectCurrentRow(ref index))
            {
                string number = dataGridView1.Rows[index].Cells[0].Value.ToString();

                mDbManager.getDetailDao(mDetailType).removeDetail(int.Parse(number));
            }

            init(mDetailType);
        }

        private void resetMenus()
        {
            jobsMenu.BackColor = getMenuColor(mDetailType == DetailType.PlanJob);

            materialsMenu.BackColor = getMenuColor(mDetailType == DetailType.PlanMaterial);

            lossesMenu.BackColor = getMenuColor(mDetailType == DetailType.PlanLoss);

            jobsFactMenu.BackColor = getMenuColor(mDetailType == DetailType.FactJob);

            materialsFactMenu.BackColor = getMenuColor(mDetailType == DetailType.FactMaterial);

            lossesFactMenu.BackColor = getMenuColor(mDetailType == DetailType.FactLoss);

            guaranteeJobMenu.BackColor = getMenuColor(mDetailType == DetailType.GuaranteeJob);

            guaranteeMaterialMenu.BackColor = getMenuColor(mDetailType == DetailType.GuaranteeMaterial);

            guaranteeLossMenu.BackColor = getMenuColor(mDetailType == DetailType.GuaranteeLoss);
        }

        private System.Drawing.Color getMenuColor(bool isActive)
        {
            return isActive ?
                System.Drawing.Color.Gray :
                System.Drawing.Color.Transparent
            ;
        }

        private DatabaseManager mDbManager;

        private DetailType mDetailType;

        private string mOrderName;

        private bool mIsMain;
    }
}
