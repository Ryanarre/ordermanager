﻿using System.Data.SqlClient;

namespace OrdersManager
{
    public class DatabaseManager
    {
        public DatabaseManager()
        {
            // TODO: extract from SQL
        }

        public OrderDao getOrderDao()
        {
            return mOrderDao;
        }

        public FlowDao getFlowDao(FlowType flowType)
        {
            switch (flowType)
            {
                default:
                case FlowType.Profit:
                    return getProfitDao();
                case FlowType.Loss:
                    return getLossDao();
                case FlowType.Charge:
                    return getChargeDao();
                case FlowType.Payment:
                    return getPaymentDao();
            }
        }

        public FlowDao getProfitDao()
        {
            return mProfitDao;
        }

        public FlowDao getLossDao()
        {
            return mLossDao;
        }

        public FlowDao getChargeDao()
        {
            return mChargeDao;
        }

        public FlowDao getPaymentDao()
        {
            return mPaymentDao;
        }

        public AgentDao getAgentDao(AgentType agentType)
        {
            switch (agentType)
            {
                default:
                case AgentType.Supplier:
                    return getSupplierDao();
                case AgentType.Customer:
                    return getCustomerDao();
                case AgentType.Contractor:
                    return getContractorDao();
            }
        }

        public AgentDao getSupplierDao()
        {
            return mSupplierDao;
        }

        public AgentDao getCustomerDao()
        {
            return mCustomerDao;
        }

        public AgentDao getContractorDao()
        {
            return mContractorDao;
        }

        public WorkerDao getWorkerDao()
        {
            return mWorkerDao;
        }

        public OrganisationDao getOrganisationDao()
        {
            return mOrganisationDao;
        }

        public AccountDao getAccountDao()
        {
            return mAccountDao;
        }

        public SalaryDao getSalaryDao()
        {
            return mSalaryDao;
        }

        public StockDao getStockDao(StockType stockType)
        {
            switch (stockType)
            {
                default:
                case StockType.Material:
                    return getMaterialDao();
                case StockType.Tool:
                    return getToolDao();
                case StockType.CustomMaterial:
                    return getCustomMaterialDao();
                case StockType.Trim:
                    return getTrimDao();
            }
        }

        public StockDao getMaterialDao()
        {
            return mMaterialDao;
        }

        public StockDao getToolDao()
        {
            return mToolDao;
        }

        public StockDao getCustomMaterialDao()
        {
            return mCustomMaterialDao;
        }

        public StockDao getTrimDao()
        {
            return mTrimDao;
        }

        public StockInfoDao getStockInfoDao(StockType stockType)
        {
            switch (stockType)
            {
                default:
                case StockType.Material:
                    return getMaterialInfoDao();
                case StockType.Tool:
                    return getToolInfoDao();
                case StockType.CustomMaterial:
                    return getCustomMaterialInfoDao();
                case StockType.Trim:
                    return getTrimInfoDao();
            }
        }

        public StockInfoDao getMaterialInfoDao()
        {
            return mMaterialInfoDao;
        }

        public StockInfoDao getToolInfoDao()
        {
            return mToolInfoDao;
        }

        public StockInfoDao getCustomMaterialInfoDao()
        {
            return mCustomMaterialInfoDao;
        }

        public StockInfoDao getTrimInfoDao()
        {
            return mTrimInfoDao;
        }

        public DetailDao getDetailDao(DetailType detailType)
        {
            switch (detailType)
            {
                default:
                case DetailType.PlanJob:
                    return mPlanJobDao;
                case DetailType.PlanLoss:
                    return mPlanLossDao;
                case DetailType.PlanMaterial:
                    return mPlanMaterialDao;
            }
        }

        public DetailDao getPlanJobDao()
        {
            return mPlanJobDao;
        }

        public DetailDao getPlanLossDao()
        {
            return mPlanLossDao;
        }

        public DetailDao getPlanMaterialDao()
        {
            return mPlanMaterialDao;
        }
        
        public StockHistoryDao getStockHistoryDao()
        {
            return mStockHistoryDao;
        }

        public JobTypeDao getJobTypeDao()
        {
            return mJobTypeDao;
        }

        public BasicJobDao getBasicJobDao()
        {
            return mBasicJobDao;
        }

        public ReferenceDao getReferenceDao(MenuElem referenceType)
        {
            switch (referenceType)
            {
                case MenuElem.Department:
                default:
                    return getDepartmentDao();
                case MenuElem.PurposeFinancial:
                    return getPurposeFinancialDao();
                case MenuElem.PurposeMaterial:
                    return getPurposeMaterialDao();
            }    
        }

        public ReferenceDao getDepartmentDao()
        {
            return mDepartmentDao;
        }

        public ReferenceDao getPurposeMaterialDao()
        {
            return mPurposeMaterialDao;
        }

        public ReferenceDao getPurposeFinancialDao()
        {
            return mPurposeFinancialDao;
        }

        public WorkLogDao getWorkLogDao()
        {
            return mWorkLogDao;
        }

        public WorkDayDao getWorkDayDao()
        {
            return mWorkDayDao;
        }

        public CalendarDao getCalendarDao()
        {
            return mCalendarDao;
        }

        public HolidayDao getHolidayDao()
        {
            return mHolidayDao;
        }

        private OrderDao mOrderDao = new OrderDao(false);

        private FlowDao mProfitDao = new FlowDao(FlowType.Profit);
        private FlowDao mLossDao = new FlowDao(FlowType.Loss);
        private FlowDao mChargeDao = new FlowDao(FlowType.Charge);
        private FlowDao mPaymentDao = new FlowDao(FlowType.Payment);

        private AgentDao mSupplierDao = new AgentDao(AgentType.Supplier);
        private AgentDao mCustomerDao = new AgentDao(AgentType.Customer);
        private AgentDao mContractorDao = new AgentDao(AgentType.Contractor);

        private WorkerDao mWorkerDao = new WorkerDao();
        private OrganisationDao mOrganisationDao = new OrganisationDao();
        private AccountDao mAccountDao = new AccountDao();
        private SalaryDao mSalaryDao = new SalaryDao();

        private StockDao mMaterialDao = new StockDao(StockType.Material);
        private StockDao mToolDao = new StockDao(StockType.Tool);
        private StockDao mCustomMaterialDao = new StockDao(StockType.CustomMaterial);
        private StockDao mTrimDao = new StockDao(StockType.Trim);

        private StockInfoDao mMaterialInfoDao = new StockInfoDao(StockType.Material);
        private StockInfoDao mToolInfoDao = new StockInfoDao(StockType.Tool);
        private StockInfoDao mCustomMaterialInfoDao = new StockInfoDao(StockType.CustomMaterial);
        private StockInfoDao mTrimInfoDao = new StockInfoDao(StockType.Trim);

        private DetailDao mPlanJobDao = new DetailDao(DetailType.PlanJob);
        private DetailDao mPlanLossDao = new DetailDao(DetailType.PlanLoss);
        private DetailDao mPlanMaterialDao = new DetailDao(DetailType.PlanMaterial);

        private StockHistoryDao mStockHistoryDao = new StockHistoryDao();

        private JobTypeDao mJobTypeDao = new JobTypeDao();

        private BasicJobDao mBasicJobDao = new BasicJobDao();

        private ReferenceDao mDepartmentDao = new ReferenceDao(MenuElem.Department);
        private ReferenceDao mPurposeFinancialDao = new ReferenceDao(MenuElem.PurposeFinancial);
        private ReferenceDao mPurposeMaterialDao = new ReferenceDao(MenuElem.PurposeMaterial);

        private WorkLogDao mWorkLogDao = new WorkLogDao();
        private WorkDayDao mWorkDayDao = new WorkDayDao();

        private CalendarDao mCalendarDao = new CalendarDao();
        private HolidayDao mHolidayDao = new HolidayDao();
    }

}