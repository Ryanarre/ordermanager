﻿using System;
using System.Windows.Forms;

namespace OrdersManager
{
    public partial class FlowDialog : Form
    {
        public FlowDialog(
                DatabaseManager dbManager
            ,   FlowType flowType
            ,   int number
        )
        {
            InitializeComponent();

            mDbManager = dbManager;
            mFlowType = flowType;
            mID = number;

            string title = mID != 0 ? "Редактирование " : " Добавление ";
            title += getEntityName(flowType);
            Text = title;

            guaranteeChk.Visible = 
                flowType == FlowType.Loss || 
                flowType == FlowType.Charge
            ;

            if (mID != 0)
            {
                Flow flow = mDbManager.getFlowDao(flowType).getFlowByID(number);

                numberTxt.Text = flow.Number;
                agentCmb.Text = flow.Agent;
                currencyCmb.Text = flow.Currency;
                commentTxt.Text = flow.Comment;
                purposeTxt.Text = flow.Purpose;
                openDate.Value = flow.Date;
                summTxt.Text = flow.Summ.ToString();
                guaranteeChk.Checked = flow.IsGuarantee;
            }

            if (flowType == FlowType.Profit)
            {
                agentLbl.Text = "Отправитель";

                for (int i = 0; i < mDbManager.getCustomerDao().size(); ++i)
                    agentCmb.Items.Add(mDbManager.getCustomerDao().getAgent(i).Name);
            }
            else
            {
                for (int i = 0; i < mDbManager.getContractorDao().size(); ++i)
                    agentCmb.Items.Add(mDbManager.getContractorDao().getAgent(i).Name);

                for (int i = 0; i < mDbManager.getWorkerDao().size(); ++i)
                    agentCmb.Items.Add(
                        dbManager.getWorkerDao().getWorker(i).Surname + " " +
                        dbManager.getWorkerDao().getWorker(i).Name + " " +
                        dbManager.getWorkerDao().getWorker(i).Patronymic
                    );
            }
        }

        private string getEntityName(FlowType flowType)
        {
            switch (flowType)
            {
                case FlowType.Profit:
                default:
                    return "прихода";
                case FlowType.Loss:
                    return "расхода";
                case FlowType.Charge:
                    return "начисления";
                case FlowType.Payment:
                    return "выплаты";
            }
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            Flow flow = new Flow();

            flow.ID = mID;
            flow.Agent = agentCmb.Text;
            flow.Number = numberTxt.Text;
            flow.Purpose = purposeTxt.Text;
            flow.Date = openDate.Value;

            if (!double.TryParse(summTxt.Text, out flow.Summ))
            {
                MessageBox.Show(
                    "Невозможно добавить закупку: некорректный формат суммы"
                  , "Ошибка"
                  , MessageBoxButtons.OK
                  , MessageBoxIcon.Error
                );
                return;
            }

            flow.Currency = currencyCmb.Text;
            flow.Comment = commentTxt.Text;
            flow.IsGuarantee = guaranteeChk.Checked;

            if (mID != 0)
                mDbManager.getFlowDao(mFlowType).modifyFlow(flow);
            else
                mDbManager.getFlowDao(mFlowType).addFlow(flow, true);

            Close();
        }

        private void skipBtn_Click(object sender, EventArgs e)
        {
            Close();
        }

        private DatabaseManager mDbManager;

        private FlowType mFlowType;

        private int mID;
    }
}
